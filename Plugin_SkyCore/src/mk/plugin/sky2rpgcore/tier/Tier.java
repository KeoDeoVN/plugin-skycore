package mk.plugin.sky2rpgcore.tier;

public enum Tier {
	
	SO_CAP("Sơ cấp", "§7", "I", 1, 0.75),
	TRUNG_CAP("Trung cấp", "§9", "II", 2, 1),
	CAO_CAP("Cao cấp", "§c", "III", 3, 1.25),
	CUC_PHAM("Cực phẩm", "§6", "IV", 4, 1.5),
	HUYEN_THOAI("Huyền thoại", "§e", "V", 5, 2);
	
	private String color;
	private String icon;
	private String name;
	private int bonus;
	private double statMultiple;
	
	private Tier(String name, String color, String icon, int bonus, double statMultiple) {
		this.color = color;
		this.icon = icon;
		this.name = name;
		this.bonus = bonus;
		this.statMultiple = statMultiple;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public String getIcon() {
		return this.icon;
	}
	
	public int getBonus() {
		return this.bonus;
	}
	
	public double getStatMultiple() {
		return this.statMultiple;
	}
	
}
