package mk.plugin.sky2rpgcore.itemgui;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.power.SRPGPowerUtils;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.gui.enhance.GUIEnhance;
import mk.plugin.skycore.gui.gem.GUIGemAdd;
import mk.plugin.skycore.gui.gem.GUIGemDispart;
import mk.plugin.skycore.gui.gem.GUIGemDrill;
import mk.plugin.skycore.gui.grade.GUIGrade;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.stat.Stat;

public class SItemGUI {
	
	public static final int NANGBAC_SLOT = 10;
	public static final int CUONGHOA_SLOT = 11;
	public static final int KHAMNGOC_SLOT = 15;
	public static final int TACHNGOC_SLOT = 16;
	public static final int DUCLO_SLOT = 25;
	
	public static final String TITLE = "§2§lTIỆM RÈN TOÀN NĂNG";
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			Utils.sendSound(player, Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			
			inv.setItem(NANGBAC_SLOT, getNangBac());
			inv.setItem(CUONGHOA_SLOT, getCuongHoa());
			inv.setItem(KHAMNGOC_SLOT, getKhamNgoc());
			inv.setItem(TACHNGOC_SLOT, getTachNgoc());
			inv.setItem(DUCLO_SLOT, getDucLo());
		});
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		boolean canPress = true;
		
		if (!canPress) {
			player.sendMessage("§cVật phẩm không đúng, hãy để lại");
			return;
		}
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			switch (slot) {
				case NANGBAC_SLOT:
					GUIGrade.openGUI(player);
					break;
				case CUONGHOA_SLOT:
					GUIEnhance.openGUI(player);
					break;
				case DUCLO_SLOT:
					GUIGemDrill.openGUI(player);
					break;
				case KHAMNGOC_SLOT:
					GUIGemAdd.openGUI(player);
					break;
				case TACHNGOC_SLOT:
					GUIGemDispart.openGUI(player);
					break;
			}
		} 
		// Action on bottom inv
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
//				Utils.sendSound(player, Sound.BLOCK_LEVER_CLICK, 1f, 1f);
//				Inventory bInv = player.getOpenInventory().getBottomInventory();
//				ItemStack clickedItem = bInv.getItem(slot);
//				
//				// Item
//				if (ItemAPI.check(clickedItem)) {
//					Item si = ItemAPI.get(clickedItem);
//					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
//						player.sendMessage("§cĐã để Vật phẩm rồi");
//						return;
//					}
//					inv.setItem(INFO_SLOT_1, getInfo1(player, si.getData()));
//					inv.setItem(INFO_SLOT_2, getInfo2(player, si.getData()));
//					inv.setItem(INFO_SLOT_3, getInfo3(player, si.getData()));
//					inv.setItem(INFO_SLOT_4, getInfo4(player, si.getData()));
//				}				
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
//		Player player = (Player) e.getPlayer();
//		Inventory inv = e.getInventory();
//		if (!e.getView().getTitle().equals(TITLE)) return;
//		
//		if (!GUIUtils.checkCheck(player)) return;
//		
//		List<ItemStack> items = new ArrayList<ItemStack> ();
//		items.add(inv.getItem(ITEM_SLOT));
//		
//		items.forEach(item -> {
//			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
//			Utils.giveItem(player, item);
//		});
	}
	
	public static ItemStack getInfo1(Player player, ItemData si) {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 1");
		if (si == null) return item;
		String name = si.getName();
		String bac = si.getGrade().name();
		int level = si.getLevel();
		List<String> lore = Lists.newArrayList();
		lore.add("§aTên trang bị: §f" + name);
		lore.add("§aBậc trang bị: §f" + bac + "/V");
		lore.add("§aCấp độ trang bị: §f" + level + "/12");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getInfo2(Player player, ItemData si) {
		ItemStack item = new ItemStack(Material.APPLE);
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 2");
		if (si == null) return item;
		List<String> lore = Lists.newArrayList();
		for (Stat stat : Stat.values()) {
			lore.add("§a" + stat.getName() + ": §f" + si.getStat(stat) + " §7(+" + si.getBonusStat(player, stat) + ")");
		}
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getInfo3(Player player, ItemData si) {
		ItemStack item = new ItemStack(Material.BLAZE_POWDER);
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 3");
		if (si == null) return item;
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 3");
		List<String> lore = Lists.newArrayList();
		int power = 0;
		for (Stat stat : Stat.values()) power += SRPGPowerUtils.getPower(stat) * si.getCalculatedStat(player, stat);
		lore.add("§aLực chiến trang bị: §f" + power);
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getInfo4(Player player, ItemData si) {
		ItemStack item = new ItemStack(Material.BOOK);
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 4");
		if (si == null) return item;
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 4");
		List<String> lore = Lists.newArrayList();
		lore.add("§aĐể tăng sức mạnh trang bị bạn có thể: ");
		lore.add("§6 1. Nâng bậc");
		lore.add("§6 2. Cường hóa");
		lore.add("§6 3. Khảm ngọc");
		lore.add("§6 4. Phù phép");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getNangBac() {
		ItemStack item = new ItemStack(Material.WOOD_HOE, 1, (short) 4);
		ItemStackUtils.setDisplayName(item, "§a§lNâng bậc");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oKhi cường hóa trang bị lên cấp");
		lore.add("§7§otối đa, bạn có thể nâng trang bị");
		lore.add("§7§othêm 1 bậc (tối đa 5). Nâng bậc");
		lore.add("§7§ogia tăng giới hạn sức mạnh và");
		lore.add("§7§olàm trang bị bạn cool ngầu hơn =))");
		
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_UNBREAKABLE);
		ItemStackUtils.setUnbreakable(item, true);
		return item;
	}
	
	public static ItemStack getCuongHoa() {
		ItemStack item = new ItemStack(Material.WOOD_HOE, 1, (short) 5);
		ItemStackUtils.setDisplayName(item, "§a§lCường hóa");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oGiúp tăng các chỉ số cơ bản");
		lore.add("§7§ocủa trang bị với cấp cường hóa");
		lore.add("§7§otối đa là 12");
		
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_UNBREAKABLE);
		ItemStackUtils.setUnbreakable(item, true);
		return item;
	}
	
	public static ItemStack getKhamNgoc() {
		ItemStack item = new ItemStack(Material.WOOD_HOE, 1, (short) 6);
		ItemStackUtils.setDisplayName(item, "§a§lKhảm ngọc");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oKhảm ngọc vào trang bị giúp");
		lore.add("§7§ođa dạng hóa chỉ số và nâng");
		lore.add("§7§otrang bị của bạn lên tầm cao mới");
		
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_UNBREAKABLE);
		ItemStackUtils.setUnbreakable(item, true);
		return item;
	}
	
	public static ItemStack getTachNgoc() {
		ItemStack item = new ItemStack(Material.WOOD_HOE, 1, (short) 7);
		ItemStackUtils.setDisplayName(item, "§a§lTách ngọc");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oNếu hối hận vì đã khảm ngọc");
		lore.add("§7§othì bạn có thể tách ra và");
		lore.add("§7§okhảm viên ngọc mới vào");
		
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_UNBREAKABLE);
		ItemStackUtils.setUnbreakable(item, true);
		return item;
	}
	
	public static ItemStack getDucLo() {
		ItemStack item = new ItemStack(Material.WOOD_HOE, 1, (short) 8);
		ItemStackUtils.setDisplayName(item, "§a§lĐục lỗ");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oKhi bạn có nhiều ngọc mà");
		lore.add("§7§otrang bị không đủ ô trống thì");
		lore.add("§7§obạn có thể đục thêm");
		
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_UNBREAKABLE);
		ItemStackUtils.setUnbreakable(item, true);
		return item;
	}
	
}
