package mk.plugin.sky2rpgcore.specialmine;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import io.lumine.xikage.mythicmobs.MythicMobs;
import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.task.OreTask;

public class SMineUtils {
	
	public static ItemStack getItem(Material m) {
		String name = Utils.getOreName(m);
		ItemStack item = new ItemStack(m);
		if (m == Material.INK_SACK) item.setDurability((short) 4);
		ItemStackUtils.setDisplayName(item, name);
		ItemStackUtils.addLoreLine(item, "§7§oĐây là một quặng hiếm");
		return item;
	}
	
	public static Material getOre(Material m) { 
		switch (m) {
			case COAL_ORE: return Material.COAL;
			case IRON_ORE: return Material.IRON_INGOT;
			case GOLD_ORE: return Material.GOLD_INGOT;
			case DIAMOND_ORE: return Material.DIAMOND;
			case EMERALD_ORE: return Material.EMERALD;
			case LAPIS_ORE: return Material.INK_SACK;
			case REDSTONE_ORE: return Material.REDSTONE;
			case GLOWING_REDSTONE_ORE: return Material.REDSTONE;
			default: return null;
		}
	}
	
	public static void playerBreak(BlockBreakEvent e) {
		Block block = e.getBlock();
		Player player = e.getPlayer();
		if (OreTask.isBeingCooldown(block)) {
			e.setCancelled(true);
			return;
		}
		if (MainSkyCore.SPECIAL_MINE_WORLDS.contains(block.getWorld().getName())) {
			Material from = block.getType();
			Material ore = getOre(from);
			if (ore != null) {
				e.setCancelled(true);
				block.setType(Material.STONE);
				OreTask.set(block, 25000l, from);
				
				// Give
				ItemStack item = getItem(ore);
				player.getInventory().addItem(item);
				player.sendMessage("§7Nhận §a" + ItemStackUtils.getName(item).replace("§l", "") + "§7 x " + item.getAmount());
				player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
			}
		}
	}
	
	public static void checkMobSpawn(LivingEntity entity) {
		Bukkit.getScheduler().runTaskLater(MainSkyCore.get(), () -> {
			if (!MythicMobs.inst().getMobManager().getAllMythicEntities().contains(entity)) entity.remove();
		}, 5);
	}
	
}
