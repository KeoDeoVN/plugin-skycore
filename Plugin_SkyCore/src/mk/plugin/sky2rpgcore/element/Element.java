package mk.plugin.sky2rpgcore.element;

import java.util.Map;

import org.bukkit.Particle;

import com.google.common.collect.Maps;

import mk.plugin.skycore.stat.Stat;

public enum Element {
	
	KIM("Kim", "§7", Particle.CRIT_MAGIC) {
		@Override
		public Element getCounter() {
			return Element.HOA;
		}

		@Override
		public Map<Stat, Integer> getStats() {
			Map<Stat, Integer> stats = Maps.newHashMap();
			stats.put(Stat.SUC_THU, 1);
			stats.put(Stat.SAT_THUONG, 1);
			return stats;
		}
	},
	
	MOC("Mộc", "§a", Particle.VILLAGER_HAPPY) {
		@Override
		public Element getCounter() {
			return Element.KIM;
		}

		@Override
		public Map<Stat, Integer> getStats() {
			Map<Stat, Integer> stats = Maps.newHashMap();
			stats.put(Stat.HOI_PHUC, 1);
			stats.put(Stat.MAU, 2);
			return stats;
		}
	},
	
	THUY("Thủy", "§b", Particle.WATER_WAKE) {
		@Override
		public Element getCounter() {
			return Element.THO;
		}

		@Override
		public Map<Stat, Integer> getStats() {
			Map<Stat, Integer> stats = Maps.newHashMap();
			stats.put(Stat.HOI_PHUC, 2);
			stats.put(Stat.XUYEN_GIAP, 1);
			return stats;
		}
	},
	
	HOA("Hỏa", "§c", Particle.FLAME) {
		@Override
		public Element getCounter() {
			return Element.THUY;
		}

		@Override
		public Map<Stat, Integer> getStats() {
			Map<Stat, Integer> stats = Maps.newHashMap();
			stats.put(Stat.SAT_THUONG, 1);
			stats.put(Stat.HOI_PHUC, 1);
			return stats;
		}
	},
	
	THO("Thổ", "§6", Particle.CRIT) {
		@Override
		public Element getCounter() {
			return Element.MOC;
		}

		@Override
		public Map<Stat, Integer> getStats() {
			Map<Stat, Integer> stats = Maps.newHashMap();
			stats.put(Stat.MAU, 2);
			stats.put(Stat.SUC_THU, 1);
			return stats;
		}
	};
	
	
	private String name;
	private String color;
	private Particle p;
	
	private Element(String name, String color, Particle p) {
		this.name = name;
		this.color = color;
		this.p = p;
	}
	
	public abstract Map<Stat, Integer> getStats();
	public abstract Element getCounter();
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public Particle getParticle() {
		return this.p;
	}
	
}
