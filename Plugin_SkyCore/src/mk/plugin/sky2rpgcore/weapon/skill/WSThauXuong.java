package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.skill.SkillExecutor;
import mk.plugin.skycore.stat.Stat;
import net.minecraft.server.v1_12_R1.PacketPlayOutAnimation;

public class WSThauXuong implements SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		Player player = (Player) components.get("player");
		double damage = PlayerUtils.getStatValue(player, Stat.SAT_THUONG) * 0.5;
		
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i > 3) {
					this.cancel();
					return;
				}
				PacketPlayOutAnimation packet = new PacketPlayOutAnimation(((CraftPlayer) player).getHandle(), (byte) 0);
				((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
				Location mainLoc = player.getLocation().add(0, 0.9, 0);
				player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 0.2f, 1.1f);
				for (int i = 0 ; i < 10; i ++) {
					Location loc =  mainLoc.clone().add(mainLoc.getDirection().multiply(i));
					player.getWorld().spawnParticle(Particle.CRIT, loc, 4, 0.1f, 0.1f, 0.1f, 0);
					player.getWorld().spawnParticle(Particle.CRIT_MAGIC, loc, 4, 0.1f, 0.1f, 0.1f, 0);
					loc.getWorld().getNearbyEntities(loc, 1, 1, 1).forEach(e -> {
						if (e instanceof LivingEntity && e != player) {
							if (!Utils.canAttack(e)) return;
							Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(), () -> {
								Damages.damage(player, (LivingEntity) e, new Damage(damage, DamageType.SKILL), 5);
							});
						}
					});
				}
			}
			
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 10);
		
		
	}

}
