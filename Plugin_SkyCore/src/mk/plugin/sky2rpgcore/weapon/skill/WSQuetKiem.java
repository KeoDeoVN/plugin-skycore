package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.skill.SkillExecutor;
import mk.plugin.skycore.stat.Stat;

public class WSQuetKiem implements SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		Player player = (Player) components.get("player");
		double damage = PlayerUtils.getStatValue(player, Stat.SAT_THUONG);
		
		double minR = 0.2;
		int amount = 20;
		player.getWorld().playSound(player.getLocation(), Sound.ENTITY_PLAYER_ATTACK_CRIT, 1, 1);
		BukkitRunnable br = new BukkitRunnable() {
			
			int i = 0;
			
			@Override
			public void run() {
				for (int k = 3 * i ; k < 3 * (i+1) ; k++) {
					for (int j = 0 ; j < 20 ; j ++) {
						Location l = player.getLocation().clone();
						double angle = Math.PI * 2 / (amount + j * 0.2) * k * 1.5;
						
						double newX = l.getX() + (minR + j * 0.1) * Math.sin(angle + l.getYaw() * -1);
						double newZ = l.getZ() + (minR + j * 0.1) * Math.cos(angle + l.getYaw() * -1);
						
						l.setX(newX);
						l.setZ(newZ);
						l.setY(l.getY() + 1.0);
						
//						player.getWorld().spawnParticle(Particle.REDSTONE, l, 1, 0, 0, 0, 0, new Particle.DustOptions(Color.SILVER, 1));
						player.getWorld().spawnParticle(Particle.CRIT, l, 1, 0, 0, 0, 0);
						player.getWorld().spawnParticle(Particle.CRIT_MAGIC, l, 1, 0, 0, 0, 0);
					}
				}

				
				i++;
				if (i * 3 > amount) {
					this.cancel();
					Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(), () -> {
						player.getNearbyEntities(2, 2, 2).forEach(e -> {
							if (e != player && e instanceof LivingEntity) {
								LivingEntity le = (LivingEntity) e;
								if (!Utils.canAttack(e)) return;
								Damages.damage(player, (LivingEntity) le, new Damage(damage, DamageType.SKILL), 5);
							}
						});
					});
					return;
				}
			}
		};
		br.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 1);
	}

}
