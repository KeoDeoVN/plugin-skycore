package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.Map;

import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.skill.SkillExecutor;
import mk.plugin.skycore.stat.Stat;

public class WSDienTinh implements SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		Player player = (Player) components.get("player");
		double damage = PlayerUtils.getStatValue(player, Stat.SAT_THUONG);
		player.playSound(player.getLocation(), Sound.ENTITY_CAT_AMBIENT, 1, 1);
		player.getNearbyEntities(2.5, 1, 2.5).forEach(e -> {
			if (!(e instanceof LivingEntity)) return;
			LivingEntity le = (LivingEntity) e;
			if (!Utils.canAttack(e)) return;
			if (e instanceof Player) {
				effectPlayer((Player) e);
			} else effectEntity(le);
			if (e != player) {
				if (le.hasMetadata("NPC")) return;
				Damages.damage(player, (LivingEntity) le, new Damage(damage, DamageType.SKILL), 5);
			}
		});
	}
	
	public void effectPlayer(Player player) {
		int seconds = 3;
		player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 * seconds, 5));
		player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * seconds, 5));
		long start = System.currentTimeMillis();
		new BukkitRunnable() {
			@Override
			public void run() {
				if (System.currentTimeMillis() - start >= seconds * 1000) {
					this.cancel();
					return;
				}
				player.getWorld().spawnParticle(Particle.HEART, player.getLocation().add(0, 1, 0), 3, 0.3, 0.3, 0.3, 0.3);
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 5);
	}
	
	public void effectEntity(LivingEntity le) {
		int seconds = 4;
		le.setAI(false);
		World world = le.getWorld();
		long start = System.currentTimeMillis();
		new BukkitRunnable() {
			@Override
			public void run() {
				if (System.currentTimeMillis() - start >= seconds * 1000) {
					this.cancel();
					le.setAI(true);
					return;
				}
				world.spawnParticle(Particle.HEART, le.getLocation().add(0, 1, 0), 3, 0.3, 0.3, 0.3, 0.3);
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 5);
	}

}
