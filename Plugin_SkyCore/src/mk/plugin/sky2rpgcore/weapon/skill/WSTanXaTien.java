package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.skill.SkillExecutor;
import mk.plugin.skycore.stat.Stat;

public class WSTanXaTien implements  SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		Player player = (Player) components.get("player");
		int amount = 5;
		
		double angleBetweenArrows = (45 / (amount - 1)) * Math.PI / 180;
		double pitch = (player.getLocation().getPitch() + 90) * Math.PI / 180;
		double yaw = (player.getLocation().getYaw() + 90 - 45 / 2) * Math.PI / 180;
		double sZ = Math.cos(pitch);

		List<Arrow> as = new ArrayList<Arrow> ();
		
		for (int i = 0; i < amount; i++) { 	
			double nX = Math.sin(pitch)	* Math.cos(yaw + angleBetweenArrows * i);
			double nY = Math.sin(pitch)* Math.sin(yaw + angleBetweenArrows * i);
			Vector newDir = new Vector(nX, sZ, nY);

			Arrow arrow = player.launchProjectile(Arrow.class);
			arrow.setShooter(player);
			arrow.setVelocity(newDir.normalize().multiply(3f));
			arrow.setFireTicks(1000);
			as.add(arrow);
			Damages.setProjectileDamage(arrow, new Damage(PlayerUtils.getStatValue(player, Stat.SAT_THUONG), DamageType.ATTACK));
		}
		
		Utils.sendSound(player, Sound.ENTITY_ARROW_SHOOT, 1, 1);
		
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Arrow a : as) {
					a.remove();
				}
			}
		}.runTaskLater(MainSky2RPGCore.getMain(), 10);
	}

}
