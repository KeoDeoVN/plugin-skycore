package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.skill.SkillExecutor;
import mk.plugin.skycore.stat.Stat;
import net.minecraft.server.v1_12_R1.PacketPlayOutAnimation;

public class WSDapRiu implements SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		Player player = (Player) components.get("player");
		double damage = PlayerUtils.getStatValue(player, Stat.SAT_THUONG);
		
		player.spawnParticle(Particle.SWEEP_ATTACK, player.getLocation().add(0, 1.2, 0).add(player.getLocation().getDirection().multiply(1.2)), 1, 0, 0, 0, 0);
		player.getWorld().playSound(player.getLocation(), Sound.ENTITY_GHAST_SHOOT, 1f, 1f);
		
		PacketPlayOutAnimation packet = new PacketPlayOutAnimation(((CraftPlayer) player).getHandle(), (byte) 0);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		
		Location main = player.getLocation().add(player.getLocation().getDirection().multiply(1));
		Utils.getLivingEntities(player, main, 3, 3, 3).forEach(le -> {
			if (!Utils.canAttack(le)) return;
			Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(),() -> {
				Damages.damage(player, (LivingEntity) le, new Damage(damage, DamageType.SKILL), 5);
			});

			player.spawnParticle(Particle.CRIT_MAGIC, le.getLocation(), 10, 0.2, 0.2, 0.2, 1);
			player.spawnParticle(Particle.CRIT, le.getLocation(), 10, 0.2, 0.2, 0.2, 1);
			le.setVelocity(le.getLocation().subtract(player.getLocation()).toVector().normalize().multiply(1.2).setY(1));
		});
	}

}
