package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.skill.SkillExecutor;
import mk.plugin.skycore.stat.Stat;

public class WSXoayNuoc implements SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		Player player = (Player) components.get("player");
		double damage = PlayerUtils.getStatValue(player, Stat.SAT_THUONG);
		
		Location l = player.getLocation().clone().add(0, 1, 0);
		
		player.getWorld().playSound(l, Sound.ENTITY_PLAYER_SWIM, 1, 1);
		player.getWorld().playSound(l, Sound.ENTITY_PLAYER_SWIM, 1, 1);
		player.getWorld().playSound(l, Sound.ENTITY_PLAYER_SWIM, 1, 1); 
		new BukkitRunnable() {			
			int c = 0;
			@Override
			public void run() {
				c++;
				show(player, l.clone().add(l.getDirection().multiply(c * 0.6)), 1.5, l.getPitch() + 90, -1 * l.getYaw(), 0, c * 10).forEach(lc -> {
					player.getWorld().spawnParticle(Particle.REDSTONE, lc, 0, (double) 204 / 255, (double) 255 / 255, 255 / 255, 1);
					player.getWorld().spawnParticle(Particle.WATER_DROP, lc, 0, (double) 204 / 255, (double) 255 / 255, 255 / 255, 1);
					player.getWorld().spawnParticle(Particle.DRIP_WATER, lc, 0, (double) 204 / 255, (double) 255 / 255, 255 / 255, 1);
					
					for (Entity e : lc.getWorld().getNearbyEntities(lc, 1, 1, 1)) {
						if (e instanceof LivingEntity && e != player) {
							if (!Utils.canAttack(e)) continue;
							new BukkitRunnable() {
								@Override
								public void run() {
									Damages.damage(player, (LivingEntity) e, new Damage(damage, DamageType.SKILL), 15);
									
								}
							}.runTask(MainSky2RPGCore.getMain());
						}
					}
					
				});;
				if (c > 20) this.cancel();
			}
		}.runTaskTimerAsynchronously(MainSkyCore.get(), 0, 1);
		
	}
	

	public static List<Location> createCircle(Location location, double radius, double startAngle) {
		int amount = 9;
		double increment = (2 * Math.PI) / amount;
		double start = startAngle / 180 * Math.PI;
		ArrayList<Location> locations = new ArrayList<Location>();

		for (int i = 0; i < amount; i++) {
			double angle = start + i * increment;
			double x = location.getX() + (radius * Math.cos(angle));
			double z = location.getZ() + (radius * Math.sin(angle));
			locations.add(new Location(location.getWorld(), x, location.getY(), z));
		}

		return locations;
	}

	public List<Location> show(Player player, Location l, double r, double angleX, double angleY, double angleZ,
			double startAngle) {
		Location pl = l;
		Location c = l;
		List<Location> list = createCircle(pl, r, startAngle);
		List<Vector> list2 = Lists.newArrayList();
		for (int i = 0; i < list.size(); i++) {
			list2.add(list.get(i).clone().subtract(c.clone()).toVector().clone());
		}

		double sinX = Math.sin(Math.toRadians(angleX));
		double cosX = Math.cos(Math.toRadians(angleX));
		list2 = list2.stream().map(vec -> rotateAroundAxisX(vec, cosX, sinX)).collect(Collectors.toList());

		double sinY = Math.sin(Math.toRadians(angleY));
		double cosY = Math.cos(Math.toRadians(angleY));
		list2 = list2.stream().map(vec -> rotateAroundAxisY(vec, cosY, sinY)).collect(Collectors.toList());

		double sinZ = Math.sin(Math.toRadians(angleZ));
		double cosZ = Math.cos(Math.toRadians(angleZ));
		list2 = list2.stream().map(vec -> rotateAroundAxisZ(vec, cosZ, sinZ)).collect(Collectors.toList());

		for (int i = 0; i < list.size(); i++) {
			list.set(i, c.clone().add(list2.get(i).clone()));
		}

		return list;
	}

	public Vector rotateAroundAxisX(Vector v, double cos, double sin) {
		double y = v.getY() * cos - v.getZ() * sin;
		double z = v.getY() * sin + v.getZ() * cos;
		return v.setY(y).setZ(z);
	}

	public Vector rotateAroundAxisY(Vector v, double cos, double sin) {
		double x = v.getX() * cos + v.getZ() * sin;
		double z = v.getX() * -sin + v.getZ() * cos;
		return v.setX(x).setZ(z);
	}

	public Vector rotateAroundAxisZ(Vector v, double cos, double sin) {
		double x = v.getX() * cos - v.getY() * sin;
		double y = v.getX() * sin + v.getY() * cos;
		return v.setX(x).setY(y);
	}

}
