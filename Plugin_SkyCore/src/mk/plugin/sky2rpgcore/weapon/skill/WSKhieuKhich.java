package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.Map;

import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.skill.SkillExecutor;

public class WSKhieuKhich implements SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		Player player = (Player) components.get("player");
		player.getNearbyEntities(5, 5, 5).forEach(entity -> {
			if (!Utils.canAttack(entity)) return;
			if (entity instanceof Monster) {
				((Monster) entity).setTarget(player);
			}
		});
	}

}
