package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.skill.SkillExecutor;
import mk.plugin.skycore.stat.Stat;

public class WSThienPhat implements SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		Player player = (Player) components.get("player");
		double damage = PlayerUtils.getStatValue(player, Stat.SAT_THUONG);
		
		new BukkitRunnable () {
			Location lo = player.getLocation();
			int i = 0;
			@Override
			public void run() {
				if (i == 4) {
					this.cancel();
				}
				i++;
				int j = 0;
				Location temp = lo.add(lo.getDirection().multiply(i * 0.8)).clone();
				while (temp.getBlock().getType() == Material.AIR) {
					j++;
					if (j > 10) {
						this.cancel();
						break;
					}
					temp = temp.add(0,-1,0);
				}
				player.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, temp.clone().add(0, 0.7, 0), 1, 0, 0, 0, 0);
				
				final Location tempL = temp;
				
				new BukkitRunnable () {
					@Override
					public void run() {
						player.getWorld().strikeLightningEffect(tempL);
					}
				}.runTaskAsynchronously(MainSky2RPGCore.getMain());

				new BukkitRunnable() {
					@Override
					public void run() {
						for (Entity e : tempL.getWorld().getNearbyEntities(tempL, 2, 10, 2)) {
							if (e instanceof LivingEntity && e != player) {
								if (!Utils.canAttack(e)) continue;
								Damages.damage(player, (LivingEntity) e, new Damage(damage, DamageType.SKILL), 5);
							}
						}
					}
				}.runTask(MainSky2RPGCore.getMain());

			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 5);
	}

}
