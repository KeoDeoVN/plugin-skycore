package mk.plugin.skycore.item;

import java.util.List;
import java.util.Map;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.sky2rpgcore.tier.Tier;
import mk.plugin.skycore.grade.Grade;
import mk.plugin.skycore.itemcategory.Armor;
import mk.plugin.skycore.itemcategory.Jewelry;
import mk.plugin.skycore.itemcategory.Weapon;
import mk.plugin.skycore.itemgenerate.ItemGenerates;
import mk.plugin.skycore.itemtexture.ItemTexture;
import mk.plugin.skycore.jewelry.JewelryCategory;
import mk.plugin.skycore.passive.Passive;
import mk.plugin.skycore.skill.Skill;
import mk.plugin.skycore.stat.Stat;
import mk.plugin.skycore.weapon.shooter.ArrowShooter;

public enum ItemType {

	/*
	 * Weapon: SAT_THUONG, TOC_DANH, CHI_MANG, XUYEN_GIAP
	 */

	// VIP
	GAY("Gậy", "Công cụ lợi hại khi chọc chó với tầm xa hơn thường",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(31, 32, 33, 34, 35), Skill.KHIEU_KHICH, false, 3),
			ItemRank.VIP, Weapon.getStats(),
			Lists.newArrayList(1.5d, 1d, 1d, 1d)),
	
	KIEM("Kiếm", "Vũ khí cận chiến phổ thông với sức mạnh cân bằng",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(1, 2, 3, 4, 5), Skill.QUET_KIEM, false, 2.5),
			ItemRank.VIP, Weapon.getStats(),
			Lists.newArrayList(1d, 1d, 1d, 1d)),
	
	CUNG("Cung", "Vũ khí nhẹ tầm xa với đặc điểm tốc độ đánh cao",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(11, 12, 13, 14, 15), Skill.TAN_XA_TIEN, true, 0,
					new ArrowShooter()),
			ItemRank.VIP, Weapon.getStats(),
			Lists.newArrayList(1d, 1.5d, 1d, 0.5d)),
	
	RIU("Rìu", "Vũ khí cận chiến hạng nặng, gây nhiều sát thương",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(6, 7, 8, 9, 10), Skill.DAP_VANG, false, 2.5),
			ItemRank.VIP, Weapon.getStats(),
			Lists.newArrayList(1.5d, 1d, 1d, 0.5d)),

	// WOW
	THUONG_THU("Thương thủ", "Với độ dài và sự sắc nhọn, vũ khí có sức mạnh đáng sợ",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(16, 17, 18, 19, 20), Skill.THAU_XUONG, false, 5),
			ItemRank.WOW, Weapon.getStats(),
			Lists.newArrayList(1.5d, 0.75d, 1.25d, 1d)),
	
	THIEN_KIEM("Thiên kiếm", "Tên đầy đủ là Thuận thiên kiếm với truyền thuyết oai hùng",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(36, 37, 38, 39, 40), Skill.THIEN_PHAT, false, 3),
			ItemRank.WOW, Weapon.getStats(),
			Lists.newArrayList(1.75d, 1.25d, 0.5d, 0.5d)),
	
	DINH_BA("Đinh ba", "Trang bị thống trị đại dương và thành phố dưới biển Atlantic",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(46, 47, 48, 49, 50), Skill.XOAY_NUOC, false, 5),
			ItemRank.WOW, Weapon.getStats(),
			Lists.newArrayList(1.75d, 1.25d, 0.5d, 0.5d)),
	
	ANH_TRANG("Ánh trăng", "Vũ khí được tinh luyện bởi mảnh trăng với sức mạnh bóng tối của nó",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(51, 51, 52, 52, 53), Skill.MAN_DEM, false, 4),
			ItemRank.WOW, Weapon.getStats(),
			Lists.newArrayList(1.5d, 0.75d, 1.25d, 1d)),
	
	

	// SUPER
	LUOI_HAI("Lưỡi hái", "Công cụ của tử thần để xét xử kẻ đáng tội chết",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(21, 22, 23, 24, 25), Skill.CHIA_CAT, false, 5),
			ItemRank.SUPER, Weapon.getStats(),
			Lists.newArrayList(1.25d, 0.75d, 1.25d, 0.75d)),
	
	KATANA("Katana", "Với sự rèn rũa tỉ mỉ, lưỡi kiếm có thể kết liễu kẻ địch trong một nốt nhạc",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(26, 27, 28, 29, 30), Skill.BAO_KIEM, false, 4),
			ItemRank.SUPER, Weapon.getStats(),
			Lists.newArrayList(1.75d, 0.5d, 1d, 0.75d)),
	
	AI_KIEM("Ái kiếm", "Đã từng là trang bị của thần, với sự hấp dẫn và chết chóc của tình yêu",
			new Weapon(Material.WOOD_SWORD, Lists.newArrayList(41, 42, 43, 44, 45), Skill.LUOI_TINH, false, 3),
			ItemRank.SUPER, Weapon.getStats(),
			Lists.newArrayList(1.5d, 0.75d, 1d, 0.75d)),

	/*
	 * Jewelry
	 */

	// VIP
	VINH_CUU("Vĩnh cửu", "Những sợi xích được tinh luyện qua phép thuật",
			new Jewelry(Material.WOOD_AXE, Lists.newArrayList(11, 12, 13, 14, 15), Passive.HOI_PHUC, JewelryCategory.BRACELET),
			ItemRank.VIP, Lists.newArrayList(Stat.SAT_THUONG, Stat.HOI_PHUC), Lists.newArrayList(0.75d, 0.75d)),

	LAM_MOC("Lam mộc", "Chứa đựng khí chất của mộc",
			new Jewelry(Material.WOOD_AXE, Lists.newArrayList(21, 22, 23, 24, 25), Passive.BANG_GIA, JewelryCategory.RING),
			ItemRank.VIP, Lists.newArrayList(Stat.SAT_THUONG, Stat.HOI_PHUC), Lists.newArrayList(0.75d, 0.75d)),
	
	
	// WOW
	NHAN_CUOI("Nhẫn cưới", "Vật chứng minh cho tình yêu đôi lứa",
			new Jewelry(Material.WOOD_AXE, Lists.newArrayList(1, 2, 3, 4, 5), Passive.HON_UOC, JewelryCategory.RING),
			ItemRank.WOW, Lists.newArrayList(Stat.SAT_THUONG, Stat.HOI_PHUC), Lists.newArrayList(0.75d, 0.75d)),
	
	AC_MA("Ác ma", "Đồ đeo tay của ác quỷ",
			new Jewelry(Material.WOOD_AXE, Lists.newArrayList(6, 7, 8, 9, 10), Passive.KHO_HEO, JewelryCategory.BRACELET),
			ItemRank.WOW, Lists.newArrayList(Stat.SAT_THUONG, Stat.HOI_PHUC), Lists.newArrayList(0.75d, 0.75d)),
	
	NHAN_BAC("Nhẫn bạc", "Được tinh luyện từ bạc nguyên chất",
			new Jewelry(Material.WOOD_AXE, Lists.newArrayList(16, 17, 18, 19, 20), Passive.NHANH_NHEN, JewelryCategory.RING),
			ItemRank.WOW, Lists.newArrayList(Stat.SAT_THUONG, Stat.HOI_PHUC), Lists.newArrayList(0.75d, 0.75d)),
	
	
	
	
	/*
	 * Armor
	 */

	// VIP
	
	// giapVai
	MU_VAI("Mũ vải", "Được làm từ vải với độ nhanh nhẹn cao",
			new Armor(new ItemTexture(Material.LEATHER_HELMET), "giapVai"),
			ItemRank.VIP, Armor.getStats(), Lists.newArrayList(1d, 0.5d, 1.5d)),
	
	AO_VAI("Áo vải", "Được làm từ vải với độ nhanh nhẹn cao",
			new Armor(new ItemTexture(Material.LEATHER_CHESTPLATE), "giapVai"),
			ItemRank.VIP, Armor.getStats(), Lists.newArrayList(1d, 0.5d, 1.5d)),
	
	QUAN_VAI("Quần vải", "Được làm từ vải với độ nhanh nhẹn cao",
			new Armor(new ItemTexture(Material.LEATHER_LEGGINGS), "giapVai"),
			ItemRank.VIP, Armor.getStats(), Lists.newArrayList(1d, 0.5d, 1.5d)),
	
	UNG_VAI("Ủng vải", "Được làm từ vải với độ nhanh nhẹn cao",
			new Armor(new ItemTexture(Material.LEATHER_BOOTS), "giapVai"),
			ItemRank.VIP, Armor.getStats(), Lists.newArrayList(1d, 0.5d, 1.5d)),
	
	// giapSat
	MU_SAT("Mũ sắt", "Được làm từ sắt với sức phòng thủ cao",
			new Armor(new ItemTexture(Material.IRON_HELMET), "giapSat"),
			ItemRank.VIP, Armor.getStats(), Lists.newArrayList(1d, 1.5d, 0.5d)),
	
	AO_SAT("Áo sắt", "Được làm từ sắt với sức phòng thủ cao",
			new Armor(new ItemTexture(Material.IRON_CHESTPLATE), "giapSat"),
			ItemRank.VIP, Armor.getStats(), Lists.newArrayList(1d, 1.5d, 0.5d)),
	
	QUAN_SAT("Quần sắt", "Được làm từ sắt với sức phòng thủ cao",
			new Armor(new ItemTexture(Material.IRON_LEGGINGS), "giapSat"),
			ItemRank.VIP, Armor.getStats(), Lists.newArrayList(1d, 1.5d, 0.5d)),
	
	UNG_SAT("Ủng sắt", "Được làm từ sắt với sức phòng thủ cao",
			new Armor(new ItemTexture(Material.IRON_BOOTS), "giapSat"),
			ItemRank.VIP, Armor.getStats(), Lists.newArrayList(1d, 1.5d, 0.5d)),
	
	// giapLuoi
	MU_LUOI("Mũ lưới", "Kết hợp giữa sự chắc chắn và nhanh nhẹn",
			new Armor(new ItemTexture(Material.CHAINMAIL_HELMET), "giapLuoi"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	AO_LUOI("Áo lưới", "Kết hợp giữa sự chắc chắn và nhanh nhẹn",
			new Armor(new ItemTexture(Material.CHAINMAIL_CHESTPLATE), "giapLuoi"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	QUAN_LUOI("Quần lưới", "Kết hợp giữa sự chắc chắn và nhanh nhẹn",
			new Armor(new ItemTexture(Material.CHAINMAIL_LEGGINGS), "giapLuoi"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	UNG_LUOI("Ủng lưới", "Kết hợp giữa sự chắc chắn và nhanh nhẹn",
			new Armor(new ItemTexture(Material.CHAINMAIL_BOOTS), "giapLuoi"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.75d, 0.5d, 0.75d)),
	
	// giapVang
	MU_VANG("Mũ vàng", "Với sự cứng cáp của vàng, đem lại sức phòng thủ lớn",
			new Armor(new ItemTexture(Material.GOLD_HELMET), "giapVang"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(1d, 0.75d, 0.25d)),
	
	AO_VANG("Áo vàng", "Với sự cứng cáp của vàng, đem lại sức phòng thủ lớn",
			new Armor(new ItemTexture(Material.GOLD_CHESTPLATE), "giapVang"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(1d, 0.75d, 0.25d)),
	
	QUAN_VANG("Quần vàng", "Với sự cứng cáp của vàng, đem lại sức phòng thủ lớn",
			new Armor(new ItemTexture(Material.GOLD_LEGGINGS), "giapVang"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(1d, 0.75d, 0.25d)),
	
	UNG_VANG("Ủng vàng", "Với sự cứng cáp của vàng, đem lại sức phòng thủ lớn",
			new Armor(new ItemTexture(Material.GOLD_BOOTS), "giapVang"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(1d, 0.75d, 0.25d)),
	
	// giapKimCuong
	MU_KIM_CUONG("Mũ kim cương", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.DIAMOND_HELMET), "giapKimCuong"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	AO_KIM_CUONG("Áo kim cương", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.DIAMOND_CHESTPLATE), "giapKimCuong"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	QUAN_KIM_CUONG("Quần kim cương", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.DIAMOND_LEGGINGS), "giapKimCuong"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	UNG_KIM_CUONG("Ủng kim cương", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.DIAMOND_BOOTS), "giapKimCuong"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	// setPirate
	MU_PIRATE("Mũ Pirate", "Trang phục của cướp biển với sự nhanh nhẹn cao",
			new Armor(new ItemTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGFmOGRjMjg3ZTE5ODk1NTdjOTBlMTQwMjkxNjgwYjAxNWI5YWNiNjNlYWY3MThjMTYwNmQ2Y2ZhZGI0Njk4In19fQ=="), "setPirate"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.4d, 0.3d, 1.25d)),
	
	AO_PIRATE("Áo Pirate", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.LEATHER_CHESTPLATE, Color.fromRGB(210, 210, 210)), "setPirate"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	QUAN_PIRATE("Quần Pirate", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.LEATHER_LEGGINGS, Color.fromRGB(44, 44, 44)), "setPirate"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	UNG_PIRATE("Ủng Pirate", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.LEATHER_BOOTS, Color.fromRGB(44, 44, 44)), "setPirate"),
			ItemRank.WOW, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	// setNinja
	MU_NINJA("Mũ Ninja", "Bộ đồ của ninja với sự tinh tế của những người may mặc xưa",
			new Armor(new ItemTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmY3MzMyNjk2MTRmZThhOWFlODlmNWM1ODI4NmFhNTNiOWUzODNmYzRkYzU3ZTM3ZWNiZmE1ZTkzYSJ9fX0="), "setNinja"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(0.5d, 0.25d, 1.25d)),
	
	AO_NINJA("Áo Ninja", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.LEATHER_CHESTPLATE, Color.fromRGB(79, 0, 102)), "setNinja"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	QUAN_NINJA("Quần Ninja", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.LEATHER_LEGGINGS, Color.fromRGB(79, 0, 102)), "setNinja"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	UNG_NINJA("Ủng Ninja", "Chắc chắn nhưng không quá nặng mang lại độ nhanh nhẹn cho người mặc",
			new Armor(new ItemTexture(Material.LEATHER_BOOTS, Color.fromRGB(213, 213, 213)), "setNinja"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(0.5d, 0.75d, 0.75d)),
	
	// setDragon
	MU_DRAGON("Mũ Dragon", "Được tạo ra bởi da của rồng lửa đã chết trong cuộc chiến với các dragon slayder",
			new Armor(new ItemTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjMDgwZDg0MjFjYThjNTU5ZDEyYzIzMjhmMWI4OWM4MmZhMGY1YjY5OTYwMjIxYjg2MTNiYjhkOTU4ZDUifX19"), "setDragon"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(1d, 0.75d, 0.25d)),
	
	AO_DRAGON("Áo Dragon", "Được tạo ra bởi da của rồng lửa đã chết trong cuộc chiến với các dragon slayder",
			new Armor(new ItemTexture(Material.LEATHER_CHESTPLATE, Color.fromRGB(46, 3, 3)), "setDragon"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(1d, 0.75d, 0.25d)),
	
	QUAN_DRAGON("Áo Dragon", "Được tạo ra bởi da của rồng lửa đã chết trong cuộc chiến với các dragon slayder",
			new Armor(new ItemTexture(Material.LEATHER_LEGGINGS, Color.fromRGB(249, 108, 0)), "setDragon"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(1d, 0.75d, 0.25d)),
	
	UNG_DRAGON("Áo Dragon", "Được tạo ra bởi da của rồng lửa đã chết trong cuộc chiến với các dragon slayder",
			new Armor(new ItemTexture(Material.LEATHER_BOOTS, Color.fromRGB(249, 108, 0)), "setDragon"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(1d, 0.75d, 0.25d)),
	
	// setSamurai
	MU_SAMURAI("Mũ Samurai", "Bộ giáp trải qua bao trận chiến, cứu chủ nhân nó khỏi cái chết nhiều lần",
			new Armor(new ItemTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjY3YmYwNjlmZWZiNDBiZTIyNzI0YjAyZTZjNGZiZTIxMzNlZjVlMTEyYmM1NTFhNGYwMDQyZWE5OWRjZjZhMiJ9fX0="), "setSamurai"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(0.5d, 1.25d, 0.25d)),
	
	AO_SAMURAI("Áo Samurai", "Bộ giáp trải qua bao trận chiến, cứu chủ nhân nó khỏi cái chết nhiều lần",
			new Armor(new ItemTexture(Material.LEATHER_CHESTPLATE, Color.fromRGB(0, 28, 104)), "setSamurai"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(0.5d, 1.25d, 0.25d)),
	
	QUAN_SAMURAI("Quần Samurai", "Bộ giáp trải qua bao trận chiến, cứu chủ nhân nó khỏi cái chết nhiều lần",
			new Armor(new ItemTexture(Material.LEATHER_LEGGINGS, Color.fromRGB(1, 8, 28)), "setSamurai"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(0.5d, 1.25d, 0.25d)),
	
	UNG_SAMURAI("Ủng Samurai", "Bộ giáp trải qua bao trận chiến, cứu chủ nhân nó khỏi cái chết nhiều lần",
			new Armor(new ItemTexture(Material.LEATHER_BOOTS, Color.fromRGB(1, 8, 28)), "setSamurai"),
			ItemRank.SUPER, Armor.getStats(), Lists.newArrayList(0.5d, 1.25d, 0.25d)),
	
	;

	private String basicName;
	private String basicDesc;
	
	// Sum: size * 1
	private Map<Stat, Double> basicStatScales;
	private ItemCategory category;
	private ItemRank rank;

	private ItemType(String basicName, String basicDesc, ItemCategory category, ItemRank rank, List<Stat> stats, List<Double> statScales) {
		this.basicName = basicName;
		this.basicDesc = basicDesc;
		this.category = category;
		this.rank = rank;
		this.basicStatScales = Maps.newHashMap();
		for (int i = 0; i < stats.size(); i++) {
			basicStatScales.put(stats.get(i), statScales.get(i));
		}
	}

	public String getBasicName() {
		return this.basicName;
	}

	public String getBasicDesc() {
		return this.basicDesc;
	}

	public Map<Stat, Integer> getBasicStats() {
		Map<Stat, Integer> map = Maps.newHashMap();
		this.basicStatScales.forEach((stat, scale) -> {
			map.put(stat, new Double(scale * this.getRank().getStatMultiple()).intValue());
		});
		return map;
	}

	public ItemCategory getCategory() {
		return this.category;
	}

	public ItemRank getRank() {
		return this.rank;
	}

	
	public ItemStack createItemStack(Player player) {
		ItemData data = new ItemData(this.basicName, this.basicDesc, 0, 2, ItemGenerates.generateStats(this.getBasicStats(), Tier.TRUNG_CAP), Lists.newArrayList(), 0, Tier.TRUNG_CAP, Grade.I.getDurability());
		Item item = new Item(this, data);
		ItemStack is = new ItemStack(Material.STONE);
		return ItemAPI.set(player, is, item);
	}

}
