package mk.plugin.skycore.item;

import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.sky2rpgcore.tier.Tier;
import mk.plugin.skycore.grade.Grade;
import mk.plugin.skycore.grade.GradeUtils;
import mk.plugin.skycore.item.gem.Gem;
import mk.plugin.skycore.stat.Stat;

public class ItemData {
	
	private String name;
	private String desc;
	private int level;
	private int gemHole;
	private Map<Stat, Integer> stats;
	private List<Gem> gems;
	private int gradeExp;
	private Tier tier;
	
	private int durability;
	
	/*
	 * Constructor
	 */
	
	public ItemData() {
		this.name = "Name";
		this.desc = "Desc";
		this.level = 0;
		this.gemHole = 0;
		this.stats = Maps.newHashMap();
		this.gems = Lists.newArrayList();
		this.gradeExp = 0;
	}
	
	public ItemData(String name, String desc, int level, int gemHole, Map<Stat, Integer> stats, List<Gem> gems, int gradeExp, Tier tier, int durability) {
		this.name = name;
		this.desc = desc;
		this.level = level;
		this.gemHole = gemHole;
		this.stats = stats;
		this.gems = gems;
		this.gradeExp = gradeExp;
		this.tier = tier;
		this.durability = durability;
	}
	
	
	/*
	 * Getters
	 */
	
	public String getName() {
		return this.name;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public int getGradeExp() {
		return this.gradeExp;
	}
	
	public Tier getTier() {
		return this.tier;
	}
	
	public Grade getGrade() {
		Grade r = Grade.I;
		for (Grade g : Grade.values()) {
			if (GradeUtils.getExpTo(g) <= this.gradeExp) r = g;
		}
		return r;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public int getCalculatedStat(Player player, Stat stat) {
		int value = 0;
		if (this.stats.containsKey(stat)) value += getStat(stat);
		if (value > 0) value += 2 * this.level;
		
		// Grade
		value *= this.getGrade().getBonus();
		
		// Gem
		for (Gem gem : gems) {
			if (gem.getStat().equals(stat)) {
				value *= (double) (100 + gem.getValue()) / 100;
			}
		}
		
		return value;
	}
	
	public int getBonusStat(Player player, Stat stat) {
		if (getStat(stat) == 0) return 0;
		return getCalculatedStat(player, stat) - getStat(stat);
	}
	
	public int getStat(Stat stat) {
		return this.stats.getOrDefault(stat, 0);
	}
	
	public Map<Stat, Integer> getStats() {
		return this.stats;
	}
	
	public List<Gem> getGems() {
		return this.gems;
	}
	
	public int getGemHole() {
		return this.gemHole;
	}
	
	public int getBlankGemHole() {
		return this.gemHole - this.gems.size();
	}
	
	public int getDurability() {
		return this.durability;
	}
	
	/*
	 * Setters
	 */
	
	public void setGradeExp(int gradeExp) {
		this.gradeExp = gradeExp;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public void setGrade(Grade grade) {
		this.gradeExp = Math.max(GradeUtils.getExpTo(grade), this.gradeExp);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public void setStats(Map<Stat, Integer> stats) {
		this.stats = stats;
	}
	
	public void setGems(List<Gem> gems) {
		this.gems = gems;
	}
	
	public void addGem(Gem gem) {
		this.gems.add(gem);
	}
	
	public void removeGem(Gem gem) {
		this.gems.remove(gem);
	}
	
	public void setGemHole(int gemHole) {
		this.gemHole = gemHole;
	}
	
	public void setTier(Tier tier) {
		this.tier = tier;
	}
	
	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	/*
	 * Methods
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		JSONObject jo = new JSONObject();
		
		// Single values
		jo.put("name", this.name);
		jo.put("desc", this.desc);
		jo.put("level", this.level);
		jo.put("gemHole", this.gemHole);
		jo.put("gradeExp", this.gradeExp);
		jo.put("tier", this.tier.name());
		jo.put("durability", this.durability);
		
		// List
		JSONArray gems = new JSONArray();
		this.gems.forEach(gem -> gems.add(gem.toString()));
		
		// Map
		JSONObject stats = new JSONObject();
		stats.putAll(this.stats);
		
		// ...
		jo.put("gems", gems);
		jo.put("stats", stats);
		
		return jo.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static ItemData parse(String s) {
		JSONObject jo = (JSONObject) JSONValue.parse(s);
		
		// Single values
		String name = (String) jo.getOrDefault("name", "Item");
		String desc = (String) jo.getOrDefault("desc", "This is description");
		int level = new Long((Long) jo.getOrDefault("level", 0)).intValue();
		int gemHole = new Long((Long) jo.getOrDefault("gemHole", 0)).intValue();
		int gradeExp = new Long((Long) jo.getOrDefault("gradeExp", 0)).intValue();
		Tier tier = Tier.valueOf((String) jo.getOrDefault("tier", "SO_CAP"));
		int durability = new Long((Long) jo.getOrDefault("durability", new Integer(Grade.I.getDurability()).longValue())).intValue(); 
		
		// List
		List<Gem> gems = Lists.newArrayList();
		((JSONArray) jo.get("gems")).forEach(gs -> gems.add(Gem.parse((String) gs)));
		
		// Map
		Map<String, Long> tstats = (JSONObject) jo.get("stats");
		
		Map<Stat, Integer> stats = Maps.newHashMap();
		tstats.forEach((k, v) -> {
			stats.put(Stat.valueOf(k), v.intValue());
		});
		
		return new ItemData(name, desc, level, gemHole, stats, gems, gradeExp, tier, durability);
	}

	
	
	
	
	
}
