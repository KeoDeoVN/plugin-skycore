package mk.plugin.skycore.item;

public enum ItemRank {
	
	VIP(10),
	WOW(15),
	SUPER(25),
	LEGEND(35);
	
	private double statMultiple;
	
	private ItemRank(double statMultiple) {
		this.statMultiple = statMultiple;
	}
	
	public double getStatMultiple() {
		return this.statMultiple;
	}
	
}
