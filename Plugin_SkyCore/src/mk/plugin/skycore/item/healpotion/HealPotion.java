package mk.plugin.skycore.item.healpotion;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public enum HealPotion {
	
	THUONG("Thường", 5),
	TRUNG("Trung", 10),
	CAO("Cao", 20);
	
	private int health;
	private String name;
	
	private HealPotion(String name, int health) {
		this.name = name;
		this.health = health;
	}
	
	public int getHealth() {
		return this.health;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ItemStack createItem() {
		ItemStack item = new ItemStack(Material.DRAGONS_BREATH);
		ItemStackUtils.setDisplayName(item, "§a§lThuốc hồi phục §6§l" + this.name);
		ItemStackUtils.addLoreLine(item, "§f+ " + this.health + " HP");
		return ItemStackUtils.setTag(item, "sRPG.thuochoiphuc", this.name());
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "sRPG.thuochoiphuc");
	}
	
	public static HealPotion fromItem(ItemStack item) {
		if (!isThatItem(item)) return null;
		return HealPotion.valueOf(ItemStackUtils.getTag(item, "sRPG.thuochoiphuc"));
	}
	
	public static boolean canUse(Location location) {
		return true;
	}
	
}
