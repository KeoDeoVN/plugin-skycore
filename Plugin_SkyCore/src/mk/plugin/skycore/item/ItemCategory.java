package mk.plugin.skycore.item;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class ItemCategory {
	
	public abstract void lastModify(Player player, ItemStack item, ItemData data);
	
}
