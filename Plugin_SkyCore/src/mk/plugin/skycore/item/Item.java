package mk.plugin.skycore.item;

public class Item {
	
	private ItemType type;
	private ItemData data;
	
	public Item(ItemType type, ItemData data) {
		this.type = type;
		this.data = data;
	}
	
	public ItemType getType() {
		return this.type;
	}
	
	public ItemData getData() {
		return this.data;
	}
	
}
