package mk.plugin.skycore.item.luckyamulet;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import mk.plugin.skycore.itemtexture.ItemTexture;

public enum LuckyAmulet {
	
	LUCKY_I("§7§lBùa may mắn I", 5, new ItemTexture(Material.INK_SACK, 10)),
	LUCKY_II("§9§lBùa may mắn II", 50, new ItemTexture(Material.INK_SACK, 11)),
	LUCKY_III("§c§lBùa may mắn III", 100, new ItemTexture(Material.INK_SACK, 12));
	
	private String name;
	private int bonus;
	private ItemTexture texture;
	
	private LuckyAmulet(String name, int bonus, ItemTexture texture) {
		this.name = name;
		this.bonus = bonus;
		this.texture = texture;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getBonus() {
		return this.bonus;
	}
	
	public ItemTexture getItemTexture() {
		return this.texture;
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(this.getItemTexture().getMaterial(), 1, (short) this.getItemTexture().getDurability());
		ItemStackUtils.setDisplayName(item, this.getName());
		ItemStackUtils.addEnchantEffect(item);
		ItemStackUtils.addLoreLine(item, "§7§oGiúp thực thi thất bại không bị");
		ItemStackUtils.addLoreLine(item, "§7§omất vật phẩm và tăng " + this.bonus + "% thành công");
		item = ItemStackUtils.setTag(item, "sRPG.sachmayman", this.name());
		
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "sRPG.sachmayman");
	}
	
	public static LuckyAmulet fromItem(ItemStack item) {
		if (!isThatItem(item)) return null;
		return LuckyAmulet.valueOf(ItemStackUtils.getTag(item, "sRPG.sachmayman"));
	}
	
}
