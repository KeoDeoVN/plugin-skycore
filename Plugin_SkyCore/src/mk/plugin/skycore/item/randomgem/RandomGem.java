package mk.plugin.skycore.item.randomgem;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.tier.Tier;

public enum RandomGem {
	
	RANDOM_I("§7§lNgọc ngẫu nhiên I", 10, 2.5, 0.5, 0.01),
	RANDOM_II("§b§lNgọc ngẫu nhiên II", 20, 4, 1, 0.15),
	RANDOM_III("§c§lNgọc ngẫu nhiên III", 40, 8, 2, 0.25),
	RANDOM_IV("§6§lNgọc ngẫu nhiên IV", 50, 15, 5, 0.5),
	RANDOM_V("§a§lNgọc ngẫu nhiên V", 50, 25, 10, 1);
	
	private String name;
	private Map<Tier, Double> chances;
	
	private RandomGem(String name, double trungcap, double caocap, double cucpham, double huyenthoai) {
		this.name = name;
		chances = Maps.newHashMap();
		chances.put(Tier.SO_CAP, 100 - trungcap - caocap - cucpham - huyenthoai);
		chances.put(Tier.TRUNG_CAP, trungcap);
		chances.put(Tier.CAO_CAP, caocap);
		chances.put(Tier.CUC_PHAM, cucpham);
		chances.put(Tier.HUYEN_THOAI, huyenthoai);
	}
	
	public String getName() {
		return this.name;
	}
	
	public Tier rate() {
		for (Entry<Tier, Double> chances : chances.entrySet()) {
			if (Utils.rate(chances.getValue())) return chances.getKey();
		}
		return Tier.SO_CAP;
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(Material.IRON_NUGGET);
		ItemStackUtils.setDisplayName(item, this.getName());
		ItemStackUtils.addLoreLine(item, "§7§oBấm chuột phải để mở ra ngọc ngẫu nhiên");
		ItemStackUtils.addEnchantEffect(item);
		item = ItemStackUtils.setTag(item, "sRPG.rg", this.name());
		
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "sRPG.rg");
	}
	
	public static RandomGem fromItem(ItemStack item) {
		if (!isThatItem(item)) return null;
		return RandomGem.valueOf(ItemStackUtils.getTag(item, "sRPG.rg"));
	}
	
}
