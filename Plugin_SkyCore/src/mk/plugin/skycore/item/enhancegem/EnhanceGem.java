package mk.plugin.skycore.item.enhancegem;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public enum EnhanceGem {
	
	CAP_1("§7§lĐá cường hóa I", 1, Material.INK_SACK, 5),
	CAP_2("§9§lĐá cường hóa II", 2, Material.INK_SACK, 6),
	CAP_3("§c§lĐá cường hóa III", 3, Material.INK_SACK, 7),
	CAP_4("§6§lĐá cường hóa IV", 4,  Material.INK_SACK, 8),
	CAP_5("§2§lĐá cường hóa V", 5, Material.INK_SACK, 9);
	
	private String name;
	private int grade;
	private Material material;
	private int durability;
	
	private EnhanceGem(String name, int level, Material material, int durability) {
		this.name = name;
		this.grade = level;
		this.material = material;
		this.durability = durability;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getGrade() {
		return this.grade;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public int getDurability() { 
		return this.durability;
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(this.getMaterial());
		item.setDurability((short) this.durability); 
		ItemStackUtils.setDisplayName(item, this.getName());
		ItemStackUtils.addEnchantEffect(item);
		ItemStackUtils.addLoreLine(item, "§7§oĐạo cụ cường hóa");
		item = ItemStackUtils.setTag(item, "sRPG.dacuonghoa", this.name());
		
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "sRPG.dacuonghoa");
	}
	
	public static EnhanceGem fromItem(ItemStack item) {
		if (!isThatItem(item)) return null;
		return EnhanceGem.valueOf(ItemStackUtils.getTag(item, "sRPG.dacuonghoa"));
	}
	
}
