package mk.plugin.skycore.item.gem;

import java.util.List;

import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.tier.Tier;
import mk.plugin.skycore.stat.Stat;

public class Gem {
	
	private String name;
	private Stat stat;
	private int value;
	private Tier tier;
	
	public Gem(String name, Stat stat, int value, Tier tier) {
		this.stat = stat;
		this.value = value;
		this.tier = tier;
		this.name = name;
	}
 	
	public Stat getStat() {
		return this.stat;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public Tier getRate() {
		return this.tier;
	}
	
	public Tier getTier() {
		return this.tier;
	}
	
	public String getName() {
		return this.name;
	}
	
	@Override
	public String toString() {
		return this.name + ";" + this.stat.name() + ";" + this.value + ";" + this.tier.name();
	}
	
	public static Gem parse(String s) {
		List<String> l = Utils.from(s, ";");
		return new Gem(l.get(0), Stat.valueOf(l.get(1)), Integer.valueOf(l.get(2)), Tier.valueOf(l.get(3)));
	}
	
}
