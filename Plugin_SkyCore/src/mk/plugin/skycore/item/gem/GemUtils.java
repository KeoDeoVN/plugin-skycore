package mk.plugin.skycore.item.gem;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public class GemUtils {

	public static ItemStack setGem(ItemStack itemStack, Gem gem) {
		itemStack = updateGem(itemStack, gem);
		itemStack = ItemStackUtils.setTag(itemStack, "sRPGGem", gem.toString());
		return itemStack;
	}
	
	public static Gem fromItemStack(ItemStack itemStack) {
		if (!ItemStackUtils.hasTag(itemStack, "sRPGGem")) return null;
		return Gem.parse(ItemStackUtils.getTag(itemStack, "sRPGGem"));
	}
	
	public static ItemStack updateGem(ItemStack itemStack, Gem gem) {
		List<String> lore = new ArrayList<String> ();
		ItemStackUtils.setDisplayName(itemStack, gem.getName());
		lore.add("§aLoại: §fĐá quý");
		lore.add("§aĐánh giá: §f" + gem.getRate().getName());
		lore.add("§aChỉ số: §f" + gem.getStat().getName());
		lore.add("§aGiá trị: §f" + gem.getValue() + "%");
		ItemStackUtils.setLore(itemStack, lore);
		ItemStackUtils.addFlag(itemStack, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.addFlag(itemStack, ItemFlag.HIDE_ENCHANTS);
		ItemStackUtils.addEnchantEffect(itemStack);
		
		return itemStack;
	}
	
	
}
