package mk.plugin.skycore.item;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.grade.Grade;
import mk.plugin.skycore.stat.Stat;

public class ItemAPI {
	
	private static final String ITEMDATA_TAG = "skycore.item.itemdata";
	private static final String ITEMTYPE_TAG = "skycore.item.itemtype";
	
	public static boolean check(ItemStack is) {
		if (is == null) return false;
		return ItemStackUtils.hasTag(is, ITEMDATA_TAG);
	}
	
	public static ItemStack set(ItemStack is, Item i) {
		Map<String, String> tags = Maps.newHashMap();
		
		tags.put(ITEMDATA_TAG, i.getData().toString());
		tags.put(ITEMTYPE_TAG, i.getType().name());
		
		return ItemStackUtils.setTag(is, tags);
	}
	
	public static ItemStack set(Item i, ItemStack is) {
		return set(is, i);
	}
	
	public static ItemStack set(Player player, ItemStack is, Item i) {
		update(is, player, i);
		return set(i, is);
	}
	
	public static Item get(ItemStack is) {
		if (!check(is)) return null;
		Map<String, String> tags = ItemStackUtils.getTags(is);
		
		ItemData data = ItemData.parse(tags.get(ITEMDATA_TAG));
		ItemType type = ItemType.valueOf(tags.get(ITEMTYPE_TAG));
		
		return new Item(type, data);
	}
	
	public static void update(ItemStack is, Player player) {
		Item item = get(is);
		update(is, player, item);
	}
	
	public static void update(ItemStack is, Player player, Item item) {
		// Get
		ItemData data = item.getData();
		
		// Display name
		String displayName = data.getGrade().getColor() + "§l" + data.getName().replace("_", " ");
		Grade grade = data.getGrade();
		String sl = data.getLevel() <= 0 ? "" : " §f§l[§c§l+" + data.getLevel() + "§f§l]";
		String tiers = "§r§f§l<" + grade.getColor() + "§l" + grade.name() + "§f§l>";
		displayName = tiers + " " + displayName + sl;
		
		// Lore
		List<String> lore = new ArrayList<String> ();
		
		// Rank
		lore.add("§7§oPhẩm chất " + item.getType().getRank().name() + "");
		
		// Desc
		String desc = data.getDesc();
		List<String> descs = Utils.toList(desc, 17, data.getTier().getColor() + "");
		lore.add("");
		lore.addAll(descs);
		lore.add("");
		
		// Durability
		lore.add("§6Độ bền: §7" + data.getDurability() + "/" + data.getGrade().getDurability());
		
		// Stats
		for (Entry<Stat, Integer> e : data.getStats().entrySet()) {
			Stat stat = e.getKey();
			int value = e.getValue();
			lore.add(stat.getColor() + stat.getName() + ": §7+" + value + " §7(" + data.getGrade().getColor() + "+" + data.getBonusStat(player, stat) + "§7)");
		}
		lore.add("");
		
		// Vallina enchants
		boolean hasEnchant = false;
		ItemMeta meta = is.getItemMeta();
		if (meta.getEnchants().size() != 0) {
			hasEnchant = true;
			meta.getEnchants().keySet().forEach(e -> {
				lore.add("§2Phép: §d" + Utils.getEnchantFormat(e, meta.getEnchantLevel(e)));
			});
		}
		if (hasEnchant) lore.add("");
		
		// Gems
		boolean hasGem = data.getGems().size() != 0;
		boolean hasSlot = data.getBlankGemHole() > 0;
		if (hasGem || hasSlot) {
			data.getGems().forEach(gem -> {
				lore.add("§b■ " + "+" + gem.getValue() + "% " + gem.getStat().getName());
			});
			for (int i = 0 ; i < data.getBlankGemHole() ; i++) {
				lore.add("§b□ <Lỗ ngọc trống>");
			}
		}
		
		// Set
		meta.setDisplayName(displayName);
		meta.setLore(lore);
		
		// Save itemstack
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		meta.setUnbreakable(true);
		is.setItemMeta(meta);
		
		// Last modify
		item.getType().getCategory().lastModify(player, is, data);
	}
	
}
