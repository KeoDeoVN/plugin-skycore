package mk.plugin.skycore.itemcategory;

import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import mk.plugin.skycore.grade.Grade;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemCategory;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.jewelry.JewelryCategory;
import mk.plugin.skycore.passive.Passive;
import mk.plugin.skycore.stat.Stat;

public class Jewelry extends ItemCategory {

	private Material material;
	private Map<Grade, Integer> durabilities;
	private Passive passive;
	private JewelryCategory category;
	
	public Jewelry(Material material, List<Integer> durabilities, Passive passive, JewelryCategory category) {
		this.material = material;
		this.passive = passive;
		this.category = category;
		this.durabilities = Maps.newHashMap();
		for (Grade g : Grade.values()) {
			this.durabilities.put(g, durabilities.get(g.getNumber() - 1));
		}
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public Map<Grade, Integer> getDurabilities() {
		return this.durabilities;
	}
	
	public Passive getPassive() {
		return this.passive;
	}
	
	public JewelryCategory getCategory() {
		return this.category;
	}
	
	@Override
	public void lastModify(Player player, ItemStack item, ItemData data) {
		// Type
		item.setType(this.material);
		item.setDurability(this.durabilities.get(data.getGrade()).shortValue());
		
		// Lore
		ItemStackUtils.addLoreLine(item, "");
		ItemStackUtils.addLoreLine(item, "§cNội tại: §f" + this.passive.getName());
	}
	
	
	public static boolean isJewelry(ItemStack is) {
		if (!ItemAPI.check(is)) return false;
		Item item = ItemAPI.get(is);
		return (item.getType().getCategory() instanceof Jewelry);
	}
	
	public static List<Stat> getRingStats() {
		List<Stat> list = Lists.newArrayList();
		list.add(Stat.SAT_THUONG);
		list.add(Stat.HOI_PHUC);
		
		return list;
	}

}
