package mk.plugin.skycore.itemcategory;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemCategory;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.itemtexture.ItemTexture;
import mk.plugin.skycore.stat.Stat;

public class Armor extends ItemCategory {

	private ItemTexture texture;
	private String setID;
	
	public Armor(ItemTexture texture, String setID) {
		this.texture = texture;
		this.setID = setID;
	}
	
	public ItemTexture getTexture() {
		return this.texture;
	}
	
	public String getSetID() {
		return this.setID;
	}
	
	@Override
	public void lastModify(Player player, ItemStack item, ItemData data) {
		// Check if set is trigger
		boolean trigger = hasAll(player, this);
		String line = trigger ? "§a§oBộ được kích hoạt" : "§c§oThiếu giáp, chỉ số vô dụng";
		ItemStackUtils.addLoreLine(item, "");
		ItemStackUtils.addLoreLine(item, line);
		this.texture.set(item);
	}
	
	public static boolean hasAll(Player player, Armor armor) {
		ItemStack[] armors = player.getInventory().getArmorContents();
		for (ItemStack is : armors) {
			if (!ItemAPI.check(is)) return false;
			Item item = ItemAPI.get(is);
			if (!(item.getType().getCategory() instanceof Armor)) return false;
			Armor ca = (Armor) item.getType().getCategory();
			if (!ca.getSetID().equals(armor.getSetID())) return false;
		}
		return true;
	}
	
	public static boolean hasRightArmors(Player player) { 
		ItemStack[] armors = player.getInventory().getArmorContents();
		String id = null;
		for (ItemStack is : armors) {
			if (!ItemAPI.check(is)) return false;
			Item item = ItemAPI.get(is);
			if (!(item.getType().getCategory() instanceof Armor)) return false;
			Armor ca = (Armor) item.getType().getCategory();
			if (id == null) id = ca.getSetID();
			if (!ca.getSetID().equals(id)) return false;
		}
		return true;
	}
	
	public static List<Stat> getStats() {
		List<Stat> list = Lists.newArrayList();
		list.add(Stat.MAU);
		list.add(Stat.SUC_THU);
		list.add(Stat.NE);
		
		return list;
	}
	
}

