package mk.plugin.skycore.itemcategory;

import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import mk.plugin.skycore.grade.Grade;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemCategory;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.skill.Skill;
import mk.plugin.skycore.stat.Stat;
import mk.plugin.skycore.weapon.Shooter;

public class Weapon extends ItemCategory {

	private Material material;
	private Map<Grade, Integer> durabilities;
	private Skill skill;
	private boolean isShooter;
	private double range;
	private Shooter shooter;
	
	public Weapon(Material material, List<Integer> durabilities, Skill skill, boolean isShooter, double range) {
		this.material = material;
		this.skill = skill;
		this.isShooter = isShooter;
		this.range = range;
		this.durabilities = Maps.newHashMap();
		for (Grade g : Grade.values()) {
			this.durabilities.put(g, durabilities.get(g.getNumber() - 1));
		}
	}
	
	
	public Weapon(Material material, List<Integer> durabilities, Skill skill, boolean isShooter, double range, Shooter shooter) {
		this.material = material;
		this.skill = skill;
		this.isShooter = isShooter;
		this.range = range;
		this.shooter = shooter;
		this.durabilities = Maps.newHashMap();
		for (Grade g : Grade.values()) {
			this.durabilities.put(g, durabilities.get(g.getNumber() - 1));
		}
	}
	
	public boolean isShooter() {
		return this.isShooter;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public Map<Grade, Integer> getDurabilities() {
		return this.durabilities;
	}
	
	public Skill getSkill() {
		return this.skill;
	}
	
	public double getRange() {
		return this.range;
	}
	
	public Shooter getShooter() {
		return this.shooter;
	}
	
	@Override
	public void lastModify(Player player, ItemStack item, ItemData data) {
		// Type
		item.setType(this.material);
		item.setDurability(this.durabilities.get(data.getGrade()).shortValue());
		
		// Lore
		ItemStackUtils.addLoreLine(item, "");
		ItemStackUtils.addLoreLine(item, "§cS.Trái: §f" + this.skill.getName());
	}
	
	public static boolean isWeapon(ItemStack is) {
		if (!ItemAPI.check(is)) return false;
		Item item = ItemAPI.get(is);
		return (item.getType().getCategory() instanceof Weapon);
	}
	
	
	public static List<Stat> getStats() {
		List<Stat> list = Lists.newArrayList();
		list.add(Stat.SAT_THUONG);
		list.add(Stat.TOC_DANH);
		list.add(Stat.CHI_MANG);
		list.add(Stat.XUYEN_GIAP);
		
		return list;
	}
	
	
}
