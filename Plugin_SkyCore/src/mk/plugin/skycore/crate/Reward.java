package mk.plugin.skycore.crate;

import mk.plugin.skycore.item.ItemRank;

public class Reward {
	
	private RewardType rt;
	private ItemRank ir;
	private double chance;
	
	public Reward(RewardType rt, ItemRank ir, double chance) {
		this.rt = rt;
		this.ir = ir;
		this.chance = chance;
	}
	
	public RewardType getType() {
		return this.rt;
	}
	
	public ItemRank getItemRank() {
		return this.ir;
	}
	
	public double getChance() {
		return this.chance;
	}
	
}
