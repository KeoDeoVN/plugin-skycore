package mk.plugin.skycore.crate;

import java.util.List;

import kdvn.sky2.rpg.core.utils.LocationData;
import mk.plugin.skycore.itemtexture.ItemTexture;

public class Crate {
	
	private String name;
	private ItemTexture texture;
	private LocationData loc;
	private List<Reward> rewards;
	
	public Crate(String name, ItemTexture texture, LocationData loc, List<Reward> rewards) {
		this.name = name;
		this.texture = texture;
		this.loc = loc;
		this.rewards = rewards;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ItemTexture getTexture() {
		return this.texture;
	}
	
	public LocationData getLocation() {
		return this.loc;
	}

	public List<Reward> getRewards() {
		return this.rewards;
	}
	
	public boolean isSimilar(Crate crate) {
		return this.getName().equals(crate.getName());
	}
	
}
