package mk.plugin.skycore.crate;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.LocationData;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.event.CrateRewardEvent;
import mk.plugin.skycore.item.ItemRank;
import mk.plugin.skycore.itemgenerate.ItemGenerates;
import mk.plugin.skycore.itemtexture.ItemTexture;

public class Crates {

	private static final int ITEM_SLOT = 13;
	private static final int INV_SIZE = 27;

	private static Map<Player, Inventory> invRolling = Maps.newHashMap();

	public static List<Crate> crates = Lists.newArrayList();

	private static Map<Player, Long> cooldownClick = Maps.newHashMap();

	public static void reload(FileConfiguration config) {
		crates.clear();
		config.getConfigurationSection("crate").getKeys(false).forEach(id -> {
			String path = "crate." + id;
			String name = config.getString(path + ".name");
			String ts = config.getString(path + ".texture");
			ItemTexture texture = new ItemTexture(Material.valueOf(ts.split(":")[0]), Integer.valueOf(ts.split(":")[1]));
			double x = config.getDouble(path + ".location.x");
			double y = config.getDouble(path + ".location.y");
			double z = config.getDouble(path + ".location.z");
			String world = config.getString(path + ".location.world");
			List<Reward> rewards = config.getStringList(path + ".rewards").stream()
					.map(s -> new Reward(RewardType.valueOf(s.split(":")[0]), ItemRank.valueOf(s.split(":")[1]), Double.valueOf(s.split(":")[2])))
					.collect(Collectors.toList());
			crates.add(new Crate(name, texture, new LocationData(x, y, z, world), rewards));
		});
	}

	public static boolean isRolling(Player player) {
		return invRolling.containsKey(player);
	}

	public static void setRolling(Player player, Inventory inv) {
		invRolling.put(player, inv);
	}

	public static void removeRolling(Player player) {
		invRolling.remove(player);
	}

	public static void openRolling(Player player) {
		player.openInventory(invRolling.get(player));
	}

	public static void roll(Crate crate, Player player) {
		List<Reward> rewards = crate.getRewards();
		List<ItemStack> ir = rate(player, rewards);

		Inventory inv = Bukkit.createInventory(new RollHolder(), INV_SIZE, "§0§lSORASKY | QUAY...");
		player.openInventory(inv);
		setRolling(player, inv);

		long current = System.currentTimeMillis();
		long mili = 5000;

		// Roll
		new BukkitRunnable() {

			long buff = 20;
			long interval = 10;
			long lastCheck = current;

			@Override
			public void run() {
				// Check roll
				if (System.currentTimeMillis() - lastCheck >= interval) {
					// Set result
					if (System.currentTimeMillis() - current >= mili) {
						this.cancel();
						return;
					}

					// Set
					setRandomColor(inv, INV_SIZE, ITEM_SLOT);
					inv.setItem(ITEM_SLOT, getRandomResult(player, rewards));
					interval += buff;
					lastCheck = System.currentTimeMillis();
					player.playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 1, 1);
				}
			}
		}.runTaskTimerAsynchronously(MainSkyCore.get(), 0, 1);

		// Check result
		new BukkitRunnable() {
			@Override
			public void run() {
				// Check roll
				if (System.currentTimeMillis() - current >= mili) {
					inv.setItem(ITEM_SLOT, ir.get(0));
					player.sendMessage("§aChúc mừng bạn nhận được phần quà");
					ir.forEach(is -> {
						player.getInventory().addItem(is);
					});
					removeRolling(player);
					player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
					
					// Event
					Bukkit.getPluginManager().callEvent(new CrateRewardEvent(player));
					this.cancel();
					return;
				}
			}
		}.runTaskTimer(MainSkyCore.get(), 0, 1);
	}

	public static List<ItemStack> rate(Player player, List<Reward> rewards) {
		double s = 0;
		List<Double> check = Lists.newArrayList();
		for (Reward r : rewards) {
			s += r.getChance();
			check.add(s);
		}

		double random = Utils.random(1, s);

		
		ItemRank ir = null;
		for (int i = 0; i < check.size(); i++) {
			if (check.get(i) >= random) {
				ir = rewards.get(i).getItemRank();
				break;
			}
				
				
		}
		ir = ir == null ? rewards.get(rewards.size() - 1).getItemRank() : ir;
		
		boolean isWeapon = rewards.get(0).getType() == RewardType.WEAPON;
		
		return isWeapon ? Lists.newArrayList(ItemGenerates.generateWeapon(player, ir)) : ItemGenerates.generateArmors(player, ir);
	}

	public static void setRandomColor(Inventory inv, int size, int except) {
		for (int i = 0; i < size; i++) {
			int slot = i;
			if (slot == except)
				continue;
			inv.setItem(slot, getRandomColor());
		}
	}

	public static ItemStack getRandomResult(Player player, List<Reward> rewards) {
		List<Reward> clone = Lists.newArrayList(rewards);
		rewards.forEach(r -> {
			if (r.getChance() <= 0) clone.remove(r);
		});
		
		boolean isWeapon = rewards.get(0).getType() == RewardType.WEAPON;
		ItemRank ir = clone.get(Utils.randomInt(0, clone.size() - 1)).getItemRank();
		
		return isWeapon ? ItemGenerates.generateWeapon(player, ir) : ItemGenerates.generateArmors(player, ir).get(0);
	}

	private static ItemStack getRandomColor() {
		int d = Utils.randomInt(0, 15);
		ItemStack i = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) d);
		ItemMeta meta = i.getItemMeta();
		meta.setDisplayName("§r");
		i.setItemMeta(meta);
		return i;
	}

	public static List<Crate> asCrate(Block block) {
		List<Crate> list = Lists.newArrayList();
		if (block == null || block.getType() == Material.AIR)
			return list;
		for (Crate c : crates) {
			Location l = c.getLocation().toLocation();
			if (l.getBlockX() == block.getX() && l.getBlockY() == block.getY() && l.getBlockZ() == block.getZ())
				list.add(c);
		}
		return list;
	}
	
	public static Crate asOneCrate(Block block) {
		if (block == null || block.getType() == Material.AIR)
			return null;
		for (Crate c : crates) {
			Location l = c.getLocation().toLocation();
			if (l.getBlockX() == block.getX() && l.getBlockY() == block.getY() && l.getBlockZ() == block.getZ())
				return c;
		}
		return null;
	}

	public static ItemStack getKey(Crate crate) {
		ItemStack is = new ItemStack(crate.getTexture().getMaterial(), 1, (short) crate.getTexture().getDurability());

		ItemStackUtils.setDisplayName(is, "§6§lChìa §e§l" + crate.getName());
		ItemStackUtils.addLoreLine(is, "§7§oDùng mở rương trang bị");

		return ItemStackUtils.setTag(is, "skycore.crate", crate.getName());
	}

	public static Crate fromItemStack(ItemStack is) {
		if (!ItemStackUtils.hasTag(is, "skycore.crate"))
			return null;
		for (Crate c : crates) {
			if (c.getName().equals(ItemStackUtils.getTag(is, "skycore.crate"))) {
				return c;
			}
		}
		return null;
	}

	public static void eventClick(InventoryClickEvent e) {
		if (e.getInventory().getHolder() instanceof RollHolder)
			e.setCancelled(true);
	}

	public static void eventInteract(PlayerInteractEvent e) {
		// Check crate
		Player player = e.getPlayer();
		Block block = e.getClickedBlock();
		List<Crate> l = Crates.asCrate(block);
		if (l.size() == 0) return;
		
		e.setCancelled(true);

		// Check delay
		if (cooldownClick.containsKey(player)) {
			if (cooldownClick.get(player) > System.currentTimeMillis())
				return;
		}
		cooldownClick.put(player, System.currentTimeMillis() + 1000);

		// Do
		if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
			player.sendMessage("§cClick chuột phải để mở");
		} else if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {

			// Check rolling
			if (Crates.isRolling(player)) {
				Crates.openRolling(player);
				return;
			}
			
			ItemStack key = player.getInventory().getItemInMainHand();
			Crate crate = fromItemStack(key);	
			if (key == null || key.getType() == Material.AIR || crate == null ) return;
			for (Crate c : l) {
				
				// Check and take key
				if (!crate.isSimilar(c)) continue;

				// Remove key
				key.setAmount(key.getAmount() - 1);
				player.updateInventory();

				// Roll
				Crates.roll(crate, player);
				break;
			}
		}
	}

}

class RollHolder implements InventoryHolder {

	@Override
	public Inventory getInventory() {
		// TODO Auto-generated method stub
		return null;
	}

}
