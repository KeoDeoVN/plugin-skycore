package mk.plugin.skycore.skill.skills;

import java.util.Map;

import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;

import mk.plugin.skycore.skill.SkillExecutor;

public class SkillPotion implements SkillExecutor {

	@Override
	public void start(Map<String, Object> components) {
		LivingEntity target = (LivingEntity) components.get("target");
		PotionEffect pe = (PotionEffect) components.get("potion");
		target.addPotionEffect(pe);
	}

}
