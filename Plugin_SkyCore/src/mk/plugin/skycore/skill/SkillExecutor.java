package mk.plugin.skycore.skill;

import java.util.Map;

public interface SkillExecutor {
	
	public void start(Map<String, Object> components);
	
}
