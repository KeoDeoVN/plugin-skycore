package mk.plugin.skycore.skill;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mk.plugin.sky2rpgcore.weapon.skill.WSBaoKiem;
import mk.plugin.sky2rpgcore.weapon.skill.WSBongDem;
import mk.plugin.sky2rpgcore.weapon.skill.WSChiaCat;
import mk.plugin.sky2rpgcore.weapon.skill.WSDapRiu;
import mk.plugin.sky2rpgcore.weapon.skill.WSDienTinh;
import mk.plugin.sky2rpgcore.weapon.skill.WSKhieuKhich;
import mk.plugin.sky2rpgcore.weapon.skill.WSQuetKiem;
import mk.plugin.sky2rpgcore.weapon.skill.WSTanXaTien;
import mk.plugin.sky2rpgcore.weapon.skill.WSThauXuong;
import mk.plugin.sky2rpgcore.weapon.skill.WSThienPhat;
import mk.plugin.sky2rpgcore.weapon.skill.WSXoayNuoc;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.itemcategory.Weapon;

public enum Skill {
	
	KHIEU_KHICH("Khiêu khích", new WSKhieuKhich(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	QUET_KIEM("Quét kiếm", new WSQuetKiem(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	TAN_XA_TIEN("Tán xạ tiễn", new WSTanXaTien(), new int[] {0}, 4) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	DAP_VANG("Đập văng", new WSDapRiu(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	THAU_XUONG("Thấu xương", new WSThauXuong(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	THIEN_PHAT("Thiên phạt", new WSThienPhat(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	CHIA_CAT("Chia cắt", new WSChiaCat(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	BAO_KIEM("Bão kiếm", new WSBaoKiem(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	LUOI_TINH("Lưới tình", new WSDienTinh(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	XOAY_NUOC("Xoáy nước", new WSXoayNuoc(), new int[] {0}, 5) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	},
	
	MAN_DEM("Màn đêm", new WSBongDem(), new int[] {0}, 6) {
		@Override
		public boolean testRequirement(Player player) {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!Weapon.isWeapon(is)) return false;
			return ((Weapon) ItemAPI.get(is).getType().getCategory()).getSkill() == this;
		}
	}
	
	;
	
	private String name;
	private SkillExecutor executor;
	private int[] combo;
	private int cooldown;
	
	public abstract boolean testRequirement(Player player);
	
	private Skill(String name, SkillExecutor executor, int[] combo, int cooldown) {
		this.name = name;
		this.executor = executor;
		this.combo = combo;
		this.cooldown = cooldown;
	}
	
	public SkillExecutor getExecutor() {
		return this.executor;
	}
	
	public int getCooldown() {
		return this.cooldown;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int[] getCombo() {
		return this.combo;
	}
	
}
