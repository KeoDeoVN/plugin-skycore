package mk.plugin.skycore.loa;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.messaging.PluginMessageRecipient;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public class SpeakerItem {
	
	private static final String NAME = "§6§lLoa thế giới";
	
	public static ItemStack getItem() {
		ItemStack item = new ItemStack(Material.CARPET, 1, (short) 15);
		ItemStackUtils.setDisplayName(item, NAME);
		ItemStackUtils.addLoreLine(item, "§7§oCó tác dụng trò chuyện");
		ItemStackUtils.addLoreLine(item, "§7§oliên server - mọi người chơi ở");
		ItemStackUtils.addLoreLine(item, "§7§oserver khác đều có thể thấy");
		ItemStackUtils.addEnchantEffect(item);
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.getName(item).contains(NAME);
	}
	
	public static void send(Player p, String mess) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(stream);
		String prefix = "§f ;§6§l❖ §a§l[SoraSky] §f§l"+ p.getName() + " §7§l>> §e" ;
		String suffix = ";§f ";
		String action = "fsbc";
		String data2 = "";
		try {
			out.writeUTF(action);
			out.writeUTF(prefix + mess + suffix);
			out.writeUTF(data2);
			((PluginMessageRecipient) p).sendPluginMessage(MainSkyCore.get(), "fs:minestrike", stream.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
