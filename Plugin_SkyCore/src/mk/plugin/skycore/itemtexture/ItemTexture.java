package mk.plugin.skycore.itemtexture;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import kdvn.sky2.rpg.core.utils.Utils;

public class ItemTexture {
	
	private Material material;
	private int durability;
	private boolean isHead;
	private String headTexture;
	private Color color;
	
	public ItemTexture(String headTexture) {
		this.isHead = true;
		this.headTexture = headTexture;
	}
	
	public ItemTexture(Material material) {
		this.material = material;
		this.durability = 0;
		this.isHead = false;
	}
	
	public ItemTexture(Material material, int durability) {
		this.material = material;
		this.durability = durability;
		this.isHead = false;
	}
	
	public ItemTexture(Material material, Color color) {
		this.material = material;
		this.color = color;
		this.isHead = false;
	}
	
	public ItemTexture(Material material, int durability, boolean isHead, String headTexture) {
		this.material = material;
		this.durability = durability;
		this.isHead = isHead;
		this.headTexture = headTexture;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public int getDurability() {
		return this.durability;
	}
	
	public boolean isHead() {
		return this.isHead;
	}
	
	public String getHeadTexture() {
		return this.headTexture;
	}
	
	
	public boolean equals(ItemTexture itemTexture) {
		return this.material == itemTexture.material && this.durability == itemTexture.getDurability();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof ItemTexture) return equals((ItemTexture) o);
		return false;
	}
	
	public void set(ItemStack is) {
		if (this.isHead) {
			is.setType(Material.SKULL_ITEM);
			is.setDurability((short) 3);
			is.setItemMeta(Utils.buildSkull((SkullMeta) is.getItemMeta(), this.headTexture));
		}
		else {
			is.setType(material);
			is.setDurability(new Integer(this.durability).shortValue());
			if (this.color != null) {
				LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
				meta.setColor(this.color);
				is.setItemMeta(meta);
			}
		}
	}
	
}
