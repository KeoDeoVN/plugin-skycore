package mk.plugin.skycore.gui.enhance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.event.ItemUpgradeEvent;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.item.enhancegem.EnhanceGem;
import mk.plugin.skycore.item.luckyamulet.LuckyAmulet;

public class GUIEnhance {
	
	
	public static final int ITEM_SLOT = 11;
	public static final int DCH_SLOT = 13;
	public static final int AMULET_SLOT = 15;
	public static final int RESULT_SLOT = 23;
	public static final int BUTTON_SLOT = 21;
	public static final int INFO_SLOT = 35;
	
	public static String TITLE = "§0§lSORASKY | CƯỜNG HÓA";
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			Utils.sendSound(player, Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(DyeColor.LIME, "§aÔ chứa §oVật phẩm"));
			inv.setItem(DCH_SLOT, GUIUtils.getItemSlot(DyeColor.BLUE, "§aÔ chứa §oĐá cường hóa"));
			inv.setItem(AMULET_SLOT, GUIUtils.getItemSlot(DyeColor.WHITE, "§aÔ chứa §oBùa may mắn §r§7(Không bắt buộc)"));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(DyeColor.PINK, "§aÔ chứa §oSản phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b0%", "§6Click để thực hiện"})));
			inv.setItem(INFO_SLOT, getInfo());
			if (item != null) {
				inv.setItem(ITEM_SLOT, item);
				// Update result
				Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
					ItemStack clone = item.clone();
					ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
					Item rpgI = ItemAPI.get(clone);
					rpgI.getData().setLevel(rpgI.getData().getLevel() + 1);
					ItemAPI.update(clone, player, rpgI);
					inv.setItem(RESULT_SLOT, clone);
				});
			}
		});
		
		player.openInventory(inv);
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				// Get Item
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item == null) {
					player.sendMessage("§cThiếu Vật phẩm!");
					return;
				}
				// Get DCH
				ItemStack dchI = GUIUtils.getItem(inv, DCH_SLOT);
				if (dchI == null) {
					player.sendMessage("§cThiếu Đá cường hóa!");
					return;
				}
				// Get Amulet
				ItemStack amuletI = GUIUtils.getItem(inv, AMULET_SLOT);
				// Object
				Item si = ItemAPI.get(item);
				ItemData rpgI = si.getData();
				EnhanceGem dch = EnhanceGem.fromItem(dchI);
				LuckyAmulet amulet = LuckyAmulet.fromItem(amuletI);
				double chance = getChance(dch, amulet, rpgI.getLevel() + 1);
				// Do
				if (Utils.rate(chance)) {
					player.sendTitle("§a§lTHÀNH CÔNG ^_^", "§6§l+" + rpgI.getLevel() + " >> +" + (rpgI.getLevel() + 1), 0, 15, 0);
					Utils.sendSound(player, Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
					rpgI.setLevel(rpgI.getLevel() + 1);
					if (rpgI.getLevel() >= 5) {
						Utils.broadcast("§f[§6Thông báo§f] §aPlayer §f" + player.getName() + " §ađã ép thành công vật phẩm lên +" + rpgI.getLevel() + ", lực chiến lên một tầm cao mới");
					}
					// Call event
					Bukkit.getPluginManager().callEvent(new ItemUpgradeEvent(player, item, rpgI.getLevel() - 1, rpgI.getLevel()));
				} else {
					String s = "§c§l+" + rpgI.getLevel() + " >> +";
					int down = 0;
					if (amulet == null) {
						down = 1;
						s +=  "" + (rpgI.getLevel() - 1);
					}
					else s += "" + rpgI.getLevel();
					rpgI.setLevel(rpgI.getLevel() - down);
					player.sendTitle("§7§lTHẤT BẠI T_T", s, 0, 15, 0);
					Utils.sendSound(player, Sound.ENTITY_GHAST_SCREAM, 1, 1);
					
					// Call event
					Bukkit.getPluginManager().callEvent(new ItemUpgradeEvent(player, item, rpgI.getLevel() + down, rpgI.getLevel()));
				}
				ItemStack i = ItemAPI.set(player, item, si);
				Utils.giveItem(player, i);
				// Close and reopen
				inv.setItem(DCH_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				inv.setItem(AMULET_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
					openGUI(player);
				}, 10);
			}
		} 
		// Action on bottom inv
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				Utils.sendSound(player, Sound.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Check item
				if (ItemAPI.check(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
					// Update result
					Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
						ItemStack clone = clickedItem.clone();
						ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
						Item si = ItemAPI.get(clone);
						ItemData rpgI = si.getData();
						rpgI.setLevel(rpgI.getLevel() + 1);
						ItemAPI.update(clone, player, si);
						inv.setItem(RESULT_SLOT, clone);
					});
					return;
				}
				
				// Check gem
				else if (EnhanceGem.fromItem(clickedItem) != null) {
					// Check item
					ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
					if (item == null) {
						player.sendMessage("§cĐặt vật phẩm trước!");
						return;
					}
					
					// Check grade of stone
					EnhanceGem eg = EnhanceGem.fromItem(clickedItem);
					int gemGrade = eg.getGrade();
					int itemGrade = ItemAPI.get(item).getData().getGrade().getNumber();
					if (gemGrade != itemGrade) {
						player.sendMessage("§cĐá cường hóa và trang bị phải cùng bậc!");
						return;
					}
							
					// Place
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, DCH_SLOT)) {
						player.sendMessage("§cĐã để Đá cường hoá rồi");
						return;
					}
				}
				
				// Check amulet
				else if (LuckyAmulet.fromItem(clickedItem) != null) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, AMULET_SLOT)) {
						player.sendMessage("§cĐã để Bùa may mắn rồi");
						return;
					}
				}
				
				// Update chance
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				ItemStack dchI = GUIUtils.getItem(inv, DCH_SLOT);
				ItemStack amuletI = GUIUtils.getItem(inv, AMULET_SLOT);
				if (item != null && dchI != null) {
					Item si = ItemAPI.get(item);
					ItemData rpgI = si.getData();
					EnhanceGem dch = EnhanceGem.fromItem(dchI);
					LuckyAmulet amulet = LuckyAmulet.fromItem(amuletI);
					double chance = Utils.round(getChance(dch, amulet, rpgI.getLevel() + 1));
					inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b" + chance + "%", "§6Click để thực hiện"})));
				}
				
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(AMULET_SLOT));
		items.add(inv.getItem(DCH_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. Cường hóa thất bại bị giảm cấp");
		ItemStackUtils.addLoreLine(item, "§f2. §oSách an toàn §r§fvà §oBùa may mắn §r§fgiúp không bị giảm");
		ItemStackUtils.addLoreLine(item, "§f3. §oBùa may mắn §r§fcòn giúp tăng tỉ lệ thành công");
		
		return item;
	}
	
	private static double getChance(EnhanceGem dch, LuckyAmulet sach, int nextLv) {
		if (nextLv >= 13) return 0;
		double chance = 100 - (nextLv - 1) * 8.5;
		int bonus = 0;
		if (sach != null) bonus = sach.getBonus(); 
		return Utils.round(Math.min(chance * ((double) (100 + bonus) / 100), 100));
	}
	
}
