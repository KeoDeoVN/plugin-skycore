package mk.plugin.skycore.gui.jewelry;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.itemcategory.Jewelry;
import mk.plugin.skycore.jewelry.JewelryCategory;
import mk.plugin.skycore.player.PlayerJewelries;
import net.md_5.bungee.api.ChatColor;

public class GUIJewelry {
	
	public static final int SIZE = 18;
	
	public static final String TITLE = "§0§lSORASKY | TRANG SỨC";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, SIZE, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			for (int i = 0 ; i < SIZE ; i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			getSlots().forEach((i, c) -> {
				inv.setItem(i, getBlankSlot().get(c));
			});
			PlayerJewelries.getData(player).forEach((i, item) -> {
				inv.setItem(i, item);
			});
		});
	}
	
	@SuppressWarnings("deprecation")
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().equals(TITLE)) return;	
//		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		if (e.getClickedInventory() == e.getWhoClicked().getOpenInventory().getTopInventory()) {
			e.setCancelled(true);
			ItemStack current = e.getCurrentItem();
			if (getSlots().keySet().contains(slot) || Jewelry.isJewelry(current)) {
				ItemStack cursor = e.getCursor();
				if (Jewelry.isJewelry(cursor)) {
					Item item = ItemAPI.get(cursor);
					// Check slot
					Jewelry j = (Jewelry) item.getType().getCategory();
					if (getSlots().get(slot) != j.getCategory()) return;
					if (isBlankSlot(current)) {
						e.setCursor(null);
					} else 	e.setCursor(current);
					e.setCurrentItem(cursor);
				}
				else {
					if (Jewelry.isJewelry(current)) {
						Item item = ItemAPI.get(current);
						// Check slot
						Jewelry j = (Jewelry) item.getType().getCategory();
						e.setCursor(current);
						e.setCurrentItem(getBlankSlot().get(j.getCategory()));
					}
				}
			}
		}
		
		
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		if (!e.getView().getTitle().equals(TITLE)) return;	
		Map<Integer, ItemStack> data = Maps.newHashMap();
		Inventory inv = e.getInventory();
		Player player = (Player) e.getPlayer();
		
		if (!GUIUtils.checkCheck(player)) return;
		
		getSlots().forEach((i, c) -> {
			ItemStack item = inv.getItem(i);
			if (isBlankSlot(item)) return;
			if (GUIUtils.isItemSlot(item)) return;
			data.put(i, item);
		});
		
		PlayerJewelries.saveData(player, data);
	}
	
	
	
	public static Map<Integer, JewelryCategory> getSlots() {
		Map<Integer, JewelryCategory> map = Maps.newHashMap();
		map.put(2, JewelryCategory.RING);
		map.put(11, JewelryCategory.RING);
		map.put(4, JewelryCategory.NECKLACE);
		map.put(13, JewelryCategory.NECKLACE);
		map.put(6, JewelryCategory.BRACELET);
		map.put(15, JewelryCategory.BRACELET);
		return map;
	}
	
	public static List<Integer> getSlots(JewelryCategory c) {
		List<Integer> list = Lists.newArrayList();
		getSlots().forEach((i, ctg) -> {
			if (ctg == c) list.add(i);
		});
		return list;
	}
	
	public static Map<JewelryCategory, ItemStack> getBlankSlot() {
		Map<JewelryCategory, ItemStack> m = Maps.newHashMap();
		
		// RING
		ItemStack item = new ItemStack(Material.WOOD_HOE);
		ItemMeta meta = item.getItemMeta();
		item.setDurability((short) 1);
		meta.setUnbreakable(true);
		meta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.ITALIC + ChatColor.BOLD + "Ô để Nhẫn");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		item.setItemMeta(meta);
		m.put(JewelryCategory.RING, ItemStackUtils.setTag(item, "jslot", ""));
		
		// NECKLACE
		item = new ItemStack(Material.WOOD_HOE);
		meta = item.getItemMeta();
		item.setDurability((short) 2);
		meta.setUnbreakable(true);
		meta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.ITALIC + ChatColor.BOLD + "Ô để Vòng cổ");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		item.setItemMeta(meta);
		m.put(JewelryCategory.NECKLACE, ItemStackUtils.setTag(item, "jslot", ""));
		
		// BRACELET
		item = new ItemStack(Material.WOOD_HOE);
		meta = item.getItemMeta();
		item.setDurability((short) 3);
		meta.setUnbreakable(true);
		meta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.ITALIC + ChatColor.BOLD + "Ô để Vòng tay");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		item.setItemMeta(meta);
		m.put(JewelryCategory.BRACELET, ItemStackUtils.setTag(item, "jslot", ""));
		
		return m;
	}
	
	public static boolean isBlankSlot(ItemStack item) {
		if (item == null) return false;
		return ItemStackUtils.hasTag(item, "jslot");
	}
	
}
