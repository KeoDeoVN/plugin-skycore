package mk.plugin.skycore.gui.grade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.event.ItemGradeEvent;
import mk.plugin.skycore.grade.Grade;
import mk.plugin.skycore.grade.GradeStone;
import mk.plugin.skycore.grade.GradeUtils;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;

public class GUIGrade {
	
	public static final int ITEM_SLOT = 11;
	public static final int GS_SLOT = 13;
	public static final int RESULT_SLOT = 15;
	public static final int BUTTON_SLOT = 23;
	
	public static String TITLE = "§0§lSORASKY | NÂNG BẬC";
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			Utils.sendSound(player, Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(DyeColor.LIME, "§aÔ chứa §oVật phẩm"));
			inv.setItem(GS_SLOT, GUIUtils.getItemSlot(DyeColor.BLUE, "§aÔ chứa §oĐá tăng bậc"));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(DyeColor.PINK, "§aÔ chứa §oSản phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aĐiểm nhận được: §f0", "§6Click để thực hiện"})));
			if (item != null) {
				inv.setItem(ITEM_SLOT, item);
				Item si = ItemAPI.get(item);
				ItemData rpgI = si.getData();
				ItemStack clone = item.clone();
				ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
				rpgI.setGrade(GradeUtils.getNextGrade(rpgI.getGrade()));;
				ItemAPI.update(clone, player, si);
				inv.setItem(RESULT_SLOT, clone);
			}
		});
		
		player.openInventory(inv);
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				// Get Item
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item == null) {
					player.sendMessage("§cThiếu Vật phẩm!");
					return;
				}
				// Get DCH
				ItemStack dchI = GUIUtils.getItem(inv, GS_SLOT);
				if (dchI == null) {
					player.sendMessage("§cThiếu Đá tăng bậc!");
					return;
				}
				
				// Object
				Item si = ItemAPI.get(item);
				ItemData rpgI = si.getData();
				int exp = dchI.getAmount() * 100;
				
				int remainAmount = 0;
				
				int totalExp = rpgI.getGradeExp() + exp;
				int expTo = GradeUtils.getExpTo(GradeUtils.getNextGrade(rpgI.getGrade()));
				
				Grade before = rpgI.getGrade();
				
				// Set to nextgrade
				if (totalExp > expTo) {
					rpgI.setGradeExp(expTo);
					remainAmount = (totalExp - expTo) / 100;
				} 
				// Add all (can't be up to next grade)
				else {
					rpgI.setGradeExp(totalExp);
					remainAmount = 0;
				}
				
				// Reset level
				if (rpgI.getGrade().getNumber() > before.getNumber()) {
					player.sendMessage("§aNâng bậc thành công cho trang bị, reset cấp độ về 0");
					rpgI.setLevel(0);
				}
				
				player.sendTitle("§a§lTHÀNH CÔNG ^_^", "§6§l+" + exp + " exp cho trang bị", 0, 15, 0);
				Utils.sendSound(player, Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
				
				// Give
				ItemStack i = ItemAPI.set(player, item, si);
				Utils.giveItem(player, i);
				if (remainAmount > 0) {
					dchI.setAmount(remainAmount);
					Utils.giveItem(player, dchI);
				}

				// Call event
				Bukkit.getPluginManager().callEvent(new ItemGradeEvent(player, item, rpgI.getGrade()));
				
				// Close and reopen
				inv.setItem(GS_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
					openGUI(player);
				}, 10);
				
			}
		} 
		// Action on bottom inv
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				Utils.sendSound(player, Sound.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Item
				if (ItemAPI.check(clickedItem)) {
					Item si = ItemAPI.get(clickedItem);
					ItemData rpgI = si.getData();
					if (rpgI.getGrade() == Grade.V) {
						player.sendMessage("§cBậc tối đa, không thể tăng thêm");
						return;
					}
					if (rpgI.getLevel() < 12) {
						player.sendMessage("§cChỉ có thể nâng bậc khi trang bị +12");
						return;
					}
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
					
					// Update result
					Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
						ItemStack clone = clickedItem.clone();
						ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
						rpgI.setGrade(GradeUtils.getNextGrade(rpgI.getGrade()));;
						clone = ItemAPI.set(player, clone, si);
						inv.setItem(RESULT_SLOT, clone);
					});
				}
				
				else if (GradeStone.isThatItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, clickedItem.getAmount(), slot, inv, bInv, GS_SLOT)) {
						player.sendMessage("§cĐã để Đá tăng bậc rồi");
						return;
					}
				}
				
				// Update button
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				ItemStack dchI = GUIUtils.getItem(inv, GS_SLOT);
				if (item != null) {
					Item si = ItemAPI.get(item);
					int exp = 0;
					if (dchI != null) exp = dchI.getAmount() * 100;
					inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aĐiểm nhận được: §f" + exp + " exp", "§aĐiểm hiện tại: §f" + si.getData().getGradeExp() + "/" + GradeUtils.getExpTo(GradeUtils.getNextGrade(si.getData().getGrade())), "§6Click để thực hiện"})));
				}
				
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(GS_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
}
