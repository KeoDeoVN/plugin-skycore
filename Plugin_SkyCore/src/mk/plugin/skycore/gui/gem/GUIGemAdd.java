package mk.plugin.skycore.gui.gem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.event.ItemGemAddEvent;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.item.gem.Gem;
import mk.plugin.skycore.item.gem.GemUtils;
import mk.plugin.skycore.item.luckyamulet.LuckyAmulet;

public class GUIGemAdd {
	
	public static final int ITEM_SLOT = 10;
	public static final int GEM_SLOT = 11;
	public static final int BUTTON_SLOT = 23;
	public static final int RESULT_SLOT = 16;
	public static final int SACH_SLOT = 12;
	public static final int TUT_SLOT = 35; 
	
	public static final String TITLE = "§0§lSORASKY | KHẢM NGỌC";
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			Utils.sendSound(player, Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(DyeColor.GREEN, "§aÔ chứa §oVật phẩm"));
			inv.setItem(GEM_SLOT, GUIUtils.getItemSlot(DyeColor.BLUE, "§aÔ chứa §oNgọc"));
			inv.setItem(SACH_SLOT, GUIUtils.getItemSlot(DyeColor.WHITE, "§aÔ chứa §oBùa may mắn §r§7(Không bắt buộc)"));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(DyeColor.PINK, "§aÔ chứa §oSản phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b0%", "§6Click để thực hiện"})));
			inv.setItem(TUT_SLOT, getInfo());
			if (item != null) inv.setItem(ITEM_SLOT, item);
			
		});
		player.openInventory(inv);
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				ItemStack gemItem = GUIUtils.getItem(inv, GEM_SLOT);
				ItemStack itemItem = GUIUtils.getItem(inv, ITEM_SLOT);
				ItemStack sachItem = GUIUtils.getItem(inv, SACH_SLOT);
				
				// Check
				if (!ItemAPI.check(itemItem)) {
					player.sendMessage("§cChưa đặt Vật phẩm");
					return;
				}
				if (GemUtils.fromItemStack(gemItem) == null) {
					player.sendMessage("§cChưa đặt Ngọc");
					return;
				}
				
				// Do
				Item si = ItemAPI.get(itemItem);
				ItemData rpgI = si.getData();
				Gem rpgG = GemUtils.fromItemStack(gemItem);
				LuckyAmulet sach = LuckyAmulet.fromItem(sachItem);
				
				// Check slot
				if (rpgI.getBlankGemHole() <= 0) {
					player.sendMessage("§cĐã full slot ngọc!");
					player.sendMessage("§cĐục lỗ để có thể khảm thêm!");
					return;
				}
				
				// Chance
				int chance = 50;
				if (sach != null) chance = chance * (100 + sach.getBonus()) / 100;
				
				// Do
				if (Utils.rate(chance)) {
					rpgI.addGem(rpgG);
					player.sendTitle("§a§lTHÀNH CÔNG ^_^", "Khảm ngọc thành công", 0, 15, 0);
					Utils.sendSound(player, Sound.ENTITY_FIREWORK_LAUNCH, 0.7f, 1f);
				} else {
					player.sendTitle("§7§lTHẤT BẠI T_T", "Chúc bạn may mắn lần sau", 0, 15, 0);
					Utils.sendSound(player, Sound.ENTITY_GHAST_SCREAM, 0.7f, 1f);
				}
				
				// Give
				itemItem = ItemAPI.set(player, itemItem, si);
				Utils.giveItem(player, itemItem);
				
				// Clear
				inv.setItem(GEM_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				inv.setItem(SACH_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
					openGUI(player);
				}, 10);
				
				// Call event
				Bukkit.getPluginManager().callEvent(new ItemGemAddEvent(player));
			}
		} else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				Utils.sendSound(player, Sound.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Place 
				if (ItemAPI.check(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
				if (GemUtils.fromItemStack(clickedItem) != null) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, GEM_SLOT)) {
						player.sendMessage("§cĐã để Ngọc rồi");
						return;
					}
				}
				if (LuckyAmulet.isThatItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, SACH_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
				
				// Chance
				ItemStack sachItem = GUIUtils.getItem(inv, SACH_SLOT);
				LuckyAmulet sach = LuckyAmulet.fromItem(sachItem);
				int chance = 50;
				if (sach != null) chance = Math.min(chance * (100 + sach.getBonus()) / 100, 100);
				
				// Update button
				Gem gem = GemUtils.fromItemStack(GUIUtils.getItem(inv, GEM_SLOT));
				if (gem != null) {
					inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b" + chance + "%", "§6Click để thực hiện"})));
				}

				// Update result
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item != null && gem != null) {
					ItemStack clone = item.clone();
					ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
					Item si = ItemAPI.get(clone);
					ItemData rpgI = si.getData();
					if (rpgI.getBlankGemHole() > 0) {
						rpgI.addGem(gem);
					}
					inv.setItem(RESULT_SLOT, ItemAPI.set(player, clone, si));
				}
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(GEM_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		items.add(inv.getItem(SACH_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. §oSách an toàn §r§fkhông có tác dụng khi Khảm ngọc");
		ItemStackUtils.addLoreLine(item, "§f2. §oBùa may mắn §r§fgiúp tăng tỉ lệ thành công");
		
		return item;
	}
	
	
}
