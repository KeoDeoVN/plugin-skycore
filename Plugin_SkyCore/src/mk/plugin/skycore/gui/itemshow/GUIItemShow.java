package mk.plugin.skycore.gui.itemshow;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.niceshops.util.ItemStackUtils;
import mk.plugin.skycore.grade.Grade;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.item.ItemType;


public class GUIItemShow {
	
	public static final int NEXT_SLOT = 52;
	public static final int PRE_SLOT = 46;
	public static final int BACK_SLOT = 8;
	
	public static void open(Player player, ItemType it) {
		Inventory inv = Bukkit.createInventory(new GISHolder(-1, null), 9, "§0§lSORASKY | KHO TRANG BỊ");
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSkyCore.get(), () -> {
			for (int i = 0 ; i < 9; i++) inv.setItem(i, Utils.getBlackSlot());
			inv.setItem(BACK_SLOT, getBack());
			List<ItemStack> l = Lists.newArrayList();
			for (Grade g : Grade.values()) {
				ItemStack is = it.createItemStack(player);
				Item item = ItemAPI.get(is);
				ItemData data = item.getData();
				data.setGrade(g);
				l.add(ItemAPI.set(player, is, item));
			}
			int i = -1;
			for (ItemStack is : l)  {
				i++;
				inv.setItem(i, is);
			}
			
		});
	}
	
	public static void open(Player player, int page) {
		int maxpage = ItemType.values().length / 45 + 1;
		page = Math.max(1, Math.min(page, maxpage));
		
		Map<Integer, ItemType> items = Maps.newHashMap();
		int c = -1;
		for (int i = (page - 1) * 45 ; i < Math.min(ItemType.values().length, page * 45) ; i++) {
			c++;
			items.put(c, ItemType.values()[i]);
		}
		
		Inventory inv = Bukkit.createInventory(new GISHolder(page, items), 54, "§0§lSORASKY | KHO TRANG BỊ [" + page + "/" + maxpage + "]");
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSkyCore.get(), () -> {
			for (int i = 45 ; i < 54 ; i++) inv.setItem(i, Utils.getBlackSlot());
			inv.setItem(NEXT_SLOT, getNext());
			inv.setItem(PRE_SLOT, getPrevios());
			
			items.forEach((sl, it) -> {
				ItemStack icon = it.createItemStack(player);
				ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oClick để xem"));
				inv.setItem(sl, icon);
			});
		});
		
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getInventory().getHolder() instanceof GISHolder == false) return;
		e.setCancelled(true);
		if (e.getClickedInventory() != e.getWhoClicked().getOpenInventory().getTopInventory()) return;
		
		int slot = e.getSlot();
		Player player = (Player) e.getWhoClicked();
		GISHolder holder = (GISHolder) e.getInventory().getHolder();	
		if (holder.getPage() == -1) {
			if (slot == BACK_SLOT) open(player, holder.getPage());
			return;
		}
		
		if (slot == NEXT_SLOT) open(player, holder.getPage() + 1);
		else if (slot == PRE_SLOT) open(player, holder.getPage() - 1);
		
		else if (holder.getItems().containsKey(slot)) {
			ItemType it = holder.getItems().get(slot);
			open(player, it);
		}
	}
	
	public static ItemStack getNext() {
		ItemStack is = new ItemStack(Material.ARROW);
		ItemStackUtils.setDisplayName(is, "§6§lTrang sau");
		return is;
	}
	
	public static ItemStack getPrevios() {
		ItemStack is = new ItemStack(Material.ARROW);
		ItemStackUtils.setDisplayName(is, "§6§lTrang trước");
		return is;
	}
	
	public static ItemStack getBack() {
		ItemStack is = new ItemStack(Material.BARRIER);
		ItemStackUtils.setDisplayName(is, "§6§lTrở lại");
		return is;
	}
	
	
	
	
	
	
	
}

class GISHolder implements InventoryHolder {

	private Map<Integer, ItemType> items;
	private int page;
	
	public GISHolder(int page, Map<Integer, ItemType> items) {
		this.page = page;
		this.items = items;
	}
	
	public int getPage() {
		return this.page;
	}
	
	public Map<Integer, ItemType> getItems() {
		return this.items;
	}
	
	@Override
	public Inventory getInventory() {
		return null;
	}
	
}
