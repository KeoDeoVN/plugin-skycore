package mk.plugin.skycore.gui.player;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.permbuff.SPermBuff;
import kdvn.sky2.rpg.core.power.SRPGPowerUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.element.Element;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.player.SkyPlayer;
import mk.plugin.skycore.stat.Stat;

public class GUIPlayerInfo {
	
	public static final String TITLE = "§0§lSORASKY | THÔNG TIN";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		// Load item
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, Utils.getBlackSlot());
			}
			inv.setItem(2, getStatInfo(player));
			inv.setItem(4, getExpInfo(player));
			inv.setItem(6, getBuffInfo(player));
		});
	}
	
	public static ItemStack getStatInfo(Player player) {
		ItemStack item = new ItemStack(Material.PAPER, 1);
		ItemStackUtils.setDisplayName(item, "§6§lCHỈ SỐ");
		SkyPlayer rpgP = PlayerUtils.getData(player);
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		lore.add("§3Điểm hệ: §f" + PlayerUtils.getRemainElementPoint(player));
		lore.add("§3Lực chiến: §f" + SRPGPowerUtils.calculatePower(player));
		lore.add("§7------------------");
		for (int i = 0 ; i < Element.values().length ; i++) {
			Element po = Element.values()[i];
			lore.add("§c" + po.getName() + ": §f" + rpgP.getElement(po));
		}
		lore.add("§7------------------");
		for (Stat stat : Stat.values()) {
			lore.add("§a" + stat.getName() + ": §f" + rpgP.getStat(player, stat) + " §7(" + Utils.round(PlayerUtils.getStatValue(player, stat)) + ")");
		}
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static ItemStack getExpInfo(Player player) {
		ItemStack item = new ItemStack(Material.EXP_BOTTLE, 1);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§lKINH NGHIỆM");
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		lore.add("§aTỉ lệ: §f" + Utils.round(player.getExp() * 100) + "%");
		int maxExp = PlayerUtils.getExpToNextLevel(player.getLevel() + 1);
		lore.add("§aKinh nghiệm: §f" + new Double(maxExp * player.getExp()).intValue() + "/" + maxExp);
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static ItemStack getBuffInfo(Player player) {
		ItemStack item = new ItemStack(Material.DRAGONS_BREATH);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§lBUFF");
		SkyPlayer rpgP = PlayerUtils.getData(player);
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		rpgP.getStatBuffs().forEach(buff -> {
			String s = buff.isPercentBuff() ? "%" : "";
			lore.add("§a+" + buff.getValue() + s + " " + buff.getStat().getName() + " §7(" + buff.getSecondsRemain() + "s)");
		});
		rpgP.getExpBuffs().forEach(buff -> {
			lore.add("§a+" + buff.getValue() + "% Exp" + " §7(" + buff.getSecondsRemain() + "s)");
		});
		
		// Permbuff
		SPermBuff.permBuffs.values().forEach(buff -> {
			if (!player.hasPermission(buff.getPermission())) return;
			buff.getBuffs().forEach((stat, value) -> {
				lore.add("§6+" + value + "% " + stat.getName());
			});
		});;
		
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().contains(TITLE)) return;
		e.setCancelled(true);
	}
	
}
