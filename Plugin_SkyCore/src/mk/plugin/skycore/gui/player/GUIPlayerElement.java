package mk.plugin.skycore.gui.player;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.element.Element;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.player.SkyPlayer;

public class GUIPlayerElement {
	
	public static final String TITLE = "§0§lSORASKY | HỆ NGUYÊN TỐ";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		// Load item
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, Utils.getBlackSlot());
			}
			getSlots().forEach((i, p) -> {
				inv.setItem(i, getItem(p));
			});
			inv.setItem(8, GUIPlayerInfo.getStatInfo(player));
		});
	}
	
	public static Map<Integer, Element> getSlots() {
		Map<Integer, Element> map = new HashMap<Integer, Element> ();
		map.put(2, Element.KIM);
		map.put(3, Element.MOC);
		map.put(4, Element.THUY);
		map.put(5, Element.HOA);
		map.put(6, Element.THO);
		
		return map;
	}
	
	public static Map<Element, Material> getIcons() {
		Map<Element, Material> map = new HashMap<Element, Material> ();
		map.put(Element.KIM, Material.FLINT);
		map.put(Element.MOC, Material.LOG);
		map.put(Element.THUY, Material.GHAST_TEAR);
		map.put(Element.HOA, Material.BLAZE_POWDER);
		map.put(Element.THO, Material.DIRT);
		
		return map;
	}
	
	public static ItemStack getItem(Element p) {
		Material material = getIcons().get(p);
		ItemStack item = new ItemStack(material, 1);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§l+1 " + p.getName().toUpperCase());
		ItemStackUtils.addLoreLine(item, "§7--------------");
		p.getStats().forEach((stat, value) -> {
			ItemStackUtils.addLoreLine(item, "§e" + stat.getName() + " §f+" + value);
		});
		
		return item;
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().contains(TITLE)) return;
		e.setCancelled(true);

		Inventory inv = e.getInventory();
		Player player = (Player) e.getWhoClicked();
		SkyPlayer rpgP = PlayerUtils.getData(player);
		Map<Integer, Element> slots = getSlots();
		int slot = e.getSlot();
		if (slots.containsKey(slot)) {
			int remain = PlayerUtils.getRemainElementPoint(player);
			if (remain <= 0) {
				player.sendMessage("§cKhông còn điểm dư");
				return;
			}
			Element po = slots.get(slot);
			int point = rpgP.getElement(po);
			point++;
			rpgP.setElement(po, point);
			
			// Update stat
			PlayerUtils.updatePlayer(player);
			Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
				inv.setItem(8, GUIPlayerInfo.getStatInfo(player));
			});
		}
	}
	
}
