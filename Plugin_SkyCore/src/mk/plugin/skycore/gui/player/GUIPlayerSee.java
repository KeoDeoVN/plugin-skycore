package mk.plugin.skycore.gui.player;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.Utils;

public class GUIPlayerSee {
	
	public static String TITLE = "§0§lSORASKY | XEM";
	
	public static void openGUI(Player player, Player viewer) {
		Inventory inv = Bukkit.createInventory(null, 54, TITLE + player.getName().toUpperCase());
		viewer.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			PlayerInventory pInv = player.getInventory();
			ItemStack[] storageContents = pInv.getStorageContents();
			ItemStack[] armorContents = pInv.getArmorContents();
			inv.setStorageContents(storageContents);
			inv.setItem(45, pInv.getItemInOffHand());
			for (int i = 0 ; i < armorContents.length ; i++) {
				inv.setItem(49 - i, armorContents[i]);
			}
			for (int i = 36 ; i < 45 ; i++) {
				inv.setItem(i, Utils.getBlackSlot());
			}
			inv.setItem(50, Utils.getBlackSlot());
			inv.setItem(51, GUIPlayerInfo.getStatInfo(player));
			inv.setItem(52, GUIPlayerInfo.getExpInfo(player));
			inv.setItem(53, GUIPlayerInfo.getBuffInfo(player));
			Utils.sendSound(viewer, Sound.BLOCK_CHEST_OPEN, 1, 1);
		});
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getView().getTitle().contains(TITLE)) e.setCancelled(true);
	}
	
	public static void eventDrag(InventoryDragEvent e) {
		if (e.getView().getTitle().contains(TITLE)) e.setCancelled(true);
	}	
	
}
