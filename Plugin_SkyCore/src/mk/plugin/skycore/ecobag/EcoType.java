package mk.plugin.skycore.ecobag;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.price.MoneyAPI;
import kdvn.sky2.rpg.core.price.PointAPI;
import kdvn.sky2.rpg.core.price.TokenAPI;
import mk.plugin.skycore.itemtexture.ItemTexture;

public enum EcoType {
	
	MONEY("Money", "§f", new ItemTexture(Material.CARPET, 8)) {
		@Override
		public void give(Player player, int value) {
			MoneyAPI.giveMoney(player, value);
		}
	},
	TOKEN("Token", "§a", new ItemTexture(Material.CARPET, 9)) {
		@Override
		public void give(Player player, int value) {
			TokenAPI.give(player, value);
		}
	},
	POINT("Point", "§e", null) {
		@Override
		public void give(Player player, int value) {
			PointAPI.givePoint(player, value);
		}
	};
	
	private String name;
	private String color;
	private ItemTexture bagTexture;
	
	public abstract void give(Player player, int value);
	
	private EcoType(String name, String color, ItemTexture bagTexture) {
		this.name = name;
		this.color = color;
		this.bagTexture = bagTexture;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public ItemTexture getBagTexture() {
		return this.bagTexture;
	}
	
}
