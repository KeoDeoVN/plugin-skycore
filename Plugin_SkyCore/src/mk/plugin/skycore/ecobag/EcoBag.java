package mk.plugin.skycore.ecobag;

import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.dungeon3.util.MinMax;
import mk.plugin.skycore.itemtexture.ItemTexture;

public enum EcoBag {
	
	M_I(EcoType.MONEY, new MinMax(2500, 3125)),
	M_II(EcoType.MONEY, new MinMax(5000, 6250)),
	M_III(EcoType.MONEY, new MinMax(10000, 12500)),
	M_IV(EcoType.MONEY, new MinMax(20000, 25000)),
	M_V(EcoType.MONEY, new MinMax(40000, 50000)),
	
	T_I(EcoType.TOKEN, new MinMax(40, 60)),
	T_II(EcoType.TOKEN, new MinMax(60, 100)),
	T_III(EcoType.TOKEN, new MinMax(100, 200)),
	T_IV(EcoType.TOKEN, new MinMax(200, 400)),
	T_V(EcoType.TOKEN, new MinMax(400, 800))

	;
	
	private EcoType type;
	private MinMax value;
	
	private EcoBag(EcoType type, MinMax value) {
		this.type = type;
		this.value = value;
	}
	
	public EcoType getEcoType() {
		return this.type;
	}
	
	public MinMax getMinMaxValue() {
		return this.value; 
	}
	
	public int rateValue() {
		return Utils.randomInt(this.value.getMin(), this.value.getMax());
	}
	
	public ItemStack createItemStack() {
		ItemTexture t = this.type.getBagTexture();
		ItemStack is = new ItemStack(t.getMaterial(), 1, (short) t.getDurability());
		ItemStackUtils.setDisplayName(is, "§6§lTúi " + this.type.getName() + " " + this.name().substring(2, this.name().length()));
		ItemStackUtils.addLoreLine(is, "§7§oChuột phải để dùng");

		return ItemStackUtils.setTag(is, "skycore.ecobag", this.name());
	}
	
	public static EcoBag parse(ItemStack is) {
		if (!ItemStackUtils.hasTag(is, "skycore.ecobag")) return null;
		return EcoBag.valueOf(ItemStackUtils.getTag(is, "skycore.ecobag"));
	}
	
}
