package mk.plugin.skycore.repair;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import mk.plugin.niceshops.util.ItemStackUtils;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.itemtexture.ItemTexture;

public class RepairStone {
	
	private static ItemStack ITEM = null;
	
	public static ItemStack getItem() {
		if (ITEM == null) {
			ItemStack is = new ItemStack(Material.FLINT, 1);
			ItemStackUtils.setDisplayName(is, "§6§lĐá sửa chữa");
			ItemStackUtils.addLoreLine(is, "");
			ItemStackUtils.addLoreLine(is, "§eTác dụng: §fTăng độ bền cho trang bị và");
			ItemStackUtils.addLoreLine(is, "§fsửa công cụ khi bị hỏng");
			ItemStackUtils.addLoreLine(is, "");
			ItemStackUtils.addLoreLine(is, "§eCách dùng: §fClick vào Đá sửa chữa sau đó");
			ItemStackUtils.addLoreLine(is, "§fclick đè vào vật phẩm muốn sửa");	
			ItemStackUtils.addEnchantEffect(is);
			ITEM = is;
		}
		
		return ITEM.clone();
	}
	
	public static boolean isItem(ItemStack i) {
		if (ITEM == null) {
			ItemStack is = new ItemStack(Material.FLINT, 1);
			ItemStackUtils.setDisplayName(is, "§6§lĐá sửa chữa");
			ItemStackUtils.addLoreLine(is, "");
			ItemStackUtils.addLoreLine(is, "§eTác dụng: §fTăng độ bền cho trang bị và");
			ItemStackUtils.addLoreLine(is, "§fsửa công cụ khi bị hỏng");
			ItemStackUtils.addLoreLine(is, "");
			ItemStackUtils.addLoreLine(is, "§eCách dùng: §fClick vào Đá sửa chữa sau đó");
			ItemStackUtils.addLoreLine(is, "§fclick đè vào vật phẩm muốn sửa");
			ItemStackUtils.addEnchantEffect(is);
			ITEM = is;
		}
		return i != null && i.isSimilar(ITEM);
	}
	
	public static void onClick(InventoryClickEvent e) {
		ItemStack cursor = e.getCursor();
		ItemStack current = e.getCurrentItem();
		Player player = (Player) e.getWhoClicked();
		
		if (!isItem(cursor)) return;
		if (current == null || current.getType() == Material.AIR) return;
		
		
		e.setCancelled(true);
		
		// Check if Sora Item
		boolean fixed = false;
		if (ItemAPI.check(current)) {
			Item item = ItemAPI.get(current);
			ItemData data = item.getData();
			data.setDurability(data.getGrade().getDurability());
			e.setCurrentItem(ItemAPI.set(player, current, item));
			fixed = true;
		}
		else {
			if (!MainSkyCore.CUSTOM_TEXTURES.contains(new ItemTexture(current.getType(), current.getDurability()))) {
				List<String> start = Lists.newArrayList("WOOD", "STONE", "IRON", "GOLD", "DIAMOND");
				for (String s : start) {
					if (current.getType().name().startsWith(s) && current.getDurability() > 0) {
						current.setDurability((short) 0);
						fixed = true;
						break;
					}
				}
			}
		}
		
		// Subtract
		if (!fixed) {
			player.sendMessage("§cKhông có gì để sửa chữa cả");
			e.setCancelled(false);
			return;
		}
		else {
			cursor.setAmount(cursor.getAmount() - 1);
			player.sendMessage("§aSửa chữa thành công");
			player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1, 1);
			player.updateInventory();
		}
	}
	
	
	
}
