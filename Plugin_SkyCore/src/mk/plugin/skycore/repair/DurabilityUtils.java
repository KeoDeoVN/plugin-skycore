package mk.plugin.skycore.repair;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mk.plugin.skycore.customcraft.CraftRecipes;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemData;

public class DurabilityUtils {
	
	public static boolean subtract(Item item, int amount) {
		ItemData data = item.getData();
		if (data.getDurability() <= 0) return false;
		data.setDurability(Math.max(data.getDurability() - amount, 0));
		return true;
	}
	
	public static boolean takeRepairStone(Player p, int amount) {
		ItemStack rs = RepairStone.getItem();
		return CraftRecipes.take(p, rs, amount);
	}
	
}
