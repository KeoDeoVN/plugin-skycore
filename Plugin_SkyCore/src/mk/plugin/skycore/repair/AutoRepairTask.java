package mk.plugin.skycore.repair;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;

public class AutoRepairTask extends BukkitRunnable {
	
	public static final String PERMISSION = "skycore.autorepair";

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(p -> {
			if (!p.hasPermission(PERMISSION)) return;
			int count = 0;
			
			// Armor
			ItemStack[] armors =  p.getInventory().getArmorContents();
			for (int i = 0 ; i < armors.length ; i++) {
				ItemStack is = armors[i];
				if (is != null && ItemAPI.check(is)) {
					Item item = ItemAPI.get(is);
					ItemData data = item.getData();
					if (data.getDurability() == 0 && DurabilityUtils.takeRepairStone(p, 1)) {
						data.setDurability(data.getGrade().getDurability());
						armors[i] = ItemAPI.set(p, is, item);
						count++;
					}
				}
			}
			p.getInventory().setArmorContents(armors);
			
			// Weapon
			ItemStack is = p.getInventory().getItemInMainHand();
			if (is != null && ItemAPI.check(is)) {
				Item item = ItemAPI.get(is);
				ItemData data = item.getData();
				if (data.getDurability() == 0 && DurabilityUtils.takeRepairStone(p, 1)) {
					data.setDurability(data.getGrade().getDurability());
					p.getInventory().setItemInMainHand(ItemAPI.set(p, is, item));
					count++;
				}
			}
			
			if (count > 0) {
				p.updateInventory();
				p.sendMessage("§aĐã sửa độ bền cho " + count + " trang bị");
			}
		});
	}
	
	
	
}
