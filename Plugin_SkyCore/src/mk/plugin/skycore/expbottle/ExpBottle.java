package mk.plugin.skycore.expbottle;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.dungeon3.util.MinMax;

public enum ExpBottle {

	I(new MinMax(500, 1000)),
	II(new MinMax(1000, 1500)),
	III(new MinMax(2000, 3000)),
	IV(new MinMax(4000, 6000)),
	V(new MinMax(8000, 12000));
		
	private MinMax value;
	
	private ExpBottle(MinMax value) {
		this.value = value;
	}
	
	public MinMax getValue() {
		return this.value;
	}
	
	public int rateValue() {
		return Utils.randomInt(this.value.getMin(), this.value.getMax());
	}
	
	public ItemStack createItemStack() {
		ItemStack is = new ItemStack(Material.EXP_BOTTLE, 1);
		ItemStackUtils.setDisplayName(is, "§6§lBình kinh nghiệm " + this.name());

		return ItemStackUtils.setTag(is, "skycore.expbottle", this.name());
	}
	
	public static ExpBottle parse(ItemStack is) {
		if (!ItemStackUtils.hasTag(is, "skycore.expbottle")) return null;
		return ExpBottle.valueOf(ItemStackUtils.getTag(is, "skycore.expbottle"));
	}
	
}
