package mk.plugin.skycore.task;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import kdvn.sky2.rpg.core.main.MainSkyCore;

public class OreTask extends BukkitRunnable {

	public final static String KEY = "ore-Key";
	
	private static Map<Block, Long> map1 = Maps.newHashMap();
	private static Map<Block, Material> map2 = Maps.newHashMap();
	
	@Override
	public void run() {
		Sets.newHashSet(map1.keySet()).forEach(block -> {
			long l = map1.get(block);
			if (l <= System.currentTimeMillis()) {
				block.removeMetadata(KEY, MainSkyCore.get());
				block.setType(map2.get(block));
				map2.remove(block);
				map1.remove(block);
			}
		});
	}
	
	public static void resetAll() {
		Sets.newHashSet(map1.keySet()).forEach(block -> {
			block.removeMetadata(KEY, MainSkyCore.get());
			block.setType(map2.get(block));
			map2.remove(block);
			map1.remove(block);
		});
	}
	
	public static void set(Block block, Long milis, Material m) {
		map1.put(block, System.currentTimeMillis() + milis);
		map2.put(block, m);
		block.setMetadata(KEY, new FixedMetadataValue(MainSkyCore.get(), ""));
	}
	
	public static boolean isBeingCooldown(Block block) {
		return block.hasMetadata(KEY);
	}

}
