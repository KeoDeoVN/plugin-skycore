package mk.plugin.skycore.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.itemcategory.Jewelry;

public class JewelryTask extends BukkitRunnable {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			Utils.getItemsInPlayer(player).forEach(is -> {
				if (ItemAPI.check(is)) {
					Item item = ItemAPI.get(is);
					if (item.getType().getCategory() instanceof Jewelry) {
						Jewelry j = (Jewelry) item.getType().getCategory();
						j.getPassive().getExecutor().doTask(player);
					}
				}
			});
		});
	}

}
