package mk.plugin.skycore.task;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.inventivetalent.glow.GlowAPI;
import org.inventivetalent.glow.GlowAPI.Color;

import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.itemcategory.Weapon;

public class TargetTask extends BukkitRunnable {

	private Map<LivingEntity, Long> map = Maps.newConcurrentMap();
	
	@Override
	public void run() {
		if (!Bukkit.getPluginManager().isPluginEnabled("GlowAPI")) {
			this.cancel();
			return;
		}
		map.forEach((le, l) -> {
			if (l < System.currentTimeMillis()) {
				GlowAPI.setGlowing(le, false, Bukkit.getOnlinePlayers());
				map.remove(le);
			}
		});
		Bukkit.getOnlinePlayers().forEach(player -> {
			ItemStack is = player.getInventory().getItemInMainHand();
			if (!ItemAPI.check(is)) return;
			Item item = ItemAPI.get(is);
			if (item.getType().getCategory() instanceof Weapon) {
				Weapon weapon = (Weapon) item.getType().getCategory();
				double range = weapon.isShooter() ? 20 : weapon.getRange();
				LivingEntity target = Utils.getTarget(player, range);
				if (target == null) return;
				map.put(target, System.currentTimeMillis() + 300);
				GlowAPI.setGlowing(target, Color.RED, player);
			}
		});
	}
	
}
