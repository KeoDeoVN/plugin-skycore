package mk.plugin.skycore.stat;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.utils.Utils;

public enum Stat{
	
	SAT_THUONG("Sát thương", 1, "§c") {
		@Override
		public double pointsToValue(int point) {
			return point * 0.9;
		}

		@Override
		public void set(Player player, int point) {}

		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	MAU("Sinh lực", 10, "§a") {
		@Override
		public double pointsToValue(int point) {
			if (point < getMinValue()) point = getMinValue();
			return point * 2;
		}
		
		// Set Health
		@Override
		public void set(Player player, int point) {
			player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(pointsToValue(point));
			if (player.getHealth() > player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()) {
				player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
			}
		}
		
		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	SUC_THU("Sức thủ", 5, "§f") {
		@Override
		public double pointsToValue(int point) {
			return Utils.round(((double) point / (point + 100))) * 100;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "%";
		}
	},
	
	NE("Né đòn", 5, "§3") {
		@Override
		public double pointsToValue(int point) {
			return Utils.round(((double) point / (point + 300))) * 100;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "%";
		}
	},
	
	HOI_PHUC("Hồi phục", 2, "§2") {
		@Override
		public double pointsToValue(int point) {
			return 1 + point * 0.1;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	HUT_MAU("Hút máu", 5, "§6") {
		@Override
		public double pointsToValue(int point) {
			return 5 + Utils.round(((double) point / (point + 200))) * 20;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	CHI_MANG("Chí mạng", 5, "§e") {
		@Override
		public double pointsToValue(int point) {
			return Utils.round(((double) point / (point + 300))) * 100;
		} 
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "%";
		}
	},
	
	XUYEN_GIAP("Xuyên giáp", 0, "§4") {
		@Override
		public double pointsToValue(int point) {
			return Utils.round(((double) point / (point + 300))) * 100;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "%";
		}
	},
	
	TOC_DANH("Tốc đánh", 0, "§3") {
		@Override
		public double pointsToValue(int point) {
			return 1.5 - ((double) point / (point + 30) * 1.5) * 1;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "%";
		}
	};
	
	public abstract double pointsToValue(int point);
	public abstract void set(Player player, int point);
	public abstract String getSubStat();
	
	private String color;
	private String name;
	private int minValue;
	
	private Stat(String name, int minValue, String color) {
		this.name = name;
		this.minValue = minValue;
		this.color = color;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getMinValue() {
		return this.minValue;
	}
	
	public String getColor() {
		return this.color;
	}
}
