package mk.plugin.skycore.combo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.actionbar.SRPGActionBar;import kdvn.sky2.rpg.core.main.MainSkyCore;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.itemcategory.Weapon;
import mk.plugin.skycore.repair.DurabilityUtils;
import mk.plugin.skycore.skill.Skill;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class ComboUtils {
	
	private static HashMap<Player, List<Integer>> combo = new HashMap<Player, List<Integer>> ();
	private static List<Player> isCombo = new ArrayList<Player> ();
	private static Map<Player, Long> delay = Maps.newHashMap();
	
	@SuppressWarnings("deprecation")
	public static void comboExecute(Player player) {
		for (Skill skill : Skill.values()) {
			// Right combo
			if (compareCombo(player, skill.getCombo())) {
				// Check skills in player
				if (!hasSkill(player, skill)) continue;

				// Check durability
				ItemStack is = player.getInventory().getItemInMainHand();
				if (!ItemAPI.check(is)) continue;
				Item item = ItemAPI.get(is);
				if (!MainSkyCore.WORLD_PVPS.contains(player.getWorld().getName())) {
					if (!DurabilityUtils.subtract(item, 2)) {
						player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder("§cVũ khí có độ bền bằng 0, cần phải sữa chữa").create());
						continue;
					}
				}

				player.getInventory().setItemInMainHand(ItemAPI.set(player, is, item));
				
				// Check delay
				if (delay.containsKey(player)) {
					if (delay.get(player) > System.currentTimeMillis()) {
						player.sendMessage("§cKỹ năng chưa hồi xong");
						player.sendMessage("§cCòn §6" + ((delay.get(player) - System.currentTimeMillis()) / 1000 + 1) + "s"); 
						continue;
					}
				}
				delay.put(player, System.currentTimeMillis() + skill.getCooldown() * 1000);

				// Executor
				player.sendTitle("§c§l[" + skill.getName() + "]", "§7§oThực thi kỹ năng", 0, 20, 0);
				skill.getExecutor().start(getComponents(player));
			}
		}
	}
	
	public static Map<String, Object> getComponents(Player player) {
		Map<String, Object> m = Maps.newHashMap();
		m.put("player", player);
		
		return m;
	}
	
	public static int getTimeOfClick(Player player) {
		if (!combo.containsKey(player)) {
			return 0;
		}
		return combo.get(player).size();
	}

	public static void addOne(Player player, int i) {
		if (combo.containsKey(player)) {
			List<Integer> list = combo.get(player);
			list.add(i);
			combo.put(player, list);
		}
		else {
			List<Integer> list = new ArrayList<Integer> ();
			list.add(i);
			combo.put(player, list);
		}
	}
	
	public static void finishCombo(Player player) {
		combo.remove(player);
		SRPGActionBar.send(player, "");
	}
	
	public static boolean compareCombo(Player player, int[] i) {
		if (!combo.containsKey(player)) {
			return false;
		}
		
		if (combo.get(player).size() != i.length) {
			return false;
		}
		boolean check = true;
		for (int i2 = 0 ; i2 < combo.get(player).size() ; i2 ++) {
			if (i[i2] != combo.get(player).get(i2)) {
				check = false;
				break;
			}
		}
		return check;
	}
	
	public static void sendTitleCombo(Player player) {
		List<String> comboList = new ArrayList<String> ();
		for (int i : combo.get(player)) {
			if (i == 0) {
				comboList.add("§a§lTrái§f");
			}
			else comboList.add("§a§lPhải§f");
		}
		String mess = "§c§lShift + §r";
		for (String s : comboList) {
			mess += " + " + s;
		}
		mess += " + ";
		mess = "§f" + mess;
		SRPGActionBar.send(player, mess);
		
	}
	
	public static boolean isPlayerComboed(Player player) {
		if (isCombo.contains(player)) {
			return true;
		} else return false;
	}
	
	public static void addCombo(Player player) {
		if (!isPlayerComboed(player)) {
			isCombo.add(player);
		}
	}
	
	public static void huyCombo(Player player) {
		if (isPlayerComboed(player)) {
			isCombo.remove(player);
		}
		finishCombo(player);
	}
	
	public static String comboToString(int[] cb) {
		String combo = "";
		for (int i = 0 ; i < cb.length ; i++) {
			String s = cb[i] == 0 ? "T" : "P";
			combo += s;
		}

		String result = combo.replaceAll("T", "Trái ").replaceAll("P", "Phải ");
		if (combo.equalsIgnoreCase("")) return "Không cần";
		return result;
	}
	
	public static boolean hasSkill(Player player, Skill skill) {
		ItemStack is = player.getInventory().getItemInMainHand();
		if (Weapon.isWeapon(is)) {
			Weapon w = (Weapon) ItemAPI.get(is).getType().getCategory();
			return w.getSkill() == skill;
		}
		return false;
	}
	
}
