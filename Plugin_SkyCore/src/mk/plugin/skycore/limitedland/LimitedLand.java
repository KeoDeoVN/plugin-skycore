package mk.plugin.skycore.limitedland;

import java.util.Map;

import org.bukkit.Location;

import com.google.common.collect.Maps;

import io.lumine.utils.config.file.FileConfiguration;

public class LimitedLand {
	
	public static Map<String, LimitedLand> limitedLands = Maps.newHashMap();
	
	public static void reload(FileConfiguration config) {
		limitedLands.clear();
		config.getConfigurationSection("limited-land").getKeys(false).forEach(id -> {
			TempLocation tl = TempLocation.parse(config.getString("limited-land." + id + ".center"));
			double r = config.getDouble("limited-land." + id + ".radius");
			String m = config.getString("limited-land." + id + ".message");
			limitedLands.put(id, new LimitedLand(tl, r, m));
		});
	}
	
	private TempLocation center;
	private double radius;
	private String message;
	
	public LimitedLand(TempLocation center, double radius, String message) {
		this.center = center;
		this.radius = radius;
		this.message = message;
	}
	
	public TempLocation getTLocation() {
		return this.center;
	}
	
	public Location getLocation() {
		return this.center.toLocation();
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public String getMessage() {
		return this.message;
	}
	
}
