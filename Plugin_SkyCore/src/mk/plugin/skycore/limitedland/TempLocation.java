package mk.plugin.skycore.limitedland;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class TempLocation {
	
	private double x;
	private double y;
	private double z;
	private String world;
	
	public TempLocation(double x, double y, double z, String world) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.world = world;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public String getWorld() {
		return this.world;
	}
	
	public Location toLocation() {
		return new Location(Bukkit.getWorld(this.getWorld()), this.x, this.y, this.z);
	}
	
	@Override
	public String toString() {
		return this.x + ";" + this.y + ";" + this.z + ";" + this.world;
	}
	
	public static TempLocation parse(String s) {
		String[] l = s.split(";");
		return new TempLocation(Double.valueOf(l[0]), Double.valueOf(l[1]), Double.valueOf(l[2]), l[3]);
	}
	
}
