package mk.plugin.skycore.customcraft.result;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.item.ItemRank;
import mk.plugin.skycore.itemgenerate.ItemGenerates;

public class ArmorResult extends CraftResult {
	
	private ArmorType type;
	
	public ArmorResult(String type) {
		this.type = ArmorType.valueOf(type.toUpperCase());
	}
	
	@Override
	public ItemStack getIcon() {
		return type.getIcon();
	}

	@Override
	public void giveResult(Player player) {
		ItemRank ir = this.type.rate();
		ItemGenerates.generateArmors(player, ir).forEach(is -> Utils.giveItem(player, is));
	}
	
}


enum ArmorType {
	
	TYPE1(0.1, 1) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 8);
			ItemStackUtils.setDisplayName(icon, "§7§lBộ giáp bậc I");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên giáp VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			ItemStackUtils.addLoreLine(icon, "§7§oNhớ chừa 4 ô trống trong kho");
			return icon;
		}
	},
	
	TYPE2(0.3, 3) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 9);
			ItemStackUtils.setDisplayName(icon, "§9§lBộ giáp bậc II");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên giáp VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			ItemStackUtils.addLoreLine(icon, "§7§oNhớ chừa 4 ô trống trong kho");
			return icon;
		}
	},
	
	TYPE3(0.7, 7) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 10);
			ItemStackUtils.setDisplayName(icon, "§c§lBộ giáp bậc III");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên giáp VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			ItemStackUtils.addLoreLine(icon, "§7§oNhớ chừa 4 ô trống trong kho");
			return icon;
		}
	},
	
	TYPE4(1.5, 15) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 11);
			ItemStackUtils.setDisplayName(icon, "§6§lBộ giáp bậc IV");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên giáp VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			ItemStackUtils.addLoreLine(icon, "§7§oNhớ chừa 4 ô trống trong kho");
			return icon;
		}
	},
	
	TYPE5(3, 25) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 12);
			ItemStackUtils.setDisplayName(icon, "§a§lBộ giáp bậc V");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên giáp VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			ItemStackUtils.addLoreLine(icon, "§7§oNhớ chừa 4 ô trống trong kho");
			return icon;
		}
	};
	
	private double superChance;
	private double wowChance;
	
	public abstract ItemStack getIcon();
	
	private ArmorType(double superChance, double wowChance) {
		this.superChance = superChance;
		this.wowChance = wowChance;
	}
	
	public ItemRank rate() {
		if (Utils.rate(superChance)) return ItemRank.SUPER;
		if (Utils.rate(wowChance)) return ItemRank.WOW;
		return ItemRank.VIP;
	}
	
}

