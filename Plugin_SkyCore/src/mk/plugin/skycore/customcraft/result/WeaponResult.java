package mk.plugin.skycore.customcraft.result;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.item.ItemRank;
import mk.plugin.skycore.itemgenerate.ItemGenerates;

public class WeaponResult extends CraftResult {
	
	private WeaponType type;
	
	public WeaponResult(String type) {
		this.type = WeaponType.valueOf(type.toUpperCase());
	}
	
	@Override
	public ItemStack getIcon() {
		return type.getIcon();
	}

	@Override
	public void giveResult(Player player) {
		ItemRank ir = this.type.rate();
		ItemStack is = ItemGenerates.generateWeapon(player, ir);	
		player.getInventory().addItem(is);
	}
	
}	

enum WeaponType {
	
	TYPE1(0.1, 2) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 3);
			ItemStackUtils.setDisplayName(icon, "§7§lKho vũ khí bậc I");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên vũ khí VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			return icon;
		}
	},
	
	TYPE2(0.5, 7) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 4);
			ItemStackUtils.setDisplayName(icon, "§9§lKho vũ khí bậc II");ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên vũ khí VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			return icon;
		}
	},
	
	TYPE3(1, 10) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 5);
			ItemStackUtils.setDisplayName(icon, "§c§lKho vũ khí bậc III");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên vũ khí VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			return icon;
		}
	},
	
	TYPE4(3, 20) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 6);
			ItemStackUtils.setDisplayName(icon, "§6§lKho vũ khí bậc IV");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên vũ khí VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			return icon;
		}
	},
	
	TYPE5(5, 30) {
		@Override
		public ItemStack getIcon() {
			ItemStack icon = new ItemStack(Material.CARPET, 1, (short) 7);
			ItemStackUtils.setDisplayName(icon, "§a§lKho vũ khí bậc V");
			ItemStackUtils.setLore(icon, Lists.newArrayList("§7§oNhận ngẫu nhiên vũ khí VIP, WOW, SUPER và", "§7§obậc càng cao thì ra WOW, SUPER càng dễ"));
			return icon;
		}
	};
	
	private double superChance;
	private double wowChance;
	
	public abstract ItemStack getIcon();
	
	private WeaponType(double superChance, double wowChance) {
		this.superChance = superChance;
		this.wowChance = wowChance;
	}
	
	public ItemRank rate() {
		if (Utils.rate(superChance)) return ItemRank.SUPER;
		if (Utils.rate(wowChance)) return ItemRank.WOW;
		return ItemRank.VIP;
	}
	
}

