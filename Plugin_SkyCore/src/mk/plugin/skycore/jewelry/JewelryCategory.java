package mk.plugin.skycore.jewelry;

public enum JewelryCategory {

	RING,
	BRACELET,
	NECKLACE;
	
}
