package mk.plugin.skycore.passive.passives;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mk.plugin.marry.util.MarryUtils;
import mk.plugin.skycore.passive.PassiveExecutor;

public class PHonUoc extends PassiveExecutor {

	@Override
	public void onActive(Player player) {
		player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 1));
	}

	@Override
	public void onInactive(Player player) {
		PotionEffect pe = player.getPotionEffect(PotionEffectType.REGENERATION);
		if (pe == null) return;
		if (pe.getDuration() > 10000 * 20) {
			player.removePotionEffect(PotionEffectType.REGENERATION);
		}
	}

	@Override
	public boolean checkActive(Player player) {
		if (Bukkit.getPluginManager().isPluginEnabled("Marry")) {
			if (!MarryUtils.isMarried(player)) return false;
			String p = MarryUtils.getPartner(player);
			Player partner = Bukkit.getPlayer(p);
			if (partner != null) {
				if (partner.getWorld() != player.getWorld()) return false;
				return partner.getLocation().distance(player.getLocation()) < 20;
			}
		}
		
		return false;
	}

}
