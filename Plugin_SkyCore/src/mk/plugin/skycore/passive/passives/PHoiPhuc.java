package mk.plugin.skycore.passive.passives;

import org.bukkit.entity.Player;

import mk.plugin.skycore.passive.PassiveExecutor;

public class PHoiPhuc extends PassiveExecutor {

	@Override
	public void onActive(Player player) {
		player.sendMessage("§aTrang bị trang sức thành công!");
		player.sendMessage("§aBạn nhận được khả năng phục hồi khi bị tấn công");
	}

	@Override
	public void onInactive(Player player) {	
	}

	@Override
	public boolean checkActive(Player player) {
		return true;
	}

}
