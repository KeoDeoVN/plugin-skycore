package mk.plugin.skycore.passive;

import mk.plugin.skycore.passive.passives.PBangGia;
import mk.plugin.skycore.passive.passives.PHoiPhuc;
import mk.plugin.skycore.passive.passives.PHonUoc;
import mk.plugin.skycore.passive.passives.PKhoHeo;
import mk.plugin.skycore.passive.passives.PNhanhNhen;

public enum Passive {
	
	HON_UOC("Hôn ước", new PHonUoc()),
	KHO_HEO("Khô héo", new PKhoHeo()),
	HOI_PHUC("Hồi phục", new PHoiPhuc()),
	NHANH_NHEN("Nhanh nhẹn", new PNhanhNhen()),
	BANG_GIA("Băng giá", new PBangGia());
	
	private String name;
	private PassiveExecutor executor;
	
	private Passive(String name, PassiveExecutor executor) {
		this.name = name;
		this.executor = executor;
	}
	
	public String getName() {
		return this.name;
	}
	
	public PassiveExecutor getExecutor() {
		return this.executor;
	}
	
}
