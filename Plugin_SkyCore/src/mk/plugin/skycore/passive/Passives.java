package mk.plugin.skycore.passive;

import java.util.List;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.itemcategory.Jewelry;

public class Passives {
	
	public static List<Passive> getPassives(Player player) {
		List<Passive> list = Lists.newArrayList();
		Utils.getItemsInPlayer(player).forEach(is -> {
			if (ItemAPI.check(is)) {
				Item item = ItemAPI.get(is);
				if (item.getType().getCategory() instanceof Jewelry) {
					Jewelry j = (Jewelry) item.getType().getCategory();
					list.add(j.getPassive());
				}
			}
		});
		return list;
	}
	
	public static boolean hasPassive(Player player, Passive passive) {
		return getPassives(player).contains(passive);
	}
	
}
