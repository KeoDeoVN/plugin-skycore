package mk.plugin.skycore.passive;

import org.bukkit.entity.Player;

public abstract class PassiveExecutor {
	
	private boolean isActive;
	
	public boolean isActive() {
		return this.isActive;
	}
	
	public void setActive(boolean value) {
		this.isActive = value;
	}
	
	public abstract void onActive(Player player);
	
	public abstract void onInactive(Player player);
	
	public abstract boolean checkActive(Player player);
	
	public void doTask(Player player) {
		if (this.isActive != checkActive(player)) {
			if (this.isActive) {
				onInactive(player);
				this.isActive = false;
			}
			else if (!this.isActive) {
				onActive(player);
				this.isActive = true;
			}
		}
	}
	
	
}
