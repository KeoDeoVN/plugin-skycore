package mk.plugin.skycore.damage;

public class Damage {
	
	private double value;
	private DamageType type;
	
	public Damage(double value, DamageType type) {
		this.value = value;
		this.type = type;
	}
	
	public double getValue() {
		return this.value;
	}
	
	public DamageType getType() {
		return this.type;
	}
	
}
