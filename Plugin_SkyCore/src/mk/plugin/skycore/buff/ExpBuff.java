package mk.plugin.skycore.buff;

public class ExpBuff {
	
	// Percent
	private int value;
	
	private long start;
	private long time;
	private String data;
	
	public ExpBuff(int value, long start, long time, String data) {
		this.value = value;
		this.start = start;
		this.time = time;
		this.data = data;
	}
	
	public String getData() {
		return this.data;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public long getStart() {
		return this.start;
	}
	
	public long getTime() {
		return this.time;
	}
	
	public void addTime(long time) {
		this.time += time;
	}
	
	public boolean isStillEffective() {
		return this.start + this.time > System.currentTimeMillis();
	}
	
	public int getSecondsRemain() {
		return new Double((this.time - (System.currentTimeMillis() - this.start)) / 1000).intValue();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof ExpBuff) {
			ExpBuff eb = (ExpBuff) o;
			return this.data.equals(eb.getData()) && this.start == eb.getStart() && this.time == eb.getTime() && this.value == eb.getValue();
		}
		return false;
	}
	
	@Override
	public String toString() {
		String s = "";
		s += this.getValue() + ":" + this.getStart() + ":" + this.getValue() + ":" + this.getData();
		return s;
	}
	
	public static ExpBuff fromString(String s) {
		String[] list = s.split(":");
		String data = "";
		if (list.length > 3) data = list[3];
		ExpBuff buff = new ExpBuff(Integer.valueOf(list[0]), Long.valueOf(list[1]), Long.valueOf(list[2]), data);
		return buff;
	}
	
}
