package mk.plugin.skycore.buff;

import mk.plugin.skycore.stat.Stat;

public class StatBuff {

	private Stat stat;
	private int value;
	private boolean isPercent;

	private long start;
	private long time;

	public StatBuff(Stat stat, double value, boolean isPercent, long start, long time) {
		this.stat = stat;
		this.value = new Double(value).intValue();
		this.isPercent = isPercent;
		this.start = start;
		this.time = time;
	}

	public Stat getStat() {
		return this.stat;
	}

	public int getValue() {
		return this.value;
	}

	public boolean isPercentBuff() {
		return this.isPercent;
	}

	public long getStart() {
		return this.start;
	}

	public void addTime(long time) {
		this.time += time;
	}

	public long getTime() {
		return this.time;
	}

	public boolean isStillEffective() {
		return this.start + this.time > System.currentTimeMillis();
	}

	public int getSecondsRemain() {
		return new Double((this.time - (System.currentTimeMillis() - this.start)) / 1000).intValue();
	}

	@Override
	public String toString() {
		String s = "";
		s += this.getStat() + ":" + this.getValue() + ":" + this.isPercentBuff() + ":" + this.getStart() + ":" + this.getValue();
		return s;
	}
	
	public static StatBuff fromString(String s) {
		if (s.length() == 0) return null;
		String[] list = s.split(":");
		StatBuff buff = new StatBuff(Stat.valueOf(list[0]), Integer.valueOf(list[1]), Boolean.valueOf(list[2]), Long.valueOf(list[3]), Long.valueOf(list[4]));
		return buff;
	}

}
