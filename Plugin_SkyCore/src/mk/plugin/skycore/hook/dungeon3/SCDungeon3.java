package mk.plugin.skycore.hook.dungeon3;

import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Maps;

public class SCDungeon3 {

	private static Map<String, Double> expRate = Maps.newHashMap();
	
	public static void reload(FileConfiguration config) {
		expRate.clear();
		config.getConfigurationSection("dungeon3.exp-rate").getKeys(false).forEach(id -> {
			expRate.put(id, config.getDouble("dungeon3.exp-rate." + id));
		});
	}
	
	public static double getExpRate(String dID) {
		return expRate.getOrDefault(dID, 0d);
	}
	
}
