package mk.plugin.skycore.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.permbuff.SPermBuff;
import mk.plugin.sky2rpgcore.element.Element;
import mk.plugin.skycore.buff.ExpBuff;
import mk.plugin.skycore.buff.StatBuff;
import mk.plugin.skycore.stat.Stat;

public class SkyPlayer {
	
	private int level = 0;
	private Map<Element, Integer> elements = Maps.newHashMap();
	private List<StatBuff> statBuffs = new ArrayList<StatBuff> ();
	private List<ExpBuff> expBuffs = new ArrayList<ExpBuff> ();
	
	private Map<Stat, Integer> itemStats = new HashMap<Stat, Integer> ();
	
	public SkyPlayer() {
		this.level = 0;
		this.itemStats = Maps.newHashMap();
		this.elements = Maps.newHashMap();
		this.statBuffs = Lists.newArrayList();
		this.expBuffs = Lists.newArrayList();
	}
	
	public SkyPlayer(int level, Map<Stat, Integer> stats, Map<Element, Integer> elements, List<StatBuff> statBuffs, List<ExpBuff> expBuffs) {
		this.level = level;
		this.itemStats = stats == null ? this.itemStats : stats;
		this.elements = elements;
		this.statBuffs = statBuffs == null ? this.statBuffs : statBuffs;
		this.expBuffs = expBuffs == null ? this.expBuffs : expBuffs;
	}
	
	// Getters
	
	public int getLevel() {
		return this.level;
	}
	
	public int getStat(Player player, Stat stat) {
		int value = 0;
		
		// Item
		if (this.itemStats.containsKey(stat)) {
			value += this.itemStats.get(stat);
		}
		
		// element
		for (Element e : this.elements.keySet()) {
			int point = this.getElement(e);
			if (e.getStats().containsKey(stat)) value += e.getStats().get(stat) * point;
		}
		
		// Buff
		for (StatBuff buff : this.statBuffs) {
			if (buff.getStat() == stat) {
				if (buff.isPercentBuff()) {
					value = value * (100 + buff.getValue()) / 100;
				} else value += buff.getValue();
			} 
		}
		
		// Permbuff
		for (SPermBuff buff : SPermBuff.permBuffs.values()) {
			if (player.hasPermission(buff.getPermission()) && buff.getBuffs().containsKey(stat)) {
				value = value * (100 + buff.getBuffs().get(stat)) / 100;
			}
		}
		
		return value;
	}
	
	public Map<Stat, Integer> getItemStats() {
		return this.itemStats;
	}
	
	public List<StatBuff> getStatBuffs() {
		return this.statBuffs;
	}
	
	public List<ExpBuff> getExpBuffs() {
		return this.expBuffs;
	}
	
	public void addExpBuff(ExpBuff eb) {
		this.expBuffs.add(eb);
	}
	
	public int getExpBuff() {
		int i = 0;
		for (ExpBuff buff : this.expBuffs) {
			i += buff.getValue();
		}
		return i;
	}
	
	public int getElement(Element element) {
		if (this.elements.containsKey(element)) return this.elements.get(element);
		return 0;
	}
	
	public Map<Element, Integer> getElements() {
		return this.elements;
	}
	
	// Setters
	
	public void setItemStats(Map<Stat, Integer> stats) {
		this.itemStats = stats;
	}
	
	public void setElements(Map<Element, Integer> elements) {
		this.elements = elements;
	}
	
	public void setElement(Element element, int value) {
		this.elements.put(element, value);
	}
	
	public void setStatBuffs(List<StatBuff> statBuffs) {
		this.statBuffs = statBuffs;
	}
	
	public void setExpBuffs(List<ExpBuff> expBuffs) {
		this.expBuffs = expBuffs;
	}
	
	public void removeStatBuff(StatBuff buff) {
		this.statBuffs.remove(buff);
	}
	
	public void removeExpBuff(ExpBuff buff) {
		this.expBuffs.remove(buff);
	}
	
	// I/O
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		JSONObject jo = new JSONObject();
		
		// Put
		jo.put("level", this.level);
		
		// Map
		JSONObject stats = new JSONObject(this.elements);
		jo.put("elements", stats);
		
		// List
		JSONArray ja = new JSONArray();
		this.statBuffs.forEach(sb -> {
			ja.add(sb.toString());
		});
		jo.put("statBuffs", ja);
		JSONArray ja2 = new JSONArray();
		this.expBuffs.forEach(eb -> {
			ja2.add(eb.toString());
		});
		jo.put("expBuffs", ja2);
		
		return jo.toJSONString();
		
	}
	
	@SuppressWarnings("unchecked")
	public static SkyPlayer fromString(String s) {
		JSONObject jo = (JSONObject) JSONValue.parse(s);
		
		// 
		int level = new Long((long) jo.get("level")).intValue();
		
		// 
		Map<String, Long> tElements = (JSONObject) jo.get("elements");
		Map<Element, Integer> elements = Maps.newHashMap();
		tElements.forEach((k, v) -> {
			elements.put(Element.valueOf(k), new Long(v).intValue());
		});
		
		//
		List<ExpBuff> expBuffs = Lists.newArrayList();
		((JSONArray) jo.get("expBuffs")).forEach(teb -> expBuffs.add(ExpBuff.fromString((String) teb)));
		List<StatBuff> statBuffs = Lists.newArrayList();
		((JSONArray) jo.get("statBuffs")).forEach(tsb -> statBuffs.add(StatBuff.fromString((String) tsb)));
		
		return new SkyPlayer(level, null, elements, statBuffs, expBuffs);
	}
	
	
}
