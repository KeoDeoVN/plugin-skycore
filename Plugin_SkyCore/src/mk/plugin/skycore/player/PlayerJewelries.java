package mk.plugin.skycore.player;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;
import mk.plugin.skycore.gui.jewelry.GUIJewelry;

public class PlayerJewelries {
	
	public static Map<Integer, ItemStack> getData(Player player) {
		Map<Integer, ItemStack> m = Maps.newHashMap();
		PlayerData pd = PlayerDataAPI.getPlayerData(player);
		for (int i = 0 ; i < GUIJewelry.SIZE ; i++) {
			if (pd.hasData("skycore-jewelry-" + i)) {
				ItemStack is = ItemStackUtils.toItemStack(pd.getValue("skycore-jewelry-" + i));
				if (is == null || is.getType() == Material.AIR) continue;
				m.put(i, is);
			}
		}
		return m;
	}
	
	public static void saveData(Player player, Map<Integer, ItemStack> m) {
		PlayerData pd = PlayerDataAPI.getPlayerData(player);
		for (int i = 0 ; i < GUIJewelry.SIZE ; i++) {
			if (pd.hasData("skycore-jewelry-" + i)) {
				pd.remove("skycore-jewelry-" + i);
			}
		}
		m.forEach((i, item) -> {
			pd.set("skycore-jewelry-" + i, ItemStackUtils.toBase64ItemStack(item));
		});
		PlayerDataAPI.saveData(player);
	}
}
