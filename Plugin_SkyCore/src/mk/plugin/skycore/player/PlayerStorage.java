package mk.plugin.skycore.player;

import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;

public class PlayerStorage {
	
	private static final String KEY = "skyPlayer";
	
	public static SkyPlayer get(String name) {
		PlayerData pb = PlayerDataAPI.getPlayerData(name);
		if (!pb.hasData(KEY)) return new SkyPlayer();
		SkyPlayer sp = SkyPlayer.fromString(pb.getValue(KEY));
		return sp;
	}
	
	public static void save(String name, SkyPlayer sp) {
		PlayerData pb = PlayerDataAPI.getPlayerData(name);
		pb.set("skyPlayer", sp.toString());
		PlayerDataAPI.saveData(name);
	}
	
}
