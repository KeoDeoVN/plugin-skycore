package mk.plugin.skycore.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.actionbar.SRPGActionBar;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.mob.LeveledMob;
import kdvn.sky2.rpg.core.party.PartyManager;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.dungeon3.dungeon.util.DPlayerUtils;
import mk.plugin.skycore.buff.ExpBuff;
import mk.plugin.skycore.buff.StatBuff;
import mk.plugin.skycore.hook.dungeon3.SCDungeon3;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.stat.Stat;

public class PlayerUtils {
	
	public static final int MAX_LEVEL = 100;
	
	private static Set<String> biPhanDon = new HashSet<String> ();
	
	private static Map<String, SkyPlayer> data = new HashMap<String, SkyPlayer> ();
	
	public static SkyPlayer getData(Player player) {
		return getData(player.getName());
	}
	
	public static SkyPlayer getData(String name) {
		if (data.containsKey(name)) {
			return data.get(name);
		}
		return null;
	}
	
	public static double getStatValue(Player player, Stat stat) {
		SkyPlayer rpgP = getData(player);
		if (rpgP == null) return 0;
		return stat.pointsToValue(rpgP.getStat(player, stat));
	}
	
	public static void buffStat(Player player, StatBuff buff) {
		SkyPlayer rpgP = getData(player);
		if (rpgP == null) return;
		List<StatBuff> buffs = rpgP.getStatBuffs();
		
		// Trùng stat thì tăng thời gian
		boolean has = false;
		for (StatBuff b : buffs) {
			if (b.getStat() == buff.getStat()) {
				b.addTime(buff.getTime());
				has = true;
				break;
			}
		}
		if (!has) buffs.add(buff);
		
		rpgP.setStatBuffs(buffs);
	}
	
	public static void buffExp(Player player, ExpBuff buff) {
		SkyPlayer rpgP = getData(player);
		if (rpgP == null) return;
		List<ExpBuff> buffs = rpgP.getExpBuffs();
		
		// Trùng % thì tăng thời gian
		boolean has = false;
		for (ExpBuff b : buffs) {
			if (b.getValue() == buff.getValue()) {
				b.addTime(buff.getTime());
				has = true;
				break;
			}
		}
		if (!has) buffs.add(buff);
		
		rpgP.setExpBuffs(buffs);
	}
	
	public static void updatePlayer(Player player) {
		SkyPlayer rpgP = getData(player);
		if (rpgP == null) return;
		
		// Item
		Map<Stat, Integer> itemStats = new LinkedHashMap<Stat, Integer> ();
		List<ItemStack> items = Utils.getItemsInPlayer(player);
		
		for (Stat stat : Stat.values()) {
			int itemStat = stat.getMinValue();
			for (ItemStack is : items) {
				if (ItemAPI.check(is)) {
					Item item = ItemAPI.get(is);
					itemStat += item.getData().getCalculatedStat(player, stat);
				}
			}
			itemStats.put(stat, itemStat);
		}
		rpgP.setItemStats(itemStats);
		
		// Check stat buff
		List<StatBuff> statBuffs = new ArrayList<StatBuff> (rpgP.getStatBuffs());
		for (StatBuff buff : rpgP.getStatBuffs()) {
			if (!buff.isStillEffective()) statBuffs.remove(buff);
		}
		rpgP.setStatBuffs(statBuffs);
		
		// Check exp buff
		List<ExpBuff> expBuffs = new ArrayList<ExpBuff> (rpgP.getExpBuffs());
		for (ExpBuff buff : rpgP.getExpBuffs()) {
			if (!buff.isStillEffective()) expBuffs.remove(buff);
		}
		rpgP.setExpBuffs(expBuffs);
		
		for (Stat stat : Stat.values()) {
			SkyPlayer sp = PlayerUtils.getData(player);
			stat.set(player, sp.getStat(player, stat));
		}
	}
	
//	public static void updatePlayer(Player player) {
//		// Sync update
//		Object mo = null;
//		if (player.hasMetadata("updatePlayer_mo")) {
//			mo = player.getMetadata("updatePlayer_mo").get(0).value();
//		} else {
//			mo = new Object();
//			player.setMetadata("updatePlayer_mo", new FixedMetadataValue(MainSky2RPGCore.getMain(), mo));
//		}
//		
//		synchronized (mo) {
//			SkyPlayer rpgP = getData(player);
//			if (rpgP == null) return;
//			
//			// Item
//			Map<Stat, Integer> itemStats = new LinkedHashMap<Stat, Integer> ();
//			List<ItemStack> items = Utils.getItemsInPlayer(player);
//			
//			for (Stat stat : Stat.values()) {
//				int itemStat = stat.getMinValue();
//				for (ItemStack is : items) {
//					if (ItemAPI.check(is)) {
//						Item item = ItemAPI.get(is);
//						itemStat += item.getData().getCalculatedStat(player, stat);
//					}
//				}
//				itemStats.put(stat, itemStat);
//				stat.set(player, itemStat);
//			}
//			rpgP.setItemStats(itemStats);
//			
//			// Check stat buff
//			List<StatBuff> statBuffs = new ArrayList<StatBuff> (rpgP.getStatBuffs());
//			for (StatBuff buff : rpgP.getStatBuffs()) {
//				if (!buff.isStillEffective()) statBuffs.remove(buff);
//			}
//			rpgP.setStatBuffs(statBuffs);
//			
//			// Check exp buff
//			List<ExpBuff> expBuffs = new ArrayList<ExpBuff> (rpgP.getExpBuffs());
//			for (ExpBuff buff : rpgP.getExpBuffs()) {
//				if (!buff.isStillEffective()) expBuffs.remove(buff);
//			}
//			rpgP.setExpBuffs(expBuffs);
//		}
//		
//	}
	
	public static void playerQuit(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			PlayerStorage.save(player.getName(), getData(player));
			data.remove(player.getName());
			Utils.sendToConsole("§6[Sky2RPGCore] Saved data of " + player.getName());
		}); 
	}
	
	public static void playerJoin(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SkyPlayer rpgP = PlayerStorage.get(player.getName());
			Lists.newArrayList(rpgP.getExpBuffs()).forEach(eb -> {
				if (eb.getData().equalsIgnoreCase("skycore-xacminh")) rpgP.removeExpBuff(eb);
			});
			data.put(player.getName(), rpgP);
			updatePlayer(player);
			Utils.sendToConsole("§a[Sky2RPGCore] Loaded data of " + player.getName());
			setTabPrefix(player);
		}); 
	}
	
	public static int getRemainElementPoint(Player player) {
		// Get sum
		int lv = player.getLevel();
		int sum = 0;
		for (int i = 1 ; i < lv ; i++) {
			sum += i / 10 + 1;
		}
		
		// Get used points
		SkyPlayer rpgP = getData(player.getName());
		int has = 0;
		for (int i : rpgP.getElements().values()) has += i;
		
		// Calculate
		return sum - has; 
	}
	
	public static void addExp(Player player, int exp) {
		int level = player.getLevel() + 1;
		// Set exp bar
		while (exp > 0) {
			if (level >= MAX_LEVEL) return;
			int toExp = getExpToNextLevel(level);
			float more = new Double((double) exp / toExp).floatValue();
			float afterExpRate = more + player.getExp();
			
			if (afterExpRate > 1) {
				exp = exp - new Double((toExp * (1 - player.getExp()))).intValue();
				player.setLevel(level);
				player.setExp(0);
				level++;
			} else {
				exp = 0;
				player.setExp(afterExpRate);
			}
		}
	}
	
	public static double getDungeon3ExpBuff(Player player) {
		int r = 0;
		for (int i = 0 ; i < 300 ; i += 25) {
			if (player.hasPermission("skycore.dungeon3.exp.buff." + i)) r = i; 
		}
		return r;
	}
	
	public static void gainExp(Player player, LivingEntity le, LeveledMob mob, double damage, double maxHealth) { 
		// Has buff
		String color = (getData(player).getExpBuff() != 0) ? "§6" : "§a";
		
		// Add
		double exp = ((double) damage / maxHealth) * mob.getMobType().getExpBonus() * mob.getLevel() * 0.1;
		exp = exp * (100 + getData(player).getExpBuff()) / 100;
		
		// Check Dungeon3
		if (Bukkit.getPluginManager().isPluginEnabled("Dungeon3") && DPlayerUtils.isInDungeon(player)) {
			// Check location
			if (!le.hasMetadata("Dungeon3")) return;
			String dID = DPlayerUtils.getCurrentDungeon(player);
			exp *= (double) (100 + SCDungeon3.getExpRate(dID)) * 0.01;
			exp *= (100 + getDungeon3ExpBuff(player)) * 0.01;
		}
		
		int expInt = new Double(exp).intValue();
		addExp(player, expInt);
		
		// Effect
		SRPGActionBar.send(player, color + "+" + expInt + " exp");
		Utils.sendSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.5f, 1f);
		
		// Party exp share
		for (Player p : PartyManager.get().getNearByMembers(player, 50)) {
			int expParty = expInt / 10;
			if (expParty != 0) {
				addExp(p, expParty);
			}
		}
	}
	
	public static String getChatPrefix(Player player) {
		int lv = player.getLevel();
 		
 		// Get color
 		String s = "§a";
 		String a = null;
 		if (player.hasMetadata("skycore-xacminh")) {
 			a = "§b✔ ";
 		} else a = "§7✘ ";
		
 		
 		return a + "§f[" + s + "Lv." + lv + "§f]§r";
	}
	
	public static String getTabPrefix(Player player) {
		int lv = player.getLevel();
		String a = null;
		
		if (player.hasMetadata("skycore-xacminh")) {
			a = "§b✔ ";
		} else a = "§7✘ ";
		
		// Get color
		String s = getLevelColorCode(lv);
		
		// Prefix
		s = s + "[Lv." + lv + "]§r";
		
		// Sky2XacMinh
//		if (Bukkit.getPluginManager().isPluginEnabled("Sky2XacMinh")) {
//			s = XacMinhUtils.getPrefixTab(player) + s;
//		}
		
		return a + s;
	}
	
	public static void setTabPrefix(Player player) {
		String prefix = getTabPrefix(player);
		player.setPlayerListName(prefix + " " + player.getName());
		
	}
	
	public static String getLevelColorCode(int lv) {
		Map<Integer, String> lvColor = new HashMap<Integer, String> ();
		lvColor.put(0, "§9");
		lvColor.put(20, "§a");
		lvColor.put(40, "§6");
		lvColor.put(60, "§e");
		int max = 0;
		String s = "§9";
		for (int i : lvColor.keySet()) {
			if (lv >= i) {
				if (i > max) {
					max = i;
					s = lvColor.get(i);
				}
			}
		}
		return s;
	}
	
	// Kill 50 quai: 4 phut 30 giay
	// Lv.1 => Lv.10: Giết 10 con quái cùng level
	// Lv.11 => Lv.20: Giết 100 con quái cùng level
	// Lv.21 => Lv.30: Giết 500 con quái cùng level
	// Lv.31 => Lv.50: Giết 2000 con quái cùng level 
	// Lv.51 => Lv.70: Giết 10000 con quái cùng level 
	// Lv.71 => Lv.90: Giết 50000 con quái cùng level 
	// Lv.91 => Lv.100: Giết 500000 con quái cùng level 
	public static int getExpToNextLevel(int level) {
		if (1 <= level && level <= 10) return level * 10;
		if (11 <= level && level <= 20) return level * 100;
		if (21 <= level && level <= 30) return level * 1000;
		if (31 <= level && level <= 50) return level * 5000;
		if (51 <= level && level <= 70) return level * 80000;
		if (71 <= level && level <= 90) return level * 1000000;
		if (91 <= level && level <= 100) return level * 10000000;
		return 0;
	}
	
	public static int getExpFrom0ToLevel(int level) {
		int sum = 0;
		for (int i = 0 ; i <= level ; i++) {
			sum += getExpToNextLevel(i);
		}
		return sum;
	}
	
	public static void levelUpEffet(Player player, int oldLevel, int newLevel) {
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			player.sendTitle(" §6§lLÊN CẤP RỒI", "§7+" + (newLevel - oldLevel) + " điểm tiềm năng", 1 * 20, 2 * 20, 1 * 20);
			Utils.sendSound(player, Sound.ENTITY_FIREWORK_TWINKLE, 1, 1);
			player.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, player.getLocation(), 50, 1, 1, 1, 1);
			setTabPrefix(player);
		});
	}
	
	public static void applyPhanDonPlayer(Player damager, Player target, double damage, int point) {
		if (point <= 0) return;
		if (biPhanDon.contains(target.getName())) {
			biPhanDon.remove(target.getName());
			return;
		}
		biPhanDon.add(damager.getName());
		Utils.damageWithoutEvent(target, damager, (double) damage * point / 100);
		
		Location loc = target.getLocation();
		loc.add(loc.getDirection().multiply(1.3f));
		Utils.hologram(Utils.randomLoc(loc, 1), "§c§lPHẢN ĐÒN", 15, target);
		Utils.hologram(Utils.randomLoc(loc, 1), "§2§lPHẢN ĐÒN", 15, damager);
	}
	
	public static void applyPhanDonEntity(Player damaged, LivingEntity target, double damage, int point) {
		if (point <= 0) return;
		Utils.damageWithoutEvent(damaged, target, (double) damage * point / 100);
		Location loc = damaged.getLocation();
		loc.add(loc.getDirection().multiply(1.3f));
		Utils.hologram(Utils.randomLoc(loc, 1), "§c§lPHẢN ĐÒN", 15, damaged);
	}
	
}
