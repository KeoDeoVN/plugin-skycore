package mk.plugin.skycore.grade;

public enum Grade {
	
	I(1, 1, "§f", 500),
	II(2, 1.4, "§9", 700),
	III(3, 1.8, "§c", 900),
	IV(4, 2.2, "§6", 1100),
	V(5, 2.6, "§a", 1500);
	
	private int n;
	private double bonus;
	private String color;
	
	private int durablity;
	
	private Grade(int n, double bonus, String color, int durability) {
		this.n = n;
		this.bonus = bonus;
		this.color = color;
		this.durablity = durability;
	}
	
	public int getNumber() {
		return this.n;
	}
	
	public double getBonus() {
		return this.bonus;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public int getDurability() {
		return this.durablity;
	}
	
	public static Grade fromNumber(int number) {
		for (Grade g : values()) {
			if (g.getNumber() == number) return g;
		}
		return null;
	}
	
}
