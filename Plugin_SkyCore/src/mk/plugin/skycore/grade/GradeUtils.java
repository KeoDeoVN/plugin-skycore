package mk.plugin.skycore.grade;

public class GradeUtils {
	
	public static int getExpRequirement(Grade grade) {
		switch (grade) {
		case I: return 0;
		case II: return 300;
		case III: return 600;
		case IV: return 1000;
		case V: return 1500;
		}
		return 0;
	}
	
	public static int getExpTo(Grade grade) {
		int exp = 0;
		for (Grade g : Grade.values()) {
			if (g.getNumber() <= grade.getNumber()) exp += getExpRequirement(g);
		}
		return exp;
	}
	
	public static Grade getNextGrade(Grade grade) {
		int lv = grade.getNumber();
		for (Grade g : Grade.values()) {
			if (g.getNumber() == lv + 1) return g;
		}
		return null;
	}
	
}
