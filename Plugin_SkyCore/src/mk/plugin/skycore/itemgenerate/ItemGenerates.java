package mk.plugin.skycore.itemgenerate;

import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.tier.Tier;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.item.ItemRank;
import mk.plugin.skycore.item.ItemType;
import mk.plugin.skycore.itemcategory.Armor;
import mk.plugin.skycore.itemcategory.Weapon;
import mk.plugin.skycore.stat.Stat;

public class ItemGenerates {
	
	private static final double SCALE_SPACE = 0.1;
	
	public static Map<Stat, Integer> generateStats(Map<Stat, Integer> basicStats, Tier tier) {
		Map<Stat, Integer> r = Maps.newHashMap();
		basicStats.entrySet().forEach(e -> {
			r.put(e.getKey(), new Double(e.getValue() * getMultiple(tier.getStatMultiple())).intValue());
		});
		return r;
	}
	
	public static Map<Stat, Integer> generateStats(Map<Stat, Integer> basicStats) {
		Tier tier = rateTier();
		Map<Stat, Integer> r = Maps.newHashMap();
		basicStats.entrySet().forEach(e -> {
			r.put(e.getKey(), new Double(e.getValue() * getMultiple(tier.getStatMultiple())).intValue());
		});
		return r;
	}
	
	public static List<ItemStack> generateArmors(Player player, ItemRank rank) {
		List<ItemStack> list = Lists.newArrayList();
		
		ItemType it = random(getArmors(rank));
		String setID = ((Armor) it.getCategory()).getSetID();
		
		Tier tier = rateTier();
		Map<Stat, Integer> stats = generateStats(it.getBasicStats(), tier);
		
		for (ItemType type : getArmors(rank)) {
			// Check set
			Armor a = (Armor) type.getCategory();
			if (!a.getSetID().equalsIgnoreCase(setID)) continue;
			
			// Do
			ItemStack is = type.createItemStack(player);
			Item item = ItemAPI.get(is);
			ItemData data = item.getData();
			
			data.setTier(tier);
			data.setStats(stats);
			
			list.add(ItemAPI.set(player, is, item));
		}
		
		return list;
	}
	
	public static ItemType generateWeapon(double superChance, double wowChance) {
		if (Utils.rate(superChance)) return random(getWeapons(ItemRank.SUPER));
		if (Utils.rate(wowChance)) return random(getWeapons(ItemRank.WOW));
		return random(getWeapons(ItemRank.VIP));
	}
	
	public static ItemStack generateWeapon(Player player, ItemRank rank) {
		ItemType it = random(getWeapons(rank));
		return generateWeapon(player, it, null);
	}
	
	public static ItemStack generateWeapon(Player player, ItemRank rank, Tier tier) {
		ItemType it = random(getWeapons(rank));
		return generateWeapon(player, it, tier);
	}
	
	public static ItemStack generateWeapon(Player player, ItemType it, Tier tier) {
		ItemStack is = it.createItemStack(player);
		Item item = ItemAPI.get(is);
		ItemData id = item.getData();
		
		tier = tier == null ? rateTier() : tier;
		Map<Stat, Integer> stats = Maps.newHashMap();
		Tier t = tier;
		it.getBasicStats().forEach((st, v) -> {
			stats.put(st, new Double(v * t.getStatMultiple()).intValue());
		});
		id.setStats(stats);
		id.setTier(tier);
		
		return ItemAPI.set(player, is, item);
	}
	
	public static List<ItemType> getWeapons(ItemRank rank) {
		List<ItemType> list = Lists.newArrayList();
		for (ItemType it : ItemType.values()) {
			if (it.getCategory() instanceof Weapon && it.getRank() == rank) list.add(it);
		}
		return list;
	}
	
	public static List<ItemType> getArmors(ItemRank rank) {
		List<ItemType> list = Lists.newArrayList();
		for (ItemType it : ItemType.values()) {
			if (it.getCategory() instanceof Armor && it.getRank() == rank) list.add(it);
		}
		return list;
	}
	
	private static double getMultiple(double source) {
		return source + Utils.random(SCALE_SPACE * -1, SCALE_SPACE);
	}
	
	public static Tier rateTier() {
		if (Utils.rate(0.5)) return Tier.HUYEN_THOAI;
		if (Utils.rate(2)) return Tier.CUC_PHAM;
		if (Utils.rate(10)) return Tier.CAO_CAP;
		if (Utils.rate(50)) return Tier.TRUNG_CAP;
		
		return Tier.SO_CAP;
	}
	
	private static ItemType random(List<ItemType> list) {
		return list.get(Utils.randomInt(0, list.size() - 1));
	}
	
	

}
