package mk.plugin.skycore.placeholder;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.power.SRPGPowerUtils;
import kdvn.sky2.rpg.core.utils.Utils;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import mk.plugin.skycore.player.PlayerUtils;

public class SkyCorePlaceholder extends PlaceholderExpansion {

	@Override
	public String getAuthor() {
		return "MankaiStep";
	}

	@Override
	public String getIdentifier() {
		return "skycore";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	@Override
    public String onPlaceholderRequest(Player player, String s){
		if (s.equalsIgnoreCase("player_power")) {
			return SRPGPowerUtils.calculatePower(player) + "";
		}
		
		else if (s.equalsIgnoreCase("player_exp")) {
			return new Double(PlayerUtils.getExpFrom0ToLevel(player.getLevel()) + PlayerUtils.getExpToNextLevel(player.getLevel() + 1) * player.getExp()).intValue() + ""; 
		}
		
		else if (s.equalsIgnoreCase("player_island_level")) {
			return "" + Utils.getIslandLevel(player);
		}
		
		else if (s.equalsIgnoreCase("player_xacminh")) {
			if (player.hasMetadata("skycore-xacminh")) {
				return "§b✔ ";
			} else return "§7✘ ";
		}
		
		return "Wrong placeholder";
	}
	
}
