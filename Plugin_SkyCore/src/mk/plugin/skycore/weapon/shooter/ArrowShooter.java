package mk.plugin.skycore.weapon.shooter;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.stat.Stat;
import mk.plugin.skycore.weapon.Shooter;

public class ArrowShooter implements Shooter {

	@Override
	public void shoot(Player player, Item item) {
		Arrow arrow = player.launchProjectile(Arrow.class);
		Damages.setProjectileDamage(arrow, new Damage(PlayerUtils.getStatValue(player, Stat.SAT_THUONG), DamageType.ATTACK));
		player.playSound(player.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);
		Bukkit.getScheduler().runTaskLater(MainSkyCore.getMain(), () -> {
			arrow.remove();
		}, 10);
	}

}
