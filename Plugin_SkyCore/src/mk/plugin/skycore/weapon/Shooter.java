package mk.plugin.skycore.weapon;

import org.bukkit.entity.Player;

import mk.plugin.skycore.item.Item;

public interface Shooter {
	
	public void shoot(Player player, Item item);
	
}
