package kdvn.sky2.rpg.core.damagecheck;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class SRPGDamageCheck {
	
	private static Map<LivingEntity, SRPGDamageCauser> damageMap = new HashMap<LivingEntity, SRPGDamageCauser> ();
	
	public static void addDamage(LivingEntity m, Player player, double damage) {
		if (damageMap.containsKey(m)) {
			SRPGDamageCauser dc = damageMap.get(m);
			dc.addDamage(player.getName(), damage);
			damageMap.put(m, dc);
		}
		else {
			SRPGDamageCauser dc = new SRPGDamageCauser();
			dc.addDamage(player.getName(), damage);
			damageMap.put(m, dc);
		}
	}
	
	public static Map<String, Double> getDamage(LivingEntity m) {
		if (damageMap.containsKey(m)) {
			return damageMap.get(m).getMap();
		}
		HashMap<String, Double> map = new HashMap<String, Double> ();
		Player killer = m.getKiller();
		if (killer != null) {
			map.put(killer.getName(), m.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
		}
		
		return map;
	}
	
	public static double getDamage(LivingEntity m, Player player) {
		if (damageMap.containsKey(m)) {
			SRPGDamageCauser dc = damageMap.get(m);
			return dc.get(player.getName());
		}
		
		return 0;
	}
	
	public static void remove(LivingEntity entity) {
		if (damageMap.containsKey(entity)) damageMap.remove(entity);
	}
	
}
