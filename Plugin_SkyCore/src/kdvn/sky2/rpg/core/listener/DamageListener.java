package kdvn.sky2.rpg.core.listener;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.Statistic;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.damagecheck.SRPGDamageCheck;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.mob.LeveledMobs;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.hologram.HologramUtils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.stat.Stat;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class DamageListener implements Listener {
	
	public final int ENTITY_DEFAULT_SUCTHU = 50;

	// Player damaged armor
	@EventHandler
	public void onPlayerDamaged(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player == false) return;
		Player player = (Player) e.getEntity();
		
		// No break in world pvp
		if (MainSkyCore.WORLD_PVPS.contains(player.getWorld().getName())) return;
		
		// Durability
		double lastHealth = ((LivingEntity) e.getEntity()).getHealth();
		Bukkit.getScheduler().runTask(MainSkyCore.get(), () -> {
			double realDamage = lastHealth - ((LivingEntity) player).getHealth();
			if (e.getEntity() instanceof Player) {
				ItemStack[] armors =  player.getInventory().getArmorContents();
				for (int i = 0 ; i < armors.length ; i++) {
					ItemStack is = armors[i];
					if (is != null && ItemAPI.check(is) && realDamage > 5) {
						Item item = ItemAPI.get(is);
						ItemData data = item.getData();
						if (data.getDurability() == 0) {
							player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder("§cMột giáp của bạn có độ bền bằng 0 và mất tác dụng, hãy đi sửa chữa").create());
							continue;
						}
						data.setDurability(Math.max(0, data.getDurability() - 1));
						armors[i] = ItemAPI.set(player, is, item);
						
					}
				}
				player.getInventory().setArmorContents(armors);
			}
		});
	}
	
	// Player get damaged by entity not player
	@EventHandler
	public void onPlayerDamagedByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player == false) return;
		if (e.getDamager() instanceof Player || e.getDamager() instanceof LivingEntity == false) return;
		
		Player player = (Player) e.getEntity();
		LivingEntity entity = (LivingEntity) e.getDamager();
		
		double ne = PlayerUtils.getStatValue(player, Stat.NE);
		if (Utils.rate(ne)) {
			HologramUtils.hologram(MainSky2RPGCore.getMain(), "§a§oNé", 15, player, entity, 1);
			e.setCancelled(true);
			return;
		}
		
		double sucThu = PlayerUtils.getStatValue(player, Stat.SUC_THU);
		e.setDamage(e.getDamage() * (1 - sucThu * 0.01));
		

		
	}
	
	// Player damage entity
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerDamageEntity(EntityDamageByEntityEvent e) {
		
		// Check Entity
		Entity en = e.getEntity();
		if (!Utils.canAttack(en)) {
			e.setCancelled(true);
			return;
		}
		
		// Player Damage entity
		if (e.getDamager() instanceof Player || e.getDamager() instanceof Projectile) {
			if (e.getEntity() instanceof LivingEntity) {
				// Projectile
				boolean isProjectileDamage = false;
				
				Damage d = null;
				double damage = 0;
				
				// Get player damager
				Player p;
				if (e.getDamager() instanceof Projectile) {
					Projectile a = (Projectile) e.getDamager();
					if (a.getShooter() instanceof Player) {
						p = (Player) a.getShooter();
						
						// Check projectile
						if (!Damages.hasProjectileDamage(a)) return;
						isProjectileDamage = true;
						d = Damages.getProjectileDamage(a);
						damage = d.getValue();
						if (a instanceof Arrow) {
							((Arrow) a).setBounce(false);
						}
						
						// Check if shooter = entity
						if (p == e.getEntity()) {
							e.setCancelled(true);
							return;
						}
						
					} else return;
				} else p = (Player) e.getDamager();
				Player player = p;
				
				LivingEntity entity = (LivingEntity) e.getEntity();

				// Check god
				if (Utils.isGod(entity)) {
					e.setCancelled(true);
					return;
				}

				// Check sneak
				if (player.isSneaking()) {
					e.setCancelled(true);
					return;
				}
				
				// Check delay
				if (Damages.isDelayed(entity)) {
					e.setCancelled(true);
					return;
				}

				// Check if not projectile
				if (!isProjectileDamage) {
					// Check is SkyCore damage
					if (!Damages.hasDamage(entity)) {
						e.setCancelled(true);
						return;
					}
					d = Damages.getDamage(entity);
					Damages.removeDamage(entity);
					damage = d.getValue();
				}

				// Hologram list
				List<String> holos = Lists.newArrayList();
				
				
				boolean crit = false;
				// Check if attack
				if (d.getType() == DamageType.ATTACK) {
					
					// Chi mang
					double critChance = PlayerUtils.getStatValue(player, Stat.CHI_MANG);
					if (Utils.rate(critChance)) {
						crit = true;
						damage *= 2;
						
						// Effet Crit
						Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
							Location loc = entity.getLocation();
							loc.getWorld().spawnParticle(Particle.CLOUD, loc.clone().add(0, 1.5, 0.0), 7);
							Utils.sendSound(player, Sound.BLOCK_ANVIL_LAND, 0.3f, 1f);
						});
					}
					
					// Suc thu, xuyen giap
					if (!(entity instanceof Player)) {
						if (!Utils.rate(PlayerUtils.getStatValue(player, Stat.XUYEN_GIAP))) {
							double defenseValue = ENTITY_DEFAULT_SUCTHU;
							damage = damage * (1 - ((double) defenseValue / 100));
						} else 	holos.add("§a§oXuyên giáp");
					}
					
					// If target is player
					if (e.getEntity() instanceof Player) {
						Player target = (Player) e.getEntity();
						
						// Stat Ne
						if (Utils.rate(PlayerUtils.getStatValue(target, Stat.NE))) {
							Location loc = player.getLocation();
							loc.add(loc.getDirection().multiply(1.3f));
							Utils.hologram(Utils.randomLoc(loc, 1), "§2§lNé", 15, target);
							Utils.hologram(Utils.randomLoc(loc, 1), "§c§lNé", 15, player);
							e.setCancelled(true);
							return;
						}
						
						// Xuyen giap
						if (!Utils.rate(PlayerUtils.getStatValue(player, Stat.XUYEN_GIAP))) {
							damage = damage * (1 - PlayerUtils.getStatValue(target, Stat.SUC_THU) * 0.01);
						} else holos.add("§a§oXuyên giáp");
						
					}					
				}
				
				// Check world pvp
				if (MainSkyCore.WORLD_PVPS.contains(player.getWorld().getName())) {
					// only 25% damage
					damage *= 0.25;
				}
				
				// End attack
				entity.setNoDamageTicks(0);
				entity.setMaximumNoDamageTicks(0);
				e.setDamage(damage);
				
				// Holograms
				holos.add(0, crit ? "§6§l§o-" + Utils.round(damage) : "§c§l§o-" + Utils.round(damage));
				
				// After damage
				double lastDamage = damage;
				double lastHealth = ((LivingEntity) e.getEntity()).getHealth();
				new BukkitRunnable() {
					@Override
					public void run() {
						// Hut mau
						double value = PlayerUtils.getStatValue(player, Stat.HUT_MAU);
						Utils.addHealth(player, (double) lastDamage * value / 100);
						
						// Holograms
						HologramUtils.hologram(MainSky2RPGCore.getMain(), holos, 15, player, entity, 1);
						
						// Armor damage
						double realDamage = lastHealth - ((LivingEntity) entity).getHealth();
						
						// Damage check
						if (LeveledMobs.isMob(entity)) SRPGDamageCheck.addDamage(entity, player, realDamage);
						
						// Statistic
						player.setStatistic(Statistic.DAMAGE_DEALT, player.getStatistic(Statistic.DAMAGE_DEALT) + new Double(realDamage).intValue());
						if (e.getEntity() instanceof Player) {
							Player target = (Player) e.getEntity();
							target.setStatistic(Statistic.DAMAGE_TAKEN, target.getStatistic(Statistic.DAMAGE_TAKEN) + new Double(realDamage).intValue());
						}

					}
				}.runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), 1);
			}
			
			// End event check
		}
		
	}

}
