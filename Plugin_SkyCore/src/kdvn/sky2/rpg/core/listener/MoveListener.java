package kdvn.sky2.rpg.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import mk.plugin.skycore.limitedland.LimitedLand;

public class MoveListener implements Listener {
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		LimitedLand.limitedLands.forEach((id, ll) -> {
			if (player.hasPermission("limitedland." + id) && !player.hasPermission("skycore.admin")) {
				Location tl = ll.getLocation();
				if (tl.getWorld() != player.getWorld()) return;
				if (player.getLocation().distance(tl) > ll.getRadius()) {
					Bukkit.getScheduler().runTask(MainSkyCore.get(), () -> {
						player.teleport(ll.getLocation());
						player.sendMessage(ll.getMessage());
						player.playSound(player.getLocation(), Sound.ENTITY_GHAST_AMBIENT, 1, 1);
					});
				}
			}
		});
	}
	
}
