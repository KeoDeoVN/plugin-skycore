package kdvn.sky2.rpg.core.listener;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.server.TabCompleteEvent;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.ChatPlaceholder;
import mk.plugin.skycore.player.PlayerUtils;

public class ChatListener implements Listener {
	
	// Tab tag
	@EventHandler
	public void onTabComplete(TabCompleteEvent e) {
		String s = e.getBuffer();
		if (s.contains("@")) {
			String regex = "@(?<preName>\\S+)";
			
			Pattern pt = Pattern.compile(regex);
			Matcher m = pt.matcher(s);
			
			String preName = null;
			while (m.find()) {
				preName = m.group("preName");
			}
			if (preName == null) return;
			
			List<String> avl = Lists.newArrayList();
			for (Player p : Bukkit.getOnlinePlayers()) {
				if (p.getName().toLowerCase().startsWith(preName.toLowerCase())) {
					avl.add("@" + p.getName());
				}
			}

			e.setCompletions(avl);
		}
	}
	
	// Chatplaceholder
	@EventHandler
	public void onPlaceholderChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		if (ChatPlaceholder.cps.containsKey(player)) {
			player.sendMessage("§7Yêu cầu chat kết thúc!");
			ChatPlaceholder cp = ChatPlaceholder.cps.get(player);
			String s = e.getMessage();
			cp.runCmd(s.replace("&", "§"), MainSky2RPGCore.getMain());
			ChatPlaceholder.cps.remove(player);
			e.setCancelled(true);
		}
	}
	
	// Prefix chat
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		String prefix = PlayerUtils.getChatPrefix(player);
		e.setFormat(prefix + " " + e.getFormat());
		
		// Sky2XacMinh
//		if (Bukkit.getPluginManager().isPluginEnabled("Sky2XacMinh")) {
//			e.setFormat(XacMinhUtils.getPrefixChat(player) + e.getFormat());
//		}

	}
	
}
