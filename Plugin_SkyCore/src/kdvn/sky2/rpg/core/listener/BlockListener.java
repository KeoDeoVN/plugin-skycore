package kdvn.sky2.rpg.core.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import mk.plugin.sky2rpgcore.specialmine.SMineUtils;
import mk.plugin.skycore.crate.Crates;

public class BlockListener implements Listener {
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		SMineUtils.playerBreak(e);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Crates.eventInteract(e);
	}
}
