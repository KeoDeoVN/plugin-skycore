package kdvn.sky2.rpg.core.listener;

import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.EquipmentSlot;

import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.combo.ComboUtils;

public class ComboListener implements Listener {
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.LEFT_CLICK_AIR) return;
		if (!ComboUtils.isPlayerComboed(e.getPlayer())) return;
		if (e.getHand() != EquipmentSlot.HAND) return;

		Utils.sendSound(e.getPlayer(), Sound.BLOCK_WOOD_BUTTON_CLICK_ON, 1f, 1f);
		if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
			ComboUtils.addOne(e.getPlayer(), 0);
			ComboUtils.sendTitleCombo(e.getPlayer());
		}
		else if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			ComboUtils.addOne(e.getPlayer(), 1);
			ComboUtils.sendTitleCombo(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {
		if (e.isSneaking()) {
			ComboUtils.addCombo(e.getPlayer());
		} else {
			ComboUtils.comboExecute(e.getPlayer());
			ComboUtils.huyCombo(e.getPlayer());
		}
		
	}
	
}
