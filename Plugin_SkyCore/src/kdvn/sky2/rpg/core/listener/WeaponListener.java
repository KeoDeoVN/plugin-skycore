package kdvn.sky2.rpg.core.listener;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.damage.Damage;
import mk.plugin.skycore.damage.DamageType;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.event.ItemToggleEvent;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.itemcategory.Weapon;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.stat.Stat;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class WeaponListener implements Listener {
	
	private static Map<String, Long> cooldownAttack = new HashMap<String, Long> ();	
	
	public static boolean isCooldownAttack(Player player) {
		if (!cooldownAttack.containsKey(player.getName())) return false;
		if (cooldownAttack.get(player.getName()) < System.currentTimeMillis()) return false;
		return true;
	}
	
	public static void setCooldownAttack(Player player, long timeMilis, ItemStack item) {
		if (item != null) player.setCooldown(item.getType(), new Long(timeMilis / 50).intValue());
		cooldownAttack.put(player.getName(), System.currentTimeMillis() + timeMilis);	
	}
	
	public static void removeCooldownAttack(Player player) {
		cooldownAttack.remove(player.getName());
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onToggle(ItemToggleEvent e) {
		Player player = e.getPlayer();
		ItemStack is = e.getItemStack();
		Item item = e.getItem();
		if (item.getType().getCategory() instanceof Weapon) {
			
			// Check durability
			ItemData data = item.getData();
			if (data.getDurability() <= 0) {
				player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder("§cVũ khí có độ bền bằng 0, cần phải sữa chữa").create());
				return;
			}
			
			// Check cooldown
			if (isCooldownAttack(player)) return;
			setCooldownAttack(player, new Double(PlayerUtils.getStatValue(player, Stat.TOC_DANH) * 1000).longValue(), is);
			
			// Check attack
			Weapon weapon = (Weapon) item.getType().getCategory();
			
			// Shoot
			if (weapon.isShooter()) {
				weapon.getShooter().shoot(player, item);
			}
			// Attack
			else {
				double range = weapon.getRange();
				LivingEntity target = Utils.getTarget(player, range);
				if (target == null) return;
				Damages.damage(player, target, new Damage(PlayerUtils.getStatValue(player, Stat.SAT_THUONG), DamageType.ATTACK), 0);
			}
			
			// Minus durability
			if (MainSkyCore.WORLD_PVPS.contains(player.getWorld().getName())) return;
			data.setDurability(Math.max(data.getDurability() - 1, 0));
			player.getInventory().setItemInMainHand(ItemAPI.set(player, is, item));
		}
	}
	
}
