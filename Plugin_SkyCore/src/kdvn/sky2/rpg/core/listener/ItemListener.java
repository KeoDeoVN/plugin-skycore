package kdvn.sky2.rpg.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerAnimationType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.tier.Tier;
import mk.plugin.skycore.damage.Damages;
import mk.plugin.skycore.event.ItemToggleEvent;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.gem.Gem;
import mk.plugin.skycore.item.gem.GemUtils;
import mk.plugin.skycore.item.randomgem.RandomGem;
import mk.plugin.skycore.itemtexture.ItemTexture;
import mk.plugin.skycore.stat.Stat;

public class ItemListener implements Listener {

	public final String DAMAGE_TAG = "item.damagetag";
	public final String INTERACT_TAG = "item.interact";
	
	// Use item
	@EventHandler
	public void onRG(PlayerInteractEvent e) {
		ItemStack is = e.getItem();
		if (is == null) return;
		Player player = e.getPlayer();
		if (RandomGem.isThatItem(is)) {
			e.setCancelled(true);
			RandomGem rg = RandomGem.fromItem(is);
			Tier tier = rg.rate();
			
			// Set item
			if (is.getAmount() == 1) player.getInventory().setItemInMainHand(null);
			else is.setAmount(is.getAmount() - 1);
			
			// Task
			Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
				Stat stat = Stat.values()[Utils.randomInt(0, Stat.values().length - 1)];
				Gem gem = new Gem("§6§lNgọc §c§l" + stat.getName(), stat, tier.getBonus(), tier);
				ItemStack item = GemUtils.setGem(new ItemStack(Material.IRON_NUGGET), gem);
				Utils.giveItem(player, item);
				player.sendMessage("§aNhận được ngọc §6§o" + stat.getName() + " §e§o" + tier.getName().toLowerCase());
				player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
			});
		}
	}
	
	/*
	 *  Animation = last
	 */
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if (e.getAction() == Action.LEFT_CLICK_AIR) {
			player.setMetadata(INTERACT_TAG, new FixedMetadataValue(MainSkyCore.getMain(), ""));
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity) {
			Player player = (Player) e.getDamager();
			LivingEntity target = (LivingEntity) e.getEntity();
			if (Damages.hasDamage(target)) return;
			player.setMetadata(DAMAGE_TAG, new FixedMetadataValue(MainSkyCore.getMain(), ""));
		}
	}
	
	@EventHandler
	public void onAnimation(PlayerAnimationEvent e) {
		Player player = e.getPlayer();
		if (player.isSneaking()) return;
		if (e.getAnimationType() == PlayerAnimationType.ARM_SWING) {
			// Check
			boolean hasDamage = player.hasMetadata(DAMAGE_TAG);
			boolean hasInteract = player.hasMetadata(INTERACT_TAG);
			
			// Remove tags
			player.removeMetadata(DAMAGE_TAG, MainSkyCore.getMain());
			player.removeMetadata(INTERACT_TAG, MainSkyCore.getMain());
			
			// Event
			if (hasDamage || hasInteract) {
				// Call event
				ItemStack is = player.getInventory().getItemInMainHand();
				if (ItemAPI.check(is)) {
					Item item = ItemAPI.get(is);
					Bukkit.getPluginManager().callEvent(new ItemToggleEvent(player, item, is));
				}
			}
		}
	}
	
	
	
	/*
	 * Cancel player owning textured items 
	 */
	
	@EventHandler
	public void onCraft(PrepareItemCraftEvent e) {
		if (e.getRecipe() == null) return;
		ItemStack result = e.getRecipe().getResult();
		if (result == null) return;
		
		CraftingInventory inv = e.getInventory();
		for (ItemTexture texture : MainSkyCore.CUSTOM_TEXTURES) {
			if (result.getType() == texture.getMaterial() && result.getDurability() == texture.getDurability()) {
				inv.setResult(new ItemStack(Material.AIR));
				return;
			}
		}

	}
	
	@EventHandler
	public void onDrop(ItemSpawnEvent e) {
		ItemStack result = e.getEntity().getItemStack();
		for (ItemTexture texture : MainSkyCore.CUSTOM_TEXTURES) {
			if (result.isSimilar(new ItemStack(texture.getMaterial(), 1, (short) texture.getDurability()))) {
				e.setCancelled(true);
				return;
			}
		}
	}
	
	@EventHandler
	public void onDamageItem(PlayerItemDamageEvent e) {
		ItemStack is = e.getItem();
		for (ItemTexture texture : MainSkyCore.CUSTOM_TEXTURES) {
			if ((new ItemStack(is.getType(), 1, new Integer(is.getDurability() + e.getDamage()).shortValue())).isSimilar(new ItemStack(texture.getMaterial(), 1, (short) texture.getDurability()))) {
				e.setCancelled(true);
				return;
			}
		}
	}
	
	
}
