package kdvn.sky2.rpg.core.listener;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import mk.plugin.skycore.buff.ExpBuff;
import mk.plugin.skycore.player.PlayerUtils;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import remvn.reanticheatspigot.event.PlayerCheckedEvent;

public class XacMinhListener implements Listener {
	
	public static List<String> XACMINH_PERMISSIONS = Lists.newArrayList();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		// Remove XM permission
		Player player = e.getPlayer();
		User user = LuckPermsProvider.get().getUserManager().getUser(player.getUniqueId());
		XACMINH_PERMISSIONS.forEach(perm -> {
			user.data().remove(Node.builder(perm).build());
		});
		LuckPermsProvider.get().getUserManager().saveUser(user);
		player.removeMetadata("skycore-xacminh", MainSkyCore.get());
		
		Bukkit.getScheduler().runTaskLater(MainSkyCore.get(), () -> {
			// Remove buff
		//	SkyPlayer sp = PlayerUtils.getData(player);

		}, 200);

	}
	
	@EventHandler
	public void onXM(PlayerCheckedEvent e) {
		Player player = e.getPlayer();
		User user = LuckPermsProvider.get().getUserManager().getUser(player.getUniqueId());
		XACMINH_PERMISSIONS.forEach(perm -> {
			user.data().add(Node.builder(perm).build());
		});
		LuckPermsProvider.get().getUserManager().saveUser(user);
		player.setMetadata("skycore-xacminh", new FixedMetadataValue(MainSkyCore.get(), ""));
		
		Bukkit.getScheduler().runTaskLater(MainSkyCore.get(), () -> {
			// Add buff
			PlayerUtils.buffExp(player, new ExpBuff(20, System.currentTimeMillis(), 86400 * 1000, "skycore-xacminh"));
		}, 40);
		
		//
		
		player.sendTitle("§b§l§oĐÃ XÁC MINH", "§fNhận buff và tích xanh", 20, 40, 20);
		
	}
	
	
}
