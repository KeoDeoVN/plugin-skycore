package kdvn.sky2.rpg.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;

import kdvn.sky2.rpg.core.damagecheck.SRPGDamageCheck;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.mob.LeveledMob;
import kdvn.sky2.rpg.core.mob.LeveledMobs;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.repair.RepairStone;

public class ExpListener implements Listener {
	
	// Level up
	@EventHandler
	public void onLevelUp(PlayerLevelChangeEvent e) {
		if (e.getNewLevel() <= e.getOldLevel()) {
			return;
		}
		int newLevel = e.getNewLevel();
		if (newLevel > PlayerUtils.MAX_LEVEL) {
			newLevel = PlayerUtils.MAX_LEVEL;
			e.getPlayer().setLevel(PlayerUtils.MAX_LEVEL);
			return;
		}

		int newLv = newLevel;
		int oldLv = e.getOldLevel();
		PlayerUtils.levelUpEffet(e.getPlayer(), oldLv, newLv);
		
	}
	
	// Kill mob
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onKillMob(EntityDeathEvent e) {
		if (e.getEntity() instanceof LivingEntity) {
			Player playerK = e.getEntity().getKiller();
			if (playerK == null)
				return;
			
			LivingEntity le = (LivingEntity) e.getEntity();
			if (!LeveledMobs.isMob(le)) return;
			
			LeveledMob rpgMob = LeveledMobs.fromMob(le);
			
			// Check damage and gain exp
			double maxHealth = le.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
			Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
				SRPGDamageCheck.getDamage(le).forEach((player, damage) -> {
					Player p = Bukkit.getPlayer(player);
					if (p == null) return;
					PlayerUtils.gainExp(p, le, rpgMob, damage, maxHealth);
				});
			}, 4);
			
			// Drop RepairStone
			if (rpgMob.getLevel() > playerK.getLevel() && Utils.rate(5)) {
				le.getLocation().getWorld().dropItem(le.getLocation(), RepairStone.getItem());
			}
		}
	}
	
}
