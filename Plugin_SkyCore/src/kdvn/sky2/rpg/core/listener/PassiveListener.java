package kdvn.sky2.rpg.core.listener;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mk.plugin.skycore.passive.Passive;
import mk.plugin.skycore.passive.Passives;

public class PassiveListener implements Listener {

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			if (Passives.hasPassive(player, Passive.KHO_HEO) && e.getDamager() instanceof LivingEntity) {
				LivingEntity le = ((LivingEntity) e.getDamager());
				le.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 40, 1));
			}
			else if (Passives.hasPassive(player, Passive.HOI_PHUC)) {
				player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 40, 1));
			}
		}
		
		if (e.getDamager() instanceof Player) {
			Player player = (Player) e.getDamager();
			if (Passives.hasPassive(player, Passive.NHANH_NHEN)) {
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20, 0));
			}
			else if (Passives.hasPassive(player, Passive.BANG_GIA) && e.getEntity() instanceof LivingEntity) {
				LivingEntity le = (LivingEntity) e.getEntity();
				le.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 30, 1));
			}
		}
	}
	
}
