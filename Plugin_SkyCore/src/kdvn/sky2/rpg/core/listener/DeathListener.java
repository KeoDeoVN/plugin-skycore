package kdvn.sky2.rpg.core.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import mk.plugin.skycore.item.keepgem.KeepGem;

public class DeathListener implements Listener {
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		if (player.hasPermission("dabaoho.bypass") || MainSkyCore.WORLD_PVPS.contains(player.getWorld().getName())) {
			e.setKeepInventory(true);
			e.setKeepLevel(true);
			e.getDrops().clear();
			e.setDroppedExp(0);
			player.sendMessage("§aBạn được giữ lại đồ và cấp độ");
			return;
		}
		ItemStack[] contents = player.getInventory().getContents();
		boolean keep = false;
		for (int i = 0 ; i < contents.length ; i++) {
			ItemStack item = contents[i];
			if (item != null) {
				if (KeepGem.isThatItem(item)) {
					keep = true;
					player.sendMessage("§aBạn được giữ lại cấp độ và đồ nhờ Đá bảo hộ");
					e.setKeepInventory(true);
					e.setKeepLevel(true);
					e.getDrops().clear();
					e.setDroppedExp(0);
					if (item.getAmount() == 1) contents[i] = null;
					else item.setAmount(item.getAmount() - 1);
					player.getInventory().setContents(contents);
					break;
				}
			}
		}
		if (!keep) {
			player.sendMessage("§aChết mất đồ vì không có Đá bảo hộ");
			e.setKeepInventory(false);
			e.setKeepLevel(true);
		}
	}
	
}
