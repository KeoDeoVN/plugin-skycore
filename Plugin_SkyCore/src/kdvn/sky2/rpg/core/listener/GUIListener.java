package kdvn.sky2.rpg.core.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

import mk.plugin.sky2rpgcore.itemgui.SItemGUI;
import mk.plugin.skycore.crate.Crates;
import mk.plugin.skycore.customcraft.CraftMenuGUI;
import mk.plugin.skycore.customcraft.CraftRecipeGUI;
import mk.plugin.skycore.customcraft.CraftStorageGUI;
import mk.plugin.skycore.gui.enhance.GUIEnhance;
import mk.plugin.skycore.gui.gem.GUIGemAdd;
import mk.plugin.skycore.gui.gem.GUIGemDispart;
import mk.plugin.skycore.gui.gem.GUIGemDrill;
import mk.plugin.skycore.gui.grade.GUIGrade;
import mk.plugin.skycore.gui.itemshow.GUIItemShow;
import mk.plugin.skycore.gui.jewelry.GUIJewelry;
import mk.plugin.skycore.gui.player.GUIPlayerElement;
import mk.plugin.skycore.gui.player.GUIPlayerInfo;
import mk.plugin.skycore.gui.player.GUIPlayerSee;
import mk.plugin.skycore.repair.RepairStone;

public class GUIListener implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		GUIPlayerInfo.eventClick(e);
		GUIPlayerElement.eventClick(e);
		GUIPlayerSee.eventClick(e);
		GUIEnhance.eventClick(e);
		GUIGemAdd.eventClick(e);
		GUIGemDrill.eventClick(e);
		GUIGemDispart.eventClick(e);
		GUIGrade.eventClick(e);
		SItemGUI.eventClick(e);
		GUIItemShow.eventClick(e);
		GUIJewelry.eventClick(e);
		CraftRecipeGUI.eventClick(e);
		CraftStorageGUI.eventClick(e);
		CraftMenuGUI.eventClick(e);
		RepairStone.onClick(e);
		Crates.eventClick(e);
	}
	
	@EventHandler
	public void onDrag(InventoryDragEvent e) {
		GUIPlayerSee.eventDrag(e);
		CraftRecipeGUI.eventDrag(e);
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		GUIEnhance.eventClose(e);
		GUIGemAdd.eventClose(e);
		GUIGemDrill.eventClose(e);
		GUIGemDispart.eventClose(e);
		GUIGrade.eventClose(e);
		SItemGUI.eventClose(e);
		GUIJewelry.eventClose(e);
	}
	
}
