package kdvn.sky2.rpg.core.listener;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.player.PlayerUtils;

public class DataListener implements Listener{
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		PlayerUtils.playerJoin(player);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		PlayerUtils.playerQuit(player);
		
		// Island
		Utils.savedIslandLevel.put(player.getName(), Utils.getIslandLevel(player));
	}
	
	@EventHandler
	public void onCloseInv(InventoryCloseEvent e) {
		if (e.getInventory().getType() != InventoryType.CRAFTING) return;
		Player player = (Player) e.getPlayer();
		PlayerUtils.updatePlayer(player);
	}
	
	@EventHandler
	public void onSwitchItem(PlayerItemHeldEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTask(MainSkyCore.get(), () -> {
			PlayerUtils.updatePlayer(player);
		});
	}
	
	@EventHandler
	public void onSwitchHand(PlayerSwapHandItemsEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTask(MainSkyCore.get(), () -> {
			PlayerUtils.updatePlayer(player);
		});
	}
	
	// Update item
	@EventHandler
	public static void onOpenInventory(InventoryCloseEvent e) {
		Inventory inv = e.getInventory();
		if (inv.getType() == InventoryType.CRAFTING) {
			Player player = (Player) e.getPlayer();
			PlayerInventory pi = player.getInventory();
			List<ItemStack> list = Lists.newArrayList(pi.getArmorContents());
			list.add(pi.getItemInMainHand());
			for (ItemStack item : list) {
				if (item != null) {
					if (ItemAPI.check(item)) {
						ItemAPI.update(item, player);
					}
				}
			}
		}
	}
	
	
}
