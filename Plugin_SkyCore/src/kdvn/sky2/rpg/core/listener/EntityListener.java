package kdvn.sky2.rpg.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.mob.LeveledMobs;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.specialmine.SMineUtils;

public class EntityListener implements Listener {
	
	@EventHandler
	public void onSpawn(EntitySpawnEvent e) {
		Entity entity = e.getEntity();
		Utils.setGod(entity, 1000);
		
		// Set stat
		if (entity instanceof LivingEntity) {
			LivingEntity le = (LivingEntity) entity;
			Bukkit.getScheduler().runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), () -> {
				if (LeveledMobs.checkName(le)) {
					LeveledMobs.setRPGFromName(le, le.getName());
				}
			}, 5);
		}
		
		// Check entity in mine worlds
		if (entity instanceof LivingEntity && MainSkyCore.SPECIAL_MINE_WORLDS.contains(entity.getWorld().getName()))
			SMineUtils.checkMobSpawn((LivingEntity) entity);
	}
	
}
