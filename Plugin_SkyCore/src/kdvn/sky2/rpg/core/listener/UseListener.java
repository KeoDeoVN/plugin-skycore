package kdvn.sky2.rpg.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.ecobag.EcoBag;
import mk.plugin.skycore.expbottle.ExpBottle;
import mk.plugin.skycore.item.healpotion.HealPotion;
import mk.plugin.skycore.loa.SpeakerItem;
import mk.plugin.skycore.player.PlayerUtils;

public class UseListener implements Listener {
	
	@EventHandler
	public void onUseLoa(PlayerInteractEvent e) {
		ItemStack item = e.getItem();
		if (item == null || item.getType() == Material.AIR) return;
		if (!SpeakerItem.isThatItem(item)) return;
		
		// Use
		e.setCancelled(true);
		Player player = e.getPlayer();
		player.setMetadata("skycore.worldspeaker", new FixedMetadataValue(MainSkyCore.get(), ""));
		item.setAmount(item.getAmount() - 1);
		player.updateInventory();
		player.sendMessage("§aHãy nhập nội dung chat!");
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		if (!player.hasMetadata("skycore.worldspeaker")) return;
		e.setCancelled(true);
		player.removeMetadata("skycore.worldspeaker", MainSkyCore.get());
		
		String mess = e.getMessage();
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSkyCore.get(), () -> {
			SpeakerItem.send(player, mess);
		});

	}
	
	@EventHandler
	public void onUseThuoc(PlayerInteractEvent e) {
		ItemStack item = e.getItem();
		if (item == null || item.getType() == Material.AIR) return;
		HealPotion thuoc = HealPotion.fromItem(item);
		if (thuoc == null) return;
		
		// Use
		e.setCancelled(true);
		Player player = e.getPlayer();
		int health = thuoc.getHealth();
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), () -> {
			// Add health
			Utils.addHealth(player, health);
			player.sendTitle("", "§a+" + health + " HP", 0, 10, 0);
			Utils.sendSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.5f, 0.5f);
			
			// Removeitem
			if (item.getAmount() > 1) item.setAmount(item.getAmount() - 1);
			else player.getInventory().setItemInMainHand(null);
		}, 0);

	}
	
	@EventHandler
	public void onUseEcoBag(PlayerInteractEvent e) {
		ItemStack is = e.getItem();
		if (is == null || is.getType() == Material.AIR) return;
		EcoBag eb = EcoBag.parse(is);
		if (eb == null) return;
		
		e.setCancelled(true);
		Player player = e.getPlayer();
		int value = eb.rateValue();
		eb.getEcoType().give(player, value);
		player.sendActionBar(eb.getEcoType().getColor() + "§l+" + value + " " + eb.getEcoType().getName());
		player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
		
		is.setAmount(is.getAmount() - 1);
		player.updateInventory();
	}
	
	@EventHandler
	public void onUseExpBottle(PlayerInteractEvent e) {
		ItemStack is = e.getItem();
		if (is == null || is.getType() == Material.AIR) return;
		ExpBottle eb = ExpBottle.parse(is);
		if (eb == null) return;
		
		e.setCancelled(true);
		Player player = e.getPlayer();
		int value = eb.rateValue();
		PlayerUtils.addExp(player, value);
		player.sendActionBar("§a§l+" + value + " Exp");
		player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
		
		is.setAmount(is.getAmount() - 1);
		player.updateInventory();
	}
	
	@EventHandler
	public void onUseExpBottle(ProjectileLaunchEvent e) {
		if (e.getEntity() instanceof ThrownExpBottle) e.setCancelled(true);
	}
	

}
