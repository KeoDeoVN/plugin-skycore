package kdvn.sky2.rpg.core.task;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.skycore.buff.ExpBuff;
import mk.plugin.skycore.buff.StatBuff;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.player.SkyPlayer;

public class SRPGBuffCheckTask extends BukkitRunnable  {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			SkyPlayer rpgP = PlayerUtils.getData(player);
			if (rpgP == null) return;
			List<StatBuff> statBuffs = new ArrayList<StatBuff> (rpgP.getStatBuffs());
			statBuffs.forEach(buff -> {
				if (!buff.isStillEffective()) {
					rpgP.removeStatBuff(buff);
					player.sendMessage("§aMột buff chỉ số của bạn đã hết hạn!");
				}
			});
			List<ExpBuff> expBuffs = new ArrayList<ExpBuff> (rpgP.getExpBuffs());
			expBuffs.forEach(buff -> {
				if (!buff.isStillEffective()) {
					rpgP.removeExpBuff(buff);
					player.sendMessage("§aMột kinh nghiệm chỉ số của bạn đã hết hạn!");
				}
			});
		});
	}

}
