package kdvn.sky2.rpg.core.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.stat.Stat;

public class SRPGHealTask extends BukkitRunnable {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			if (player.isDead()) return;
			double value = PlayerUtils.getStatValue(player, Stat.HOI_PHUC);
			Utils.addHealth(player, value);
		});
	}

}
