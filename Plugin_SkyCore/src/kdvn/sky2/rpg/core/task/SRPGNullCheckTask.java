package kdvn.sky2.rpg.core.task;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.skycore.player.PlayerUtils;

public class SRPGNullCheckTask extends BukkitRunnable {

	@Override
	public void run() {
		List<Player> players = new ArrayList<Player> (Bukkit.getOnlinePlayers());
		players.forEach(player -> {
			if (PlayerUtils.getData(player) == null) {
				player.kickPlayer("§cXin lỗi bạn vì sự bất tiện này, hãy vào lại game!");
			}
		});
	}

}
