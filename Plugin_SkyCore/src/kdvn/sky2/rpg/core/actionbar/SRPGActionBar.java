package kdvn.sky2.rpg.core.actionbar;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class SRPGActionBar {
	
	@SuppressWarnings("deprecation")
	public static void send(Player player, String content) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(content));
	}
	
//	public static void damageSend(Player player, LivingEntity entity, double health, double maxHealth, double damage) {
//		String name = entity.getName();
//		String s = "§0§l" + SRPGUtils.boldAll(name) + " §f§l[§c§l" + SRPGUtils.round(health) + "§7§l/§2§l" + maxHealth + "§f§l] §7§l(§4§l-" + SRPGUtils.round(damage) + "§7§l)";
//		if (entity.isDead()) {
//			s = "§4§lHạ gục";
//		}
//		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(s));
//	}
	
}
