package kdvn.sky2.rpg.core.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationData {
	
	private double x;
	private double y;
	private double z;
	private String world;
	
	public LocationData(double x, double y, double z, String world) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.world = world;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public String getWorld() {
		return this.world;
	}
	
	public String toString() {
		return this.x + ";" + this.y + ";" + this.z + ";" + this.world;
	}
	
	public boolean isSimilar(LocationData ld) {
		return this.x == ld.getX() && this.getY() == ld.getY() && this.getZ() == ld.getZ() && this.world.equalsIgnoreCase(ld.getWorld());
	}
	
	public Location toLocation() {
		return new Location(Bukkit.getWorld(this.getWorld()), this.getX(), this.getY(), this.getZ());
	}
	
	public static LocationData parse(String s) {
		return new LocationData(Double.valueOf(s.split(";")[0]), Double.valueOf(s.split(";")[1]), Double.valueOf(s.split(";")[2]), s.split(";")[3]);
	}
	
}
