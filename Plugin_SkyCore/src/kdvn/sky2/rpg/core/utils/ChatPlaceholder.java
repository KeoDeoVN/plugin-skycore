package kdvn.sky2.rpg.core.utils;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class ChatPlaceholder {
	
	public static Map<Player, ChatPlaceholder> cps = new HashMap<Player, ChatPlaceholder> ();
	
	public String chat;
	public String cmd;
	
	private final String placeholder = "<s>";
	
	public ChatPlaceholder(String chat, String cmd) {
		this.chat = chat;
		this.cmd = cmd;
	}
	
	public ChatPlaceholder(Player player, String chat, String cmd) {
		this.chat = chat;
		this.cmd = cmd;
		player.sendMessage(chat);
		cps.put(player, this);
	}
	
	public void runCmd(String s, Plugin plugin) {
		Bukkit.getScheduler().runTask(plugin, () -> {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace(placeholder, s));
		});
	}
	
}
