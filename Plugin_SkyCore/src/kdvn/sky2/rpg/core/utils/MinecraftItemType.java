package kdvn.sky2.rpg.core.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public enum MinecraftItemType {
	
	BOW("Cung", Arrays.asList(new Material[] {Material.BOW})),
	SWORD("Kiếm", Arrays.asList(new Material[] {Material.WOOD_SWORD, Material.STONE_SWORD, Material.IRON_SWORD, Material.GOLD_SWORD, Material.DIAMOND_SWORD})),
	AXE("Rìu", Arrays.asList(new Material[] {Material.WOOD_AXE, Material.IRON_AXE, Material.GOLD_AXE, Material.DIAMOND_AXE})),
	SHIELD("Khiên", Arrays.asList(new Material[] {Material.SHIELD})),
	ARMOR("Giáp", Arrays.asList(new Material[] {Material.DIAMOND_HELMET, Material.GOLD_HELMET, Material.IRON_HELMET, Material.CHAINMAIL_HELMET, Material.LEATHER_HELMET, Material.DIAMOND_CHESTPLATE, Material.GOLD_CHESTPLATE, Material.IRON_CHESTPLATE, Material.CHAINMAIL_CHESTPLATE, Material.LEATHER_CHESTPLATE, Material.DIAMOND_LEGGINGS, Material.GOLD_LEGGINGS, Material.IRON_LEGGINGS,  Material.CHAINMAIL_LEGGINGS, Material.LEATHER_LEGGINGS, Material.DIAMOND_BOOTS, Material.GOLD_BOOTS, Material.IRON_BOOTS, Material.CHAINMAIL_BOOTS, Material.LEATHER_BOOTS}));
	
	public List<Material> listMaterial = new ArrayList<Material> ();
	public String name;
	
	private MinecraftItemType(String name, List<Material> listMaterial) {
		this.listMaterial = listMaterial;
		this.name = name;
	}
	
	public static MinecraftItemType getType(Material material) {
		for (MinecraftItemType type : values()) {
			if (type.listMaterial.contains(material)) return type;
		}
		return null;
	}
	
	public static boolean isHoldingType(MinecraftItemType type, Player player) {
		ItemStack item = player.getInventory().getItemInMainHand();
		if (item == null || item.getType () == Material.AIR) return false;
		return getType(item.getType()) == type;
	}
	
}
