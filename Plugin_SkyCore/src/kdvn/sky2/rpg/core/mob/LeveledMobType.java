package kdvn.sky2.rpg.core.mob;

public enum LeveledMobType {
	
	THUONG("Thường", 3) {
		@Override
		public int getHealth(int level) {
			return level * 6;
		}

		@Override
		public int getDamage(int level) {
			return level;
		}
	},
	AC("Ác", 5) {
		@Override
		public int getHealth(int level) {
			return level * 12;
		}

		@Override
		public int getDamage(int level) {
			return level * 2;
		}
	},
	TRUM("Trùm", 8) {
		@Override
		public int getHealth(int level) {
			return level * 50;
		}

		@Override
		public int getDamage(int level) {
			return level * 10;
		}
	},
	CHUA_TE("Chúa tể", 50) {
		@Override
		public int getHealth(int level) {
			return level * 300;
		}

		@Override
		public int getDamage(int level) {
			return level * 40;
		}
	};
	
	public abstract int getHealth(int level);
	public abstract int getDamage(int level);
	
	private String name;
	private int expBonus;
	
	private LeveledMobType(String name, int expBonus) {
		this.name = name;
		this.expBonus = expBonus;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getExpBonus() {
		return this.expBonus;
	}
	
	
}
