package kdvn.sky2.rpg.core.mob;

public class LeveledMob {
	
	private int level;
	private LeveledMobType mobType;
	
	public LeveledMob(int level, LeveledMobType mobType) {
		this.level = level;
		this.mobType = mobType;
	}

	public int getLevel() {
		return level;
	}

	public LeveledMobType getMobType() {
		return mobType;
	}

	public String toString() {
		return this.level + ";" + this.mobType.name();
	}
	
	public static LeveledMob parse(String s) {
		int level = Integer.parseInt(s.split(";")[0]);
		LeveledMobType t = LeveledMobType.valueOf(s.split(";")[1]);
		return new LeveledMob(level, t);
	}
	
	
	
}
