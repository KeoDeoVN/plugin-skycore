package kdvn.sky2.rpg.core.main;

import java.util.List;

import org.black_ixx.playerpoints.PlayerPoints;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.google.common.collect.Lists;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import kdvn.sky2.rpg.core.command.admin.SRPGAdminCommand;
import kdvn.sky2.rpg.core.command.player.JewelryCommand;
import kdvn.sky2.rpg.core.command.player.SRPGClassCommand;
import kdvn.sky2.rpg.core.command.player.SRPGInfoCommand;
import kdvn.sky2.rpg.core.command.player.SRPGPotentialCommand;
import kdvn.sky2.rpg.core.command.player.SRPGSeeCommand;
import kdvn.sky2.rpg.core.command.player.SRPGTiemRenCommand;
import kdvn.sky2.rpg.core.listener.BlockListener;
import kdvn.sky2.rpg.core.listener.ChatListener;
import kdvn.sky2.rpg.core.listener.ComboListener;
import kdvn.sky2.rpg.core.listener.DamageListener;
import kdvn.sky2.rpg.core.listener.DataListener;
import kdvn.sky2.rpg.core.listener.DeathListener;
import kdvn.sky2.rpg.core.listener.EntityListener;
import kdvn.sky2.rpg.core.listener.ExpListener;
import kdvn.sky2.rpg.core.listener.GUIListener;
import kdvn.sky2.rpg.core.listener.ItemListener;
import kdvn.sky2.rpg.core.listener.MoveListener;
import kdvn.sky2.rpg.core.listener.PassiveListener;
import kdvn.sky2.rpg.core.listener.UseListener;
import kdvn.sky2.rpg.core.listener.WeaponListener;
import kdvn.sky2.rpg.core.listener.XacMinhListener;
import kdvn.sky2.rpg.core.party.PartyConsoleCommands;
import kdvn.sky2.rpg.core.party.PartyEventListener;
import kdvn.sky2.rpg.core.party.PartyPlayerCommands;
import kdvn.sky2.rpg.core.permbuff.SPermBuff;
import kdvn.sky2.rpg.core.storage.SRPGMySQL;
import kdvn.sky2.rpg.core.task.SRPGBuffCheckTask;
import kdvn.sky2.rpg.core.task.SRPGHealTask;
import me.realized.tokenmanager.api.TokenManager;
import mk.plugin.skycore.crate.Crates;
import mk.plugin.skycore.customcraft.CraftRecipes;
import mk.plugin.skycore.hook.dungeon3.SCDungeon3;
import mk.plugin.skycore.itemtexture.ItemTexture;
import mk.plugin.skycore.placeholder.SkyCorePlaceholder;
import mk.plugin.skycore.player.PlayerStorage;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.repair.AutoRepairTask;
import mk.plugin.skycore.task.JewelryTask;
import mk.plugin.skycore.task.OreTask;
import mk.plugin.skycore.task.TargetTask;
import mk.plugin.skycore.yaml.YamlFile;
import net.milkbowl.vault.economy.Economy;

public class MainSkyCore extends MainSky2RPGCore {
	
	private static MainSkyCore main;	
	private static SRPGMySQL db;
	private static Economy econ = null;
	private static PlayerPoints playerPoints = null;
	public static TokenManager tokenM = null;
	
	public static MainSkyCore getMain() {return main;}
	public static MainSkyCore get() {return getMain();}
	public static SRPGMySQL getDatabase() {return db;}
    public static Economy getEcononomy() { return econ; }   
    public static PlayerPoints getPP() { return playerPoints; }  
    public static TokenManager getTokenManager() { return tokenM; }	
    
    public static List<String> SPECIAL_MINE_WORLDS = null;
    public static List<ItemTexture> CUSTOM_TEXTURES = null;
    public static List<String> WORLD_PVPS;
    
	private FileConfiguration config;
	
	@Override
	public void onEnable() {
		main = this;
		
		// Config
		saveDefaultConfig();
		reloadConfig();
		
		// Hook
		hookOtherPlugins();
		
		// ...
		registerCommands();
		registerEvents();
		registerPlaceholders();
		runTasks();
		
		// ...
		registerChannels();
		
		// Online players
		loadDataOnlinePlayers();
	}
	
	@Override
	public void onDisable() {
		// Save data
		Bukkit.getOnlinePlayers().forEach(p -> {
			PlayerStorage.save(p.getName(), PlayerUtils.getData(p));
		});
		
		// Reset ore
		OreTask.resetAll();
	}
	
	public void reloadConfig() {
		YamlFile.reloadAll(this);
		this.config = YamlFile.CONFIG.get();
		
		// Config
		CraftRecipes.reload(YamlFile.CRAFT.get());
		SPermBuff.load(config);
		SPECIAL_MINE_WORLDS = config.getStringList("special-mine.worlds");
		CUSTOM_TEXTURES = Lists.newArrayList();
		WORLD_PVPS = config.getStringList("world-pvp");
		config.getStringList("custom-textures").forEach(s -> {
			Material m = Material.valueOf(s.split(";")[0]);
			if (s.split(";")[1].contains("-")) {
				short start = Short.valueOf(s.split(";")[1].split("-")[0]);
				short end = Short.valueOf(s.split(";")[1].split("-")[1]);
				for (int i = start ; i <= end ; i++) CUSTOM_TEXTURES.add(new ItemTexture(m, i));
			} else CUSTOM_TEXTURES.add(new ItemTexture(m, Short.valueOf(s.split(";")[1])));
		});
		
		// Xac minh
		XacMinhListener.XACMINH_PERMISSIONS = config.getStringList("xac-minh.permissions");
		Crates.reload(YamlFile.CONFIG.get());
	}
	
	private void registerChannels() {
		Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "fs:minestrike");
	}
	
	private void registerEvents() {
		Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
		Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
		Bukkit.getPluginManager().registerEvents(new DataListener(), this);
		Bukkit.getPluginManager().registerEvents(new GUIListener(), this);
		Bukkit.getPluginManager().registerEvents(new ComboListener(), this);
		Bukkit.getPluginManager().registerEvents(new ExpListener(), this);
		Bukkit.getPluginManager().registerEvents(new UseListener(), this);
		Bukkit.getPluginManager().registerEvents(new PartyEventListener(), this);
		Bukkit.getPluginManager().registerEvents(new DeathListener(), this);
		Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
		Bukkit.getPluginManager().registerEvents(new EntityListener(), this);
		Bukkit.getPluginManager().registerEvents(new ItemListener(), this);
		Bukkit.getPluginManager().registerEvents(new WeaponListener(), this);
		Bukkit.getPluginManager().registerEvents(new XacMinhListener(), this);
		Bukkit.getPluginManager().registerEvents(new PassiveListener(), this);
		Bukkit.getPluginManager().registerEvents(new MoveListener(), this);
	}
	
	private void registerPlaceholders() {
		System.out.println("[SkyCore] Registered placeholders");
		new SkyCorePlaceholder().register();
	}
	
	private void runTasks() {
		new SRPGHealTask().runTaskTimerAsynchronously(this, 20, 20);
		new SRPGBuffCheckTask().runTaskTimerAsynchronously(this, 20, 20);
		new JewelryTask().runTaskTimer(this, 0, 20);
		new TargetTask().runTaskTimer(this, 0, 2);
		new OreTask().runTaskTimer(this, 0, 10);
		new AutoRepairTask().runTaskTimer(this, 0, 10);
	}
	
	private void registerCommands() {
		this.getCommand("skyrpg").setExecutor(new SRPGAdminCommand());
		this.getCommand("see").setExecutor(new SRPGSeeCommand());
		this.getCommand("potential").setExecutor(new SRPGPotentialCommand());
		this.getCommand("party").setExecutor(new PartyPlayerCommands());
		this.getCommand("partyadmin").setExecutor(new PartyConsoleCommands());
		this.getCommand("class").setExecutor(new SRPGClassCommand());
		this.getCommand("skill").setExecutor(new SRPGClassCommand());
		this.getCommand("info").setExecutor(new SRPGInfoCommand());
		this.getCommand("player").setExecutor(new SRPGInfoCommand());
		this.getCommand("tiemren").setExecutor(new SRPGTiemRenCommand());
		this.getCommand("jewelry").setExecutor(new JewelryCommand());
		this.getCommand("khochetac").setExecutor(new SRPGInfoCommand());
		this.getCommand("khovukhi").setExecutor(new SRPGInfoCommand());
		this.getCommand("chetac").setExecutor(new SRPGInfoCommand());
	}
	
	private void loadDataOnlinePlayers() {
		Bukkit.getOnlinePlayers().forEach(p -> {
			PlayerUtils.playerJoin(p);
		});
	}
	
	private void hookOtherPlugins() {
		hookVault();
		hookPlayerPoint();
		tokenM = (TokenManager) Bukkit.getPluginManager().getPlugin("TokenManager");
		SCDungeon3.reload(config);
	}
	
	private boolean hookVault() {
	    if (getServer().getPluginManager().getPlugin("Vault") == null) {
	            return false;
	     }
	    RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
	    if (rsp == null) {
	    	return false;
	    }
	    econ = rsp.getProvider();
	    return econ != null;
	}
	
    private boolean hookPlayerPoint() {
    	final Plugin pl = this.getServer().getPluginManager().getPlugin("PlayerPoints");
    	playerPoints = PlayerPoints.class.cast(pl);
    	return playerPoints != null;
    	
    }
    
    public static WorldGuardPlugin getWorldGuardPlugin() {
		Plugin pl = Bukkit.getPluginManager().getPlugin("WorldGuard");
		if (pl == null) {
			return null;
		}
		return (WorldGuardPlugin) pl;
	}
}
