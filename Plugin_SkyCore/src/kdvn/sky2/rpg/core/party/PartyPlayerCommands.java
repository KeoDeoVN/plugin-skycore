package kdvn.sky2.rpg.core.party;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PartyPlayerCommands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		if (args.length == 0) {
			player.openInventory(PartyGUI.getInv(player));
			return false;
		}
		if (args[0].equalsIgnoreCase("dongy")) {
			PartyManager.get().acceptRequestJoin(player);
			return false;
		}
		else if (args[0].equalsIgnoreCase("tuchoi")) {
			PartyManager.get().refuseRequestJoin(player);
			return false;
		}
		return false;
	}

}
