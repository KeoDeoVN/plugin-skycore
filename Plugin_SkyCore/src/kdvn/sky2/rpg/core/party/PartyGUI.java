package kdvn.sky2.rpg.core.party;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.ChatPlaceholder;
import kdvn.sky2.rpg.core.utils.Utils;




public class PartyGUI {
	
	public static final String TITLE = "§c§lPARTY MENU";
	
	public static Inventory getInv(Player player) {
		PartyGUIContentObject gg = getCaiLonMeMay(player);
		Inventory inv = Bukkit.createInventory(null, gg.size, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			for (int i = 0 ; i < gg.size ; i++) {
				if (gg.items.containsKey(i)) {
					ItemStack item = gg.items.get(i);
					ItemMeta meta = item.getItemMeta();
					meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_UNBREAKABLE);
					item.setItemMeta(meta);
					inv.setItem(i, gg.items.get(i));
				} else inv.setItem(i, Utils.getBlackSlot());
			}
		});
		
		return inv;
	}
	
	private static PartyGUIContentObject getCaiLonMeMay(Player player) {
		boolean isMaster = false;
		boolean isMember = false;
		boolean isGuest = false;
		
		
		// Check rank
		if (PartyManager.get().getPartyFromPlayer(player) != null) {
			isMember = true;
		} else isGuest = true;
		if (PartyManager.get().getPartyFromMaster(player) != null) {
			isMember = false;
			isMaster = true;
		}
		
		Map<Integer, ItemStack> items = new HashMap<Integer, ItemStack> ();
		PartyGUIContentObject cailon = new PartyGUIContentObject(items, 9);
		if (isGuest) {
			items.put(3, Utils.getTexture(Material.PAPER, 0, "§aGửi đơn gia nhập"));
			items.put(5, Utils.getTexture(Material.BOOK, 0, "§aTạo Party"));
			cailon.chat.put(3, new ChatPlaceholder("§aNhập tên chủ Party", "partyadmin request {player} <s>".replace("{player}", player.getName())));
			cailon.chat.put(5, new ChatPlaceholder("", "partyadmin create {player}".replace("{player}", player.getName())));
		}
		else if (isMember) {
			items.put(3, Utils.getTexture(Material.PAPER, 0, "§aThông tin Party"));
			items.put(5, Utils.getTexture(Material.REDSTONE_BLOCK, 0, "§cCút khỏi Party"));
			cailon.chat.put(3, new ChatPlaceholder("", "partyadmin info {player}".replace("{player}", player.getName())));
			cailon.chat.put(5, new ChatPlaceholder("", "partyadmin kick {player}".replace("{player}", player.getName())));
		}
		else if (isMaster) {
			items.put(2, Utils.getTexture(Material.PAPER, 0, "§aThông tin Party"));
			items.put(4, Utils.getTexture(Material.REDSTONE_BLOCK, 0, "§cCho thằng nào đó cút khỏi Party"));
			items.put(6, Utils.getTexture(Material.BARRIER, 0, "§cXóa Party"));
			cailon.chat.put(2, new ChatPlaceholder("", "partyadmin info {player}".replace("{player}", player.getName())));
			cailon.chat.put(4, new ChatPlaceholder("Nhập tên thằng đó vào đi", "partyadmin kick <s>".replace("{player}", player.getName())));
			cailon.chat.put(6, new ChatPlaceholder("", "partyadmin kick {player}".replace("{player}", player.getName())));
		}
		
		return cailon;
	}
	
	public static void eventListener(InventoryClickEvent e) {
		if (e.getClickedInventory() != e.getWhoClicked().getOpenInventory().getTopInventory()) {
			return;
		}
		if (e.getClickedInventory() == null) {
			return;
		}
		if (!e.getView().getTitle().equals(TITLE)) {
			return;
		}
		
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		if (ChatPlaceholder.cps.containsKey(player)) {
			player.sendMessage("§cTrả lời câu hỏi trước!");
			return;
		}
		int slot = e.getSlot();
	//	if (rank == null) return;
		PartyGUIContentObject gg = getCaiLonMeMay(player);
		if (gg.chat.containsKey(slot)) {
			ChatPlaceholder.cps.put(player, gg.chat.get(slot));
			player.closeInventory();
			String chat = gg.chat.get(slot).chat;
			if (chat.equalsIgnoreCase("")) {
				ChatPlaceholder cp = ChatPlaceholder.cps.get(player);
				cp.runCmd("", MainSky2RPGCore.getMain());
				ChatPlaceholder.cps.remove(player);
				return;
			}
			player.sendMessage(chat);
		}
	}
	
}

class PartyGUIContentObject {
	
	public Map<Integer, ItemStack> items = new HashMap<Integer, ItemStack> ();
	public int size;
	public Map<Integer, ChatPlaceholder> chat = new HashMap<Integer, ChatPlaceholder> ();
	
	public PartyGUIContentObject(Map<Integer, ItemStack> items, int size) {
		this.items = items;
		this.size = size;
	}
	
}