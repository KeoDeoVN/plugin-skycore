package kdvn.sky2.rpg.core.price;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSkyCore;

public class PointAPI {
	
	@SuppressWarnings("deprecation")
	public static int getPoint(Player player) {
		int points = MainSkyCore.getPP().getAPI().look(player.getName());
		return points;
	}
	
	public static boolean pointCost(Player player, int points) {
		if (points > getPoint(player)) {
			return false;
		} else {
			MainSkyCore.getPP().getAPI().take(player.getUniqueId(), points);
			return true;
		}
	}
	
	public static void givePoint(Player player, int point) {
		MainSkyCore.getPP().getAPI().give(player.getUniqueId(), point);
	}
	
	
}
