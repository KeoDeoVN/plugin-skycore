package kdvn.sky2.rpg.core.price;

import java.util.OptionalLong;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSkyCore;

public class TokenAPI {
	
	public static boolean tokenCost(Player player, int value) {
		OptionalLong l = MainSkyCore.getTokenManager().getTokens(player);
		if (l.getAsLong() < value) return false;
		MainSkyCore.tokenM.setTokens(player, MainSkyCore.getTokenManager().getTokens(player).getAsLong() - value);
		return true;
	}
	
	public static void give(Player player, int value) {
		MainSkyCore.getTokenManager().setTokens(player, MainSkyCore.getTokenManager().getTokens(player).getAsLong() + value);
	}
	
}
