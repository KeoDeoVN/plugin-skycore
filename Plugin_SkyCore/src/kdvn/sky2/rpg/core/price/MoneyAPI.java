package kdvn.sky2.rpg.core.price;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSkyCore;
import net.milkbowl.vault.economy.Economy;

public class MoneyAPI {
	
	public static double getMoney(Player player) {
		Economy eco = MainSkyCore.getEcononomy();
		
		return eco.getBalance(player);
	}

	public static boolean moneyCost(Player player, double money) {
		Economy eco = MainSkyCore.getEcononomy();
		double moneyOfPlayer = eco.getBalance(player);
		if (moneyOfPlayer < money) {
			return false;
		}
		eco.withdrawPlayer(player, money);
		return true;
		
	}
	
	public static void giveMoney(Player player, double money) {
		Economy eco = MainSkyCore.getEcononomy();
		eco.depositPlayer(player, money);
	}
	
}
