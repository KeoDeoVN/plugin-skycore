package kdvn.sky2.rpg.core.permbuff;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Maps;

import mk.plugin.skycore.stat.Stat;

public class SPermBuff {
	
	public static Map<String, SPermBuff> permBuffs = new HashMap<String, SPermBuff> ();	
	
	public static void load(FileConfiguration config) {
		permBuffs.clear();
		config.getConfigurationSection("permbuff").getKeys(false).forEach(id -> {
			String perm = config.getString("permbuff." + id + ".permission");
			Map<Stat, Integer> buffs = Maps.newHashMap();
			config.getStringList("permbuff." + id + ".buff").forEach(s -> {
				Stat stat = Stat.valueOf(s.split(":")[0].toUpperCase());
				int value = Integer.parseInt(s.split(":")[1]);
				buffs.put(stat, value);
			});
			SPermBuff buff = new SPermBuff(perm, buffs);
			permBuffs.put(id, buff);
		});
	}
	
	private String permission;
	private Map<Stat, Integer> buffs = new HashMap<Stat, Integer> ();
	
	public SPermBuff(String permission, Map<Stat, Integer> buffs) {
		this.permission = permission;
		this.buffs = buffs;
	}
	
	public String getPermission() {
		return this.permission;
	}
	
	public Map<Stat, Integer> getBuffs() {
		return this.buffs;
	}
	
}
