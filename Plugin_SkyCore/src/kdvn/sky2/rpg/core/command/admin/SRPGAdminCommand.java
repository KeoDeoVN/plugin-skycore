package kdvn.sky2.rpg.core.command.admin;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.main.MainSkyCore;
import kdvn.sky2.rpg.core.mob.LeveledMob;
import kdvn.sky2.rpg.core.mob.LeveledMobType;
import kdvn.sky2.rpg.core.mob.LeveledMobs;
import kdvn.sky2.rpg.core.utils.ChatPlaceholder;
import kdvn.sky2.rpg.core.utils.Utils;
import mk.plugin.sky2rpgcore.element.Element;
import mk.plugin.sky2rpgcore.itemgui.SItemGUI;
import mk.plugin.sky2rpgcore.tier.Tier;
import mk.plugin.skycore.buff.ExpBuff;
import mk.plugin.skycore.buff.StatBuff;
import mk.plugin.skycore.crate.Crate;
import mk.plugin.skycore.crate.Crates;
import mk.plugin.skycore.customcraft.CraftMenuGUI;
import mk.plugin.skycore.customcraft.CraftRecipeGUI;
import mk.plugin.skycore.customcraft.CraftStorageGUI;
import mk.plugin.skycore.ecobag.EcoBag;
import mk.plugin.skycore.expbottle.ExpBottle;
import mk.plugin.skycore.grade.Grade;
import mk.plugin.skycore.grade.GradeStone;
import mk.plugin.skycore.gui.enhance.GUIEnhance;
import mk.plugin.skycore.gui.gem.GUIGemAdd;
import mk.plugin.skycore.gui.gem.GUIGemDispart;
import mk.plugin.skycore.gui.gem.GUIGemDrill;
import mk.plugin.skycore.gui.grade.GUIGrade;
import mk.plugin.skycore.gui.itemshow.GUIItemShow;
import mk.plugin.skycore.gui.player.GUIPlayerElement;
import mk.plugin.skycore.gui.player.GUIPlayerInfo;
import mk.plugin.skycore.gui.player.GUIPlayerSee;
import mk.plugin.skycore.item.Item;
import mk.plugin.skycore.item.ItemAPI;
import mk.plugin.skycore.item.ItemData;
import mk.plugin.skycore.item.ItemType;
import mk.plugin.skycore.item.enhancegem.EnhanceGem;
import mk.plugin.skycore.item.gem.Gem;
import mk.plugin.skycore.item.gem.GemUtils;
import mk.plugin.skycore.item.healpotion.HealPotion;
import mk.plugin.skycore.item.keepgem.KeepGem;
import mk.plugin.skycore.item.luckyamulet.LuckyAmulet;
import mk.plugin.skycore.item.randomgem.RandomGem;
import mk.plugin.skycore.itemcategory.Armor;
import mk.plugin.skycore.itemgenerate.ItemGenerates;
import mk.plugin.skycore.itemtexture.ItemTexture;
import mk.plugin.skycore.loa.SpeakerItem;
import mk.plugin.skycore.player.PlayerUtils;
import mk.plugin.skycore.player.SkyPlayer;
import mk.plugin.skycore.repair.RepairStone;
import mk.plugin.skycore.stat.Stat;

public class SRPGAdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		if (!sender.hasPermission("skyrpg.*")) return false;
		
		try {
			if (args[0].equalsIgnoreCase("reload")) {
				sender.sendMessage("§aReloaded");
				MainSky2RPGCore.getMain().reloadConfig();
			}
			
			else if (args[0].equalsIgnoreCase("setlevel")) {
				Player player = Bukkit.getPlayer(args[1]);
				int level = Integer.parseInt(args[2]);
				player.setLevel(level);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("buffstat")) {
				Player player = Bukkit.getPlayer(args[1]);
				StatBuff buff = new StatBuff(Stat.valueOf(args[2]), Integer.parseInt(args[3]), Boolean.parseBoolean(args[4]), System.currentTimeMillis(), Integer.parseInt(args[5]) * 1000);
				PlayerUtils.buffStat(player, buff);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("buffexp")) {
				Player player = Bukkit.getPlayer(args[1]);
				ExpBuff buff = new ExpBuff(Integer.parseInt(args[2]), System.currentTimeMillis(), Integer.parseInt(args[3]) * 1000, "");
				PlayerUtils.buffExp(player, buff);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("sendmess")) {
				Player target = Bukkit.getPlayer(args[1]);
				String mess = "";
				for (int i = 2 ; i < args.length ; i++) {
					mess += args[i] + " ";
				}
				mess = mess.replace("&", "§");
				target.sendMessage(mess);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("renameitem")) {
				Player target = Bukkit.getPlayer(args[1]);
				if (args.length == 3) {
					String name = args[2].replace("_", " ");
					ItemStack item = target.getInventory().getItemInMainHand();
					Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), ()  -> {
						Item rpgI = ItemAPI.get(item);
						if (rpgI == null) return;
						rpgI.getData().setName(name.replace("&", "§") + "§8§l™");
						if (rpgI != null) {
							ItemStack i = ItemAPI.set(target, item, rpgI);
							target.getInventory().setItemInMainHand(i);
						}
					});
				}
				else {
					new ChatPlaceholder(target, "§aNhập tên bạn muốn đổi (dấu cách là \"_\", chỉ đổi được item có chỉ số)", "skyrpg renameitem "  + target.getName() + " <s>");
				}
				sender.sendMessage("§aOk, done");
			} 
			
			else if (args[0].equals("levelup")) {
				Player target = Bukkit.getPlayer(args[1]);
				if (args.length > 2) {
					int lv = Integer.parseInt(args[2]);
					if (target.getLevel() < lv) {
						target.setLevel(lv);
					}
				}
				else target.setLevel(target.getLevel() + 1);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equals("setlevel")) {
				Player target = Bukkit.getPlayer(args[1]);
				int lv = Integer.parseInt(args[2]);
				target.setLevel(lv);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equals("addexp")) {
				Player target = Bukkit.getPlayer(args[1]);
				PlayerUtils.addExp(target, Integer.parseInt(args[2]));
				target.sendMessage("§aNhận §f" + Integer.parseInt(args[2]) + " exp");
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("infogui")) {
				Player player = Bukkit.getPlayer(args[1]);
				GUIPlayerInfo.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("potentialgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				GUIPlayerElement.openGUI(player);
			}
			
//			else if (args[0].equalsIgnoreCase("classgui")) {
//				Player player = Bukkit.getPlayer(args[1]);
//				if (args.length == 3) {
//					SRPGClass cls = SRPGClass.valueOf(args[2].toUpperCase());
//					SRPGGUIClass.openGUI(player, cls);
//				}
//				else {
//					SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
//					if (rpgP.hasClass()) {
//						SRPGGUIClass.openGUI(player, rpgP.getSClass());
//					} else {
//						player.sendMessage("§cBạn chưa có class");
//					}
//				}
//			}
			
			else if (args[0].equalsIgnoreCase("seegui")) {
				Player target = Bukkit.getPlayer(args[1]);
				Player viewer = Bukkit.getPlayer(args[2]);
				GUIPlayerSee.openGUI(target, viewer);
			}
			
			else if (args[0].equalsIgnoreCase("resetelement")) {
				Player player = Bukkit.getPlayer(args[1]);
				Map<Element, Integer> potentials = new LinkedHashMap<Element, Integer>();
				SkyPlayer rpgPlayer = PlayerUtils.getData(player);
				rpgPlayer.setElements(potentials);
				player.sendMessage("§aĐã reset Hệ!");
			}
			
			else if (args[0].equalsIgnoreCase("cuonghoagui")) {
				Player player = Bukkit.getPlayer(args[1]);
				GUIEnhance.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("chuyenhoagui")) {
//				Player player = Bukkit.getPlayer(args[1]);

			}
			
			else if (args[0].equalsIgnoreCase("epdagui")) {
				Player player = Bukkit.getPlayer(args[1]);
				GUIGemAdd.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("duclogui")) {
				Player player = Bukkit.getPlayer(args[1]);
				GUIGemDrill.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("tachdagui")) {
				Player player = Bukkit.getPlayer(args[1]);
				GUIGemDispart.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("nangbacgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				GUIGrade.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("itemgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SItemGUI.openGUI(player);
			}
			
			
			else if (args[0].equalsIgnoreCase("craftgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				String rID = args[2];
				CraftRecipeGUI.open(player, rID);
			}
			
			else if (args[0].equalsIgnoreCase("craftstoragegui")) {
				Player player = Bukkit.getPlayer(args[1]);
				CraftStorageGUI.open(player);
			}
			
//			else if (args[0].equals("setclass")) {
//				SRPGPlayerUtils.changeClass(Bukkit.getPlayer(args[2]), SRPGClass.valueOf(args[1]));
//			}
			
			else if (args[0].equalsIgnoreCase("giverandomgem")) {
				Player player = Bukkit.getPlayer(args[1]);
				Tier rate = Tier.valueOf(args[2].toUpperCase());
				Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
					Stat stat = Stat.values()[Utils.randomInt(0, Stat.values().length - 1)];
					Gem gem = new Gem("§6Ngọc §c" + stat.getName(), stat, rate.getBonus(), rate);
					ItemStack item = GemUtils.setGem(new ItemStack(Material.IRON_NUGGET), gem);
					Utils.giveItem(player, item);
					player.sendMessage("§aNhận được ngọc §c" + stat.getName());
				});
			}
			
			else if (args[0].equalsIgnoreCase("fixall")) {
				Player player = Bukkit.getPlayer(args[1]);
				for (ItemStack is : player.getInventory().getContents()) {
					if (is != null && is.getType() != Material.AIR) {
						// Check 
						if (MainSkyCore.CUSTOM_TEXTURES.contains(new ItemTexture(is.getType(), is.getDurability()))) continue;
						
						List<String> start = Lists.newArrayList("WOOD", "STONE", "IRON", "GOLD", "DIAMOND");
						for (String s : start) {
							if (is.getType().name().startsWith(s)) {
								// Fix
								is.setDurability((short) 0);
							}
						}
					}
				}
				player.sendMessage("§aFix thành công");
			}
			
			else if (args[0].equalsIgnoreCase("craftmenu")) {
				Player player = Bukkit.getPlayer(args[1]);
				String id = args[2];
				CraftMenuGUI.open(id, player);
			}
			
			// Command In-game
			if (!(sender instanceof Player)) {
				return false;
			}
			Player player = (Player) sender;
			SkyPlayer rpgP = PlayerUtils.getData(player);
			
			if (args[0].equalsIgnoreCase("checkstat")) {
				for (Stat stat : Stat.values()) {
					player.sendMessage("§a" + stat.getName() + ": §f" + rpgP.getStat(player, stat));
				}
			}
			
			else if (args[0].equalsIgnoreCase("checkitem")) {
				Item rpgI = ItemAPI.get(player.getInventory().getItemInMainHand());
				ItemData data = rpgI.getData();
				player.sendMessage("§aSkyCore: §f" + data.toString());
			}
			
//			else if (args[0].equalsIgnoreCase("setitem")) {
//				String name = args[1].replace("_", " ").replace("&", "§");
//				int level = Integer.parseInt(args[2]);
//				int gemHole = Integer.parseInt(args[3]);
//				Map<Stat, Integer> stats = ItemUtils.mapStatFromString(args[4]);
//				Map<Enchant, Integer> enchants = new LinkedHashMap<Enchant, Integer> ();
//				Item rpgItem = new Item(name, level, gemHole, stats, null, enchants, null, Grade.I, 0);
//				ItemStack itemStack = player.getInventory().getItemInMainHand();
//				itemStack = ItemUtils.setItem(player, itemStack, rpgItem);
//				player.getInventory().setItemInMainHand(itemStack);
//				sender.sendMessage("§aOk, done");
//			}
			
			else if (args[0].equalsIgnoreCase("setgem")) {
				Stat stat = Stat.valueOf(args[1]);
				int value =  Integer.parseInt(args[2]);
				Tier rate = Tier.valueOf(args[3]);
				String name = args[4].replace("_", " ").replace("&", "§");
				
				Gem gem = new Gem(name, stat, value, rate);
				
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				itemStack = GemUtils.setGem(itemStack, gem);
				player.getInventory().setItemInMainHand(itemStack);
				
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("setitemlevel")) {
				int level = Integer.parseInt(args[1]);
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				Item i = ItemAPI.get(itemStack);
				i.getData().setLevel(level);
				itemStack = ItemAPI.set(player, itemStack, i);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
//			
//			else if (args[0].equalsIgnoreCase("setitemname")) {
//				String name = args[1];
//				ItemStack itemStack = player.getInventory().getItemInMainHand();
//				Item si = ItemUtils.fromItemStack(itemStack);
//				si.setName(name);
//				itemStack = ItemUtils.setItem(player, itemStack, si);
//				player.getInventory().setItemInMainHand(itemStack);
//				sender.sendMessage("§aOk, done");
//			}
//			
//			else if (args[0].equalsIgnoreCase("setitemdesc")) {
//				String desc = "";
//				for (int i = 1 ; i < args.length ; i++) desc += args[i] + " ";
//				desc = desc.substring(0, desc.length() - 1);
//				ItemStack itemStack = player.getInventory().getItemInMainHand();
//				Item si = ItemUtils.fromItemStack(itemStack);
//				si.setDesc(desc);
//				itemStack = ItemUtils.setItem(player, itemStack, si);
//				player.getInventory().setItemInMainHand(itemStack);
//				sender.sendMessage("§aOk, done");
//			}
//			
			else if (args[0].equalsIgnoreCase("setitemgrade")) {
				Grade grade = Grade.valueOf(args[1].toUpperCase());
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				Item si = ItemAPI.get(itemStack);
				ItemData id = si.getData();
				id.setGrade(grade);
				itemStack = ItemAPI.set(player, itemStack, si);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
//			
//			else if (args[0].equalsIgnoreCase("setitemstats")) {
//				Map<Stat, Integer> stats = ItemUtils.mapStatFromString(args[1]);
//				ItemStack itemStack = player.getInventory().getItemInMainHand();
//				Item si = ItemUtils.fromItemStack(itemStack);
//				si.setStats(stats);;
//				itemStack = ItemUtils.setItem(player, itemStack, si);
//				player.getInventory().setItemInMainHand(itemStack);
//				sender.sendMessage("§aOk, done");
//			}
//			
//			
//			else if (args[0].equalsIgnoreCase("setitemgemhole")) {
//				int hole = Integer.valueOf(args[1]);
//				ItemStack itemStack = player.getInventory().getItemInMainHand();
//				Item si = ItemUtils.fromItemStack(itemStack);
//				si.setGemHole(hole);;
//				itemStack = ItemUtils.setItem(player, itemStack, si);
//				player.getInventory().setItemInMainHand(itemStack);
//				sender.sendMessage("§aOk, done");
//			}
//			
			
			else if (args[0].equalsIgnoreCase("getcuonghoa")) {
				for (EnhanceGem dch : EnhanceGem.values()) {
					Utils.giveItem(player, dch.getItem());
				}
				for (LuckyAmulet smm : LuckyAmulet.values()) {
					Utils.giveItem(player, smm.getItem());
				}
			}

			
			else if (args[0].equalsIgnoreCase("spawnmob")) {
				int lv = Integer.parseInt(args[1]);
				EntityType type = EntityType.valueOf(args[2]);
				LivingEntity z = (LivingEntity) player.getWorld().spawnEntity(player.getLocation(), type);
				LeveledMobType mobType = LeveledMobType.valueOf(args[3]);
				LeveledMobs.setRPGMob(z, new LeveledMob(lv, mobType), z.getName() + " Lv." + lv);
			}
			
			else if (args[0].equalsIgnoreCase("getthuochoiphuc")) {
				HealPotion type = HealPotion.valueOf(args[1]);
				Utils.giveItem(player, type.createItem());
			}
			
			else if (args[0].equalsIgnoreCase("getitem")) {
				player.getInventory().addItem(ItemType.valueOf(args[1].toUpperCase()).createItemStack(player));
				player.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("generate")) {
				ItemType it = ItemType.valueOf(args[1].toUpperCase());
				Tier tier = Tier.valueOf(args[2].toUpperCase());
				ItemStack is = ItemGenerates.generateWeapon(player, it, tier);
				player.getInventory().addItem(is);
			}
			
			
			else if (args[0].equalsIgnoreCase("getdnb")) {
				player.getInventory().addItem(GradeStone.getItem());
				player.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("getdbh")) {
				player.getInventory().addItem(KeepGem.getItem());
				player.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("weapongui")) {
				GUIItemShow.open(player, 1);
			}

			else if (args[0].equalsIgnoreCase("getitem")) {
				player.getInventory().addItem(ItemType.valueOf(args[1].toUpperCase()).createItemStack(player));
			}
			
			else if (args[0].equalsIgnoreCase("getarmor")) {
				String id = args[1];
				
				Tier tier = ItemGenerates.rateTier();
				Map<Stat, Integer> stats = null;
				
				for (ItemType type : ItemType.values()) {
					// Check
					if (type.getCategory() instanceof Armor == false) continue;
					
					// Check set
					Armor a = (Armor) type.getCategory();
					if (!a.getSetID().equalsIgnoreCase(id)) continue;
					
					// Give
					if (stats == null) stats = ItemGenerates.generateStats(type.getBasicStats(), tier);
					ItemStack is = type.createItemStack(player);
					Item item = ItemAPI.get(is);
					ItemData data = item.getData();
					
					data.setTier(tier);
					data.setStats(stats);
					
					player.getInventory().addItem(ItemAPI.set(player, is, item));
				}

			}
			
			else if (args[0].equalsIgnoreCase("getrg")) {
				RandomGem tier = RandomGem.valueOf(args[1].toUpperCase());
				player.getInventory().addItem(tier.getItem());
			}
			
			else if (args[0].equalsIgnoreCase("getdsc")) {
				player.getInventory().addItem(RepairStone.getItem());
			}
			
			else if (args[0].equalsIgnoreCase("block")) {
				Block block = player.getTargetBlock(Sets.newHashSet(Material.AIR), 10);
				player.sendMessage("");
				player.sendMessage("§6Material: §e" + block.getType().name());
				player.sendMessage("§6Location: §e" + block.getLocation().toString());
				player.sendMessage("");
			}
			
			else if (args[0].equalsIgnoreCase("cratekey")) {
				String name = "";
				for (int i = 1 ; i < args.length ; i++) name += args[i] + " ";
				name = name.substring(0, name.length() - 1);
				
				Crate crate = null;
				for (Crate c : Crates.crates) {
					if (c.getName().equalsIgnoreCase(name)) crate = c;
				}

				player.getInventory().addItem(Crates.getKey(crate));
			}
			
			else if (args[0].equalsIgnoreCase("ecobag")) {
				EcoBag eb = EcoBag.valueOf(args[1].toUpperCase());
				player.getInventory().addItem(eb.createItemStack());
			}
			
			else if (args[0].equalsIgnoreCase("expbottle")) {
				ExpBottle eb = ExpBottle.valueOf(args[1].toUpperCase());
				player.getInventory().addItem(eb.createItemStack());
			}
			
			else if (args[0].equalsIgnoreCase("worldspeaker")) {
				player.getInventory().addItem(SpeakerItem.getItem());
			}

		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendTut(sender);
		}
		
		return false;
	}
	
	public void sendTut(CommandSender sender) {
		sender.sendMessage("§7-- §e§lAll commands §7-------------------------------------");
		sender.sendMessage("§a/skyrpg reload: §6Reload config");
		sender.sendMessage("§a/skyrpg setlevel <player> <level>: §6Set player level");
		sender.sendMessage("§a/skyrpg buffstat <player> <stat> <value> <isPercent> <time(seconds)>: §6Buff stat");
		sender.sendMessage("§a/skyrpg buffexp <player> <value> <time(seconds)>: §6Buff exp");
		sender.sendMessage("§a/skyrpg sendmess <player> <message>: §6Send message");
		sender.sendMessage("§a/skyrpg renameitem <player> <!name>: §6Rename item");
		sender.sendMessage("§a/skyrpg levelup <player> <!toLevel>: §6Level up player");
		sender.sendMessage("§a/skyrpg addexp <player> <exp>: §6Add exp");
		sender.sendMessage("§a/skyrpg infogui <player>: §6Open player gui");
		sender.sendMessage("§a/skyrpg potentialgui <player>: §6Open potential gui");
		sender.sendMessage("§a/skyrpg classgui <player>: §6Open player's class gui");
		sender.sendMessage("§a/skyrpg classgui <player> <class>: §6Open player's class gui");
		sender.sendMessage("§a/skyrpg seegui <target> <viewer>: §6Open see gui of <target> for <viewer>");
		sender.sendMessage("§a/skyrpg cuonghoagui <player>: §6Open cuong hoa gui");
		sender.sendMessage("§a/skyrpg chuyenhoagui <player>: §6Open chuyen hoa gui");
		sender.sendMessage("§a/skyrpg epdagui <player>: §6Open ep da gui");
		sender.sendMessage("§a/skyrpg duclogui <player>: §6Open duc lo gui");
		sender.sendMessage("§a/skyrpg tachdagui <player>: §6Open tach da gui");
		sender.sendMessage("§a/skyrpg nangbacgui <player>: §6Open tach enchant gui");
		sender.sendMessage("§a/skyrpg itemgui <player>: §6Open item gui");
		sender.sendMessage("§a/skyrpg craftgui <player> <id>: §6Open craft gui");
		sender.sendMessage("§a/skyrpg setlevel <player> <level>: §6Set player level");
		sender.sendMessage("§a/skyrpg resetelement <player>: §6Reset potential point");
		sender.sendMessage("§a/skyrpg setsetelement <e> <player>: §6Set class");
		sender.sendMessage("§a/skyrpg randomgem <player> <rate> (THO/THUONG/TRUNG/CAO/CUC_PHAM/HUYEN_THOAI): §6Give random gem");
		sender.sendMessage("§a/skyrpg fixall <player>: Fix");
		sender.sendMessage("§a/skyrpg craftmenu <player> <guiID>: Open craftmenu");
		sender.sendMessage("");
		sender.sendMessage("§7-- §e§lIngame only §7--------------------------------------");
		sender.sendMessage("§a/skyrpg checkstat: §6Check stat");
		sender.sendMessage("§a/skyrpg checkitem: §6Check item");
		sender.sendMessage("§a/skyrpg setitem <name> <level> <gemHole> <stats>: §6Set item");
		sender.sendMessage("§a/skyrpg setgem <stat> <value> <rate> <name>: §6Set gem");
		sender.sendMessage("§a/skyrpg setitemlevel <level>: §6Set item level");
		sender.sendMessage("§a/skyrpg setitemname <name>: §6Set item name");
		sender.sendMessage("§a/skyrpg setitemdesc <desc>: §6Set item desc");
		sender.sendMessage("§a/skyrpg setitemgrade <desc>: §6Set item tier (I, II, III, IV, V)");
		sender.sendMessage("§a/skyrpg setitemstats <stats>: §6Set item stats");
		sender.sendMessage("§a/skyrpg setitemenchants <enchants>: §6Set item stats");
		sender.sendMessage("§a/skyrpg setitemgemhole <hole>: §6Set item gem hole");
		sender.sendMessage("§a/skyrpg removeclass: §6Remove class");
		sender.sendMessage("§a/skyrpg getcuonghoa: §6Get item cuong hoa");
		sender.sendMessage("§a/skyrpg spawnmob <lv> <entityType> <mobType>: §6Spawn mob");
		sender.sendMessage("§a/skyrpg getthuochoiphuc <type>: §6Get thuoc hoi phuc");
		sender.sendMessage("§a/skyrpg getitem <type>: §6Get weapon");
		sender.sendMessage("§a/skyrpg getdnb: §6Get grade stone");
		sender.sendMessage("§a/skyrpg getdbh: §6Get Đá bảo hộ");
		sender.sendMessage("§a/skyrpg weapongui: §6WG");
		sender.sendMessage("§a/skyrpg getjewelry <type>: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg getitem <type>: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg getarmor <id>: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg getrg <RANDOM_I >> V>: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg getdsc: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg block: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg cratekey <name>: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg ecobag <name>: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg expbottle <name>: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg generate <type> <tier>: §6Yeu em 3000");
		sender.sendMessage("§a/skyrpg worldspeaker: §6Yeu em 3000");
		sender.sendMessage("");
	}
	
	
}
