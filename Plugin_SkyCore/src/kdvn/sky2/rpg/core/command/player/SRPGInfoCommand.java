package kdvn.sky2.rpg.core.command.player;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.skycore.customcraft.CraftStorageGUI;
import mk.plugin.skycore.gui.itemshow.GUIItemShow;
import mk.plugin.skycore.gui.player.GUIPlayerInfo;

public class SRPGInfoCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		
		if (arg1.getName().equalsIgnoreCase("khochetac")) {
			Player player = (Player) sender;
			CraftStorageGUI.open(player);
			return false;
		}
		
		else if (arg1.getName().equalsIgnoreCase("khovukhi")) {
			Player player = (Player) sender;
			GUIItemShow.open(player, 1);
			return false;
		}
		
		if (sender instanceof Player) {
			Player player = (Player) sender;
			GUIPlayerInfo.openGUI(player);

		}
		
		return false;
	}

}
