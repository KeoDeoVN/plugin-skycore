package kdvn.sky2.rpg.core.command.player;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.skycore.gui.jewelry.GUIJewelry;

public class JewelryCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		
		if (sender instanceof Player) {
			Player player = (Player) sender;
			GUIJewelry.openGUI(player);
		}
		
		return false;
	}

}
