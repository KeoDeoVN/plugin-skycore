package kdvn.sky2.rpg.core.command.player;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.skycore.gui.player.GUIPlayerSee;

public class SRPGSeeCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

		if (sender instanceof Player) {
			Player player = (Player) sender;
			try {
				Player target = Bukkit.getPlayer(args[0]);
				GUIPlayerSee.openGUI(target, player);
			}
			catch (ArrayIndexOutOfBoundsException e) {
				player.sendMessage("§aDùng lệnh: §f/xem <player>. §aVí dụ: §f/xem MasterClaus");
			}
			catch (NullPointerException e) {
				player.sendMessage("§aKhông rõ người chơi §f" + args[0]);
			}

		}
		
		return false;
	}

}
